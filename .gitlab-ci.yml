###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

default:
  tags:
    - cvmfs

stages:
  - test
  - deploy

image: registry.cern.ch/docker.io/library/python:latest

.setup_environment:
  before_script:
    - wget -qO- https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
    - eval "$(./bin/micromamba shell hook -s bash -r "$PWD/micromamba")"
    - micromamba create --yes --name test-env --file environment.yml
    - micromamba activate test-env
    - pip install .

pre-commit:
  stage: test
  before_script:
    - pip install pre-commit
  script:
    - pre-commit run --all-files
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  cache:
    paths:
      - ${PRE_COMMIT_HOME}

pylint:
  extends: .setup_environment
  stage: test
  script:
    - pylint src/LbMCSubmit
  allow_failure: true

pytest:
  extends: .setup_environment
  stage: test
  script:
    - pytest --junitxml=report.xml
    - python ./tests/generate_diffopts_tests.py
    - bats -T -j $(nproc) tests
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    reports:
      junit: report.xml

.deploy:
  stage: deploy
  image:
    name: registry.cern.ch/docker.io/alpine/git:latest
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_TAG
  variables:
    GIT_SSH_COMMAND: "ssh -i deploy_key -o StrictHostKeyChecking=no"
  before_script:
    - apk add curl curl-dev
    - git config --global user.name "${CI_GIT_USER_NAME}"
    - git config --global user.email "${CI_GIT_USER_EMAIL}"
  script:
    - chmod 500 ${DEPLOY_KEY}
    - scripts/.ci_deploy_via_mr.sh

deploy to LbConda:
  extends: .deploy
  variables:
    TARGET_FILE: "environments/Simulation/liaisons.yaml"
    TARGET_PROJECT_SLUG: "lhcb-core/conda-environments"
    TARGET_PROJECT_ID: 91275
    TARGET_PROJECT_BRANCH: "master"
    TARGET_PROJECT_TOKEN: "${LBCONDA_ACCESS_TOKEN}" # CI secret

deploy to LbAPI:
  extends: .deploy
  variables:
    TARGET_FILE: "environment.yaml"
    TARGET_PROJECT_SLUG: "lhcb-dpa/analysis-productions/LbAPI"
    TARGET_PROJECT_ID: 120284
    TARGET_PROJECT_BRANCH: "main"
    TARGET_PROJECT_TOKEN: "${LBAPI_ACCESS_TOKEN}" # CI secret
