###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import builtins
from typing import Optional, Union

import yaml
from strictyaml import Bool, Enum, FixedSeq, Float, Int, Map, MapPattern
from strictyaml import Optional as Opt
from strictyaml import Regex, Seq, Str
from strictyaml import UniqueSeq as Set
from strictyaml import Validator, load
from strictyaml.exceptions import YAMLValidationError

from LbMCSubmit.organisation import priority_levels, working_groups
from LbMCSubmit.sim_versions import SimBase


class InvalidStageData(builtins.BaseException):
    pass


def _generation_schema(SimClass: Optional[type[SimBase]] = None) -> Map:
    schema = {
        "production-tool": (Enum(SimClass.production_tools) if SimClass else Str()),
        "decay-tool": Enum(SimClass.decay_tools) if SimClass else Str(),
        "material-tool": Enum(SimClass.material_tools) if SimClass else Str(),
    }
    if SimClass is None:
        schema = {Opt(k): v for k, v in schema.items()}
    tool_option_schema = {  # Extra options for named tools
        # Variations on generator configuration
        Opt("EPOS"): Enum({"Fix3", "Fix4"}),
        Opt("PGun"): Enum(
            {
                "Flat",
                "CollisionsPP",
                "Pythia8",
                "SuperChic",
                "SuperChic2",
            }
        ),
        Opt("Beam"): Enum(
            {
                "PVDZ1300mm",
                "PVZpm250mm",
                "leftPVZ",
                "rightPVZ",
                "reducedPVZ",
                "SMOG2",
                "andSMOG2",
                "SMOGBeam1",
                "SMOGBeam2",
                "SMOGBeam1and2",
                "CrossingAngle",
            }
        ),
        Opt("Centrality"): Enum({"central", "peripheral"}),
        Opt("SignalMode"): Enum({"embedding"}),
    }
    schema.update({Opt("options"): Map(tool_option_schema)})
    return Map(schema)


def _stripping_schema() -> Map:
    adhoc_filter_schema = {
        # If lines is a single string we interpret it as a regex
        "lines": Seq(Str()) | Str(),
        "analysis-name": Regex(r"[A-Za-z0-9_]{5,20}"),
    }
    return Map(
        {
            Opt("version"): Str(),
            Opt("filtering-script"): Str(),
            Opt("data-pkgs"): Set(Str()),
            Opt("output-name"): Str(),
            Opt("filter"): Map(adhoc_filter_schema),
        }
    )


def _sprucing_schema() -> Map:
    return Map({Opt("version"): Str()})


def _trigger_schema() -> Map:
    filtered_schema = {}
    return Map({Opt("filter"): Map(filtered_schema)})


def _fast_mc_schema() -> Map:
    redecay_schema = {
        Opt("mode"): Enum(["signal-and-heavier"]),
        Opt("num-redecays"): Int(),
    }
    splitsim_schema = {
        Opt("mode"): Enum(["default", "extend-z"]),
    }
    schema = {
        Opt("redecay"): Bool() | Map(redecay_schema),
        Opt("splitsim"): Bool() | Map(splitsim_schema),
        Opt("tracker-only"): Bool(),
    }
    return Map(schema)


class Loader:
    def __init__(self, filename: str):
        self.filename = filename
        self.nevt_type = Int() | Regex(r"^([\d_]+\.)?[\d_]+[kM]$")

    def load(self, stage: int, data: Union[None, str, dict, list] = None):
        if data is None:
            with open(self.filename, "r", encoding="utf8") as fp:
                data = fp.read()
        elif isinstance(data, (dict, list)):
            if not hasattr(self, f"stage{stage}"):
                return data
            data = yaml.dump(data)
        return getattr(self, f"stage{stage}", yaml.safe_load)(data)

    def _load_from_schema(self, data: str, schema: Validator):
        try:
            return load(data, schema=schema, label=self.filename).data
        except YAMLValidationError as e:
            raise InvalidStageData(f"Invalid contents in {self.filename}, {e}") from e

    def stage0(self, data: str):
        eventtypes_schema = MapPattern(Int(), self.nevt_type) | Set(Regex(r"^\d{8}"))
        samples_schema = {
            Opt("priority", default="2a"): Enum(priority_levels),
            # Conditions
            Opt("event-types"): eventtypes_schema,
            "data-types": Set(Str()),
            Opt("pileup-nu"): Str(),
            Opt("num-events"): self.nevt_type,
            Opt("num-test-events", default=10): Int(),
            Opt("split-by-lumi"): Bool(),
            Opt("retention-rate"): Float(),  # no default here, see simbase.py
            Opt("magnet-polarities"): Set(Str()),
            Opt("collision-types", default=["pp"]): Set(
                Enum(
                    [
                        "pp",
                        "Pbp",
                        "pPb",
                        "pNe",
                        "pHe",
                        "pH2",
                        "pD2",
                        "pN2",
                        "pO2",
                        "pKr",
                        "pXe",
                        "pAr",
                        "Arp",
                        "ppAr",
                        "ppHe",
                        "ppH2",
                        "ppD2",
                        "ppN2",
                        "ppNe",
                        "ppO2",
                        "ppKr",
                        "ppXe",
                        "PbNe",
                        "PbHe",
                        "PbAr",
                        "PbH2",
                        "PbD2",
                        "PbO2",
                        "PbN2",
                        "PbKr",
                        "PbXe",
                        "ArPb",
                        "PbPbAr",
                        "PbPbNe",
                        "PbPbHe",
                        "PbPbH2",
                        "PbPbD2",
                        "PbPbN2",
                        "PbPbO2",
                        "PbPbKr",
                        "PbPbXe",
                        "PbPb",
                        "uniformHeadOn",
                    ]
                )
            ),
            Opt("smog", default=False): Bool(),
            # Information about processing
            Opt("generation"): _generation_schema(),
            Opt("fast-mc"): _fast_mc_schema(),
            Opt("stripping"): _stripping_schema(),
            Opt("sprucing"): Bool() | Str() | _sprucing_schema(),
            Opt("trigger"): _trigger_schema(),
            # Output
            Opt("file-format"): Str(),
        }
        schema = {
            "sim-version": Str(),
            # Production Metadata
            "name": Regex(r"^[A-Za-z0-9_\-\:\.\,\(\)\+\>\ ]+?$"),
            "inform": Set(Str()),
            "WG": Enum(working_groups),
            Opt("comment", default=""): Str(),
            # Information about samples
            Opt("event-types"): eventtypes_schema,
            "samples": Seq(Map(samples_schema)),
            # Information about processing
            Opt("generation"): _generation_schema(),
            Opt("fast-mc"): _fast_mc_schema(),
            Opt("stripping"): _stripping_schema(),
            Opt("sprucing"): Bool() | Str() | _sprucing_schema(),
            Opt("trigger"): _trigger_schema(),
            # Output
            Opt("file-format", default="MDST"): Str(),
        }
        schema = Map(schema)

        return self._load_from_schema(data, schema)

    def stage3(self, data: str):
        sample_sim_versions = [x["sim-version"] for x in yaml.safe_load(data)]
        schema = []
        for sim_version in sample_sim_versions:
            SimClass = SimBase.available_versions[sim_version]
            sample_schema = {
                "sim-version": Str(),
                "priority": Enum(priority_levels),
                # Production Metadata
                "name": Regex(r"^[A-Za-z0-9_\-\:\.\,\(\)\+\>\ ]+?$"),
                "inform": Set(Str()),
                "WG": Enum(working_groups),
                Opt("retention-rate"): Float(),
                "comment": Str(),
                # Conditions
                "event-types": MapPattern(Enum(SimClass.eventtypes), self.nevt_type)
                | Set(Enum(SimClass.eventtypes)),
                Opt("num-events"): self.nevt_type,
                "num-test-events": Int(),
                Opt("split-by-lumi"): Bool(),
                "collision-type": Enum(SimClass.collision_types),
                "smog": Bool(),
                "data-type": Enum(SimClass.data_types),
                "magnet-polarity": Enum(SimClass.magnet_polarities),
                # Information about processing
                "generation": _generation_schema(SimClass),
                Opt("fast-mc"): _fast_mc_schema(),
                Opt("stripping"): _stripping_schema(),
                Opt("sprucing"): Bool() | Str() | _sprucing_schema(),
                Opt("trigger"): _trigger_schema(),
                # Output
                "file-format": Enum(SimClass.file_formats),
                Opt("pileup-nu"): Str(),
            }
            schema.append(Map(sample_schema))

        return self._load_from_schema(data, FixedSeq(schema))
