"""
Options for applying trigger filtering during MC production

The filename hash was generated with:

$hash_contents
"""
raise NotImplementedError("This hasn't yet been validated")

from PhysConf.Filters import LoKi_Filters
from Gaudi.Configuration import *
from Configurables import DaVinci
from GaudiConf import IOHelper
from Configurables import ApplicationMgr


output_name = $output_name
hlt1_codes = $hlt1_codes
hlt2_codes = $hlt2_codes

# Trigger filter
hlt1_codes = " | ".join(["HLT_PASS_RE('" + x + "')" for x in hlt1_codes])
hlt2_codes = " | ".join(["HLT_PASS_RE('" + x + "')" for x in hlt2_codes])
trigfltrs = LoKi_Filters(HLT1_Code=hlt1_codes, HLT2_Code=hlt2_codes)

# DaVinci Configuration
DaVinci().Simulation = True
DaVinci().EvtMax = -1
fltrSeq = trigfltrs.sequence ("TrigFilters")
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().HistogramFile = output_name + ".histos.root"
DaVinci().RootInTES = "/Event/Turbo"

# Configure the copy-stream algorithm
oname = output_name + ".HLTFILTER.MDST"
ioh = IOHelper()
copy = ioh.outputAlgs(oname , "InputCopyStream/INPUTCOPY")
copy[0].AcceptAlgs = DaVinci().EventPreFilters

app = ApplicationMgr(OutStream=copy)
