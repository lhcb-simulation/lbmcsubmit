"""
Options for applying stripping filtering during MC production

The filename hash was generated with:

$hash_contents
"""
import re

stripping = $stripping_name
analysis_name = $analysis_name
output_format = $output_format
stripping_tck = $stripping_tck
lines = $lines
redo_calo_pid = $redo_calo_pid

from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)
# Select my lines
AllStreams = StrippingStream(analysis_name + ".Strip")
linesToAdd = []
for stream in streams:
    for line in stream.lines:
        # Remove any prescales
        line._prescale = 1.0
        if isinstance(lines, list):
            if line.name() in lines:
                linesToAdd.append(line)
        elif isinstance(lines, str):
            if re.match("^" + lines + "$$", line.name()):
                linesToAdd.append(line)
        else:
            raise NotImplementedError(type(lines))
if len(linesToAdd) == 0:
    raise ValueError(f"No lines matched {lines}")
if isinstance(lines, list) and len(linesToAdd) != len(lines):
    missing = set(lines) - {line.name() for line in linesToAdd}
    raise ValueError("Did not find lines named: " + repr(missing))
AllStreams.appendLines(linesToAdd)
sc = StrippingConf(Streams=[AllStreams], MaxCandidates = 2000, TESPrefix = 'Strip')
AllStreams.sequence().IgnoreFilterPassed = False  # so that we do not get all events written out

# Configuration of SelDSTWriter
enablePacking = True
if output_format == "DST":
    # Standard configuration for full DST output
    from DSTWriters.microdstelements import *
    from DSTWriters.Configuration import (SelDSTWriter,
                                            stripDSTStreamConf,
                                            stripDSTElements
                                            )

    SelDSTWriterElements = {
        'default': stripDSTElements(pack=enablePacking)
    }
    SelDSTWriterConf = {
        'default': stripDSTStreamConf(pack=enablePacking)
    }
elif output_format == "MDST":
    # Standard configuration for MDST output
    from DSTWriters.microdstelements import *
    from DSTWriters.Configuration import (
        SelDSTWriter,
        stripDSTStreamConf,
        stripDSTElements,
        stripMicroDSTStreamConf,
        stripMicroDSTElements,
    )

    SelDSTWriterElements = {
        'default': stripMicroDSTElements(pack=enablePacking, isMC=True)
    }
    SelDSTWriterConf = {
        'default': stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }
else:
    raise NotImplementedError(output_format)

if redo_calo_pid:
    # Items that might get lost when running the CALO+PROTO ReProcessing in DV
    caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
    # Make sure they are present on full DST streams
    SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter(
    "MyDSTWriter",
    StreamConf=SelDSTWriterConf,
    MicroDSTElements=SelDSTWriterElements,
    OutputFileSuffix='Filtered',
    SelectionSequences=sc.activeStreams()
)

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=stripping_tck)

# DaVinci Configuration
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
