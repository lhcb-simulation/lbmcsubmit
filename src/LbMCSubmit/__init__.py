###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "beam_parameters",
    "datapkgs",
    "steps",
    "utils",
    "run",
    "eventtypes",
)

from copy import deepcopy
from itertools import product

from . import beam_parameters, datapkgs, eventtypes, lumi, steps, utils
from .schema import Loader
from .sim_versions import SimBase
from .validation import validate_all


def run(
    yaml_path,
    in_stage: int,
    out_stage: int,
    *,
    verbose=0,
    validation=True,
    log_fcn=lambda s: None,
):
    loader = Loader(yaml_path)
    data = None
    for i in range(in_stage, out_stage):
        data = loader.load(i, data)
        i += 1
        if i <= 3:
            data = globals()[f"apply_stage_{i}"](data)
        else:
            data = [SimBase.apply_stage(sample, i) for sample in data]
        if validation:
            if i == 5:
                stage5data = deepcopy(data)
            elif i == 6:
                stage6data = deepcopy(data)
        utils.print_stage(data, f"stage {i} output", verbose)

    if out_stage == 6:
        utils.check_duplicate_prod_name(data)
        if validation:
            log_fcn("Running checks")
            for stage5sample, stage6sample in zip(stage5data, stage6data):
                validate_all(stage5sample, stage6sample["steps"])
        log_fcn(f"Generated {len(data)} production requests")
        n_sub = sum(len(d["event_types"]) for d in data)
        log_fcn(f"    containing {n_sub} subrequests")
        n_events = sum(x["num_events"] for d in data for x in d["event_types"])
        log_fcn(f"    and {n_events:,} events")

    return data


def apply_stage_1(data: dict) -> dict:
    SimVer = SimBase.available_versions[data["sim-version"]]
    data["sim-version"] = SimVer.version
    data.setdefault("generation", {})
    data["generation"].setdefault("production-tool", SimVer.default_production_tool)
    data["generation"].setdefault("decay-tool", SimVer.default_decay_tool)
    data["generation"].setdefault("material-tool", SimVer.default_material_tool)
    return data


def apply_stage_2(data: dict) -> list[dict]:
    samples = []
    for sample in data.pop("samples"):
        samples.append(deepcopy(data))
        utils.recursive_update(samples[-1], sample)
    return samples


def apply_stage_3(data: list[dict]) -> list[dict]:
    samples = []
    for sample in data:
        keys = (
            sample.pop("data-types"),
            sample.pop("collision-types", ["pp"]),
        )
        user_magnet_polarities = sample.pop("magnet-polarities", None)
        available_data_types = SimBase.available_versions[
            sample["sim-version"]
        ].data_types
        for user_data_type, collision_type in product(*keys):
            data_types = []
            # Assign weights of 1 to avoid crash if user specifies split-by-lumi
            # for non-'super' data-types
            weights = {user_data_type: {"MagUp": 1, "MagDown": 1}}
            if user_data_type in available_data_types and user_data_type not in (
                super_data_types := utils.get_super_data_types(collision_type)
            ):
                data_types = [user_data_type]
            elif user_data_type in super_data_types:
                # User may have requested something like "Run2" or "2024"
                sub_data_types = super_data_types[user_data_type]
                # Important to make sure the resulting data-types are supported
                data_types = [dt for dt in sub_data_types if dt in available_data_types]
                weights = lumi.get_lumi_weights(
                    collision_type, data_types, user_magnet_polarities
                )
            if len(data_types) == 0:
                raise NotImplementedError(
                    f"Data-type {user_data_type} not understood "
                    f"for {collision_type} collisions "
                    f"in Sim{sample['sim-version']}"
                )
            if (sample_size := sample.get("num-events", None)) is None:
                sample_size = sample["event-types"]
            for data_type in data_types:
                if not user_magnet_polarities:
                    magnet_polarities = sorted(
                        lumi.get_sample_polarities(data_type, collision_type),
                        reverse=True,
                    )
                else:
                    magnet_polarities = user_magnet_polarities
                for magnet_polarity in magnet_polarities:
                    sample = deepcopy(sample)
                    sample["magnet-polarity"] = magnet_polarity
                    sample["data-type"] = data_type
                    sample["collision-type"] = collision_type
                    if sample.get("split-by-lumi", False):
                        sample_weight = weights[data_type][magnet_polarity]
                        if "num-events" in sample:
                            sample["num-events"] = utils.split_sample(
                                sample_size, sample_weight
                            )
                        else:
                            sample["event-types"] = {
                                event_type: utils.split_sample(
                                    num_events, sample_weight
                                )
                                for event_type, num_events in sample_size.items()
                            }
                    samples.append(sample)
    return samples
