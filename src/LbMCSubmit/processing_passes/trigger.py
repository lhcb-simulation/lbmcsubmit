###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


L0 = {
    "0x0037": {  # 2011 pp
        "application": "Moore/v24r4",
        "data-pkgs": [],
    },
    "0x0045": {  # 2012 pp
        "application": "Moore/v24r4",
        "data-pkgs": [],
    },
    "0x00a2": {  # 2015 pp
        "application": "Moore/v25r5p3",  # or v24r3p1
        "data-pkgs": [],
    },
    "0x160F": {  # 2016 pp
        "application": "Moore/v25r5p3",
        "data-pkgs": [],
    },
    "0x1709": {  # 2017 pp
        "application": "Moore/v26r6p1",
        "data-pkgs": [],
    },
    "0x18a4": {  # 2018 pp
        "application": "Moore/v28r3p1",
        "data-pkgs": [],
    },
    "0x1710": {  # 2013 pPb
        "application": {
            "name": "Moore",
            "version": "v20r4",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    "0x1621": {  # 2016 pPb
        "application": "Moore/v25r5p2",
        "data-pkgs": [],
    },
    "0x024e": {  # 2015 pAr
        "application": "Moore/v24r2",
        "data-pkgs": [],
    },
    "0x1608": {  # May 2016 pHe (6.5 TeV beams)
        "application": "Moore/v25r5",
        "data-pkgs": [],
    },
    "0x1620": {  # Dec 2016 pHe (4 TeV beams)
        "application": "Moore/v25r5p3",
        "data-pkgs": [],
    },
    "0x1725": {  # 2017 pNe
        "application": "Moore/v26r6p1",
        "data-pkgs": [],
    },
    "0x1827": {  # 2018 PbPb PbNe
        "application": "Moore/v28r3p1",
        "data-pkgs": [],
    },
    "0x0243": {  # 2015 PbPb
        "application": "Moore/v24r3",
        "data-pkgs": [],
    },
}

HLT = {
    "0x40760037": {  # 2011 pp
        "application": {
            "name": "Moore",
            "version": "v12r8g4",
            "binary_tag": "x86_64-slc5-gcc43-opt",
        },
        "data-pkgs": [],
    },
    "0x409f0045": {  # 2012 pp
        "application": {
            "name": "Moore",
            "version": "v14r8p1g1",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    "0x411400a2": {  # 2015 pp
        "application": {
            "name": "Moore",
            "version": "v24r3p1",
            "binary_tag": "x86_64-slc6-gcc49-opt",
        },
        "data-pkgs": [],
    },
    "0x406a1710": {  # 2013 pPb
        "application": {
            "name": "Moore",
            "version": "v14r12",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    "0x4113024e": {  # 2015 pAr
        "application": {
            "name": "Moore",
            "version": "v24r2",
            "binary_tag": "x86_64-slc6-gcc48-opt",
        },
        "data-pkgs": [],
    },
    "0x41260243": {  # 2015 PbPb
        "application": {
            "name": "Moore",
            "version": "v24r3",
            "binary_tag": "x86_64-slc6-gcc48-opt",
        },
        "data-pkgs": [],
    },
}

HLT1 = {
    "0x5138160F": {  # 2016 pp
        "application": "Moore/v25r5p3",
        "data-pkgs": [],
    },
    "0x51611709": {  # 2017 pp
        "application": "Moore/v26r6p1",
        "data-pkgs": [],
    },
    "0x517a18a4": {  # 2018 pp
        "application": "Moore/v28r3p1",
        "data-pkgs": [],
    },
    "0x51431621": {  # 2016 pPb
        "application": "Moore/v25r5p2",
        "data-pkgs": [],
    },
    "0x512e1608": {  # May 2016 pHe (6.5 TeV beams)
        "application": "Moore/v25r5",
        "data-pkgs": [],
    },
    "0x513d1620": {  # Dec 2016 pHe (4 TeV beams)
        "application": "Moore/v25r5p3",
        "data-pkgs": [],
    },
    "0x51641725": {  # 2017 pNe
        "application": "Moore/v26r6p1",
        "data-pkgs": [],
    },
    "0x51861827": {  # 2018 PbPb PbNe
        "application": "Moore/v28r3p1",
        "data-pkgs": [],
    },
    "hlt1_pp_no_gec_no_ut": {  # 2022 pp
        "application": {
            "name": "Moore",
            "version": "v54r3p2",
            "binary_tag": "x86_64_v2-centos7-gcc11-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:allen_hlt1_production",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
                # "output_file": "allen_production_test.dst",  # Not needed?
            },
            "extra_args": ["hlt1_pp_no_gec_no_ut"],
        },
    },
    "hlt1_2023": {  # 2023 pp
        "application": {
            "name": "Moore",
            "version": "v54r12p1",
            "binary_tag": "x86_64_v2-centos7-gcc12+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:allen_hlt1_sim2023",
            "extra_options": {
                "data_type": "2023",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
        },
    },
    "hlt1_2024.Q3": {  # 2024.Q3 pp
        "application": {
            "name": "Moore",
            "version": "v55r11",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt1",
            "extra_options": {
                "data_type": "2023",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": ["--", "--sequence=hlt1_pp_matching", "--flagging"],
        },
    },
    "HLT1_2024.W31.34_noUT": {
        "application": {
            "name": "Moore",
            "version": "v55r11p7",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt1",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--sequence=hlt1_pp_matching_no_ut_1000KHz",
                "--flagging",
            ],
        },
    },
    "HLT1_2024.W35.39": {
        "application": {
            "name": "Moore",
            "version": "v55r12p6",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt1",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--sequence=hlt1_pp_forward_then_matching_1000KHz",
                "--flagging",
            ],
        },
    },
    "HLT1_2024.W40.42": {
        "application": {
            "name": "Moore",
            "version": "v55r13p5",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt1",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--sequence=hlt1_pp_forward_then_matching_and_downstream_1200KHz",
                "--flagging",
            ],
        },
    },
    "HLT1_2024_PbPb": {
        "application": {
            "name": "Moore",
            "version": "v55r16p6",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt1",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--sequence=hlt1_PbPb_PbSMOG_veloSP",
                "--flagging",
            ],
        },
    },
}

HLT2 = {
    "0x6139160F": {  # 2016 pp
        "application": "Moore/v25r5p3",
        "data-pkgs": [],
    },
    "0x62661709": {  # 2017 pp
        "application": "Moore/v26r6p1",
        "data-pkgs": [],
    },
    "0x617d18a4": {  # 2018 pp
        "application": "Moore/v28r3p1",
        "data-pkgs": [],
    },
    "0x61421621": {  # 2016 pPb
        "application": "Moore/v25r5p2",
        "data-pkgs": [],
    },
    "0x612e1608": {  # May 2016 pHe (6.5 TeV beams)
        "application": "Moore/v25r5",
        "data-pkgs": [],
    },
    "0x613c1620": {  # Dec 2016 pHe (4 TeV beams)
        "application": "Moore/v25r5p3",
        "data-pkgs": [],
    },
    "0x61641725": {  # 2017 pNe
        "application": "Moore/v26r6p1",
        "data-pkgs": [],
    },
    "0x61851827": {  # 2018 PbPb PbNe
        "application": "Moore/v28r3p1",
        "data-pkgs": [],
    },
    "HLT2-2022-pp": {  # 2022 pp
        "application": {
            "name": "Moore",
            "version": "v54r16p4",
            "binary_tag": "x86_64_v2-centos7-gcc12+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2_pp_2022_reprocessing",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "input_raw_format": 0.5,
                "scheduler_legacy_mode": False,
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 4,
                    "max_buffer_size": 1048576,
                },
            },
        },
    },
    "HLT2-2023-noRetinaLinks": {  # 2023 pp, missing retina clusters links
        "application": {
            "name": "Moore",
            "version": "v54r16p4",
            "binary_tag": "x86_64_v2-centos7-gcc12+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2_pp_commissioning",
            "extra_options": {
                "data_type": "2023",
                "simulation": True,
                "input_type": "ROOT",
                "input_raw_format": 0.5,
                "scheduler_legacy_mode": False,
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 4,
                    "max_buffer_size": 1048576,
                },
            },
        },
    },
    "HLT2-2023": {  # 2023 pp
        "application": {
            "name": "Moore",
            "version": "v54r16p5",
            "binary_tag": "x86_64_v2-centos7-gcc12+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2_pp_commissioning",
            "extra_options": {
                "data_type": "2023",
                "simulation": True,
                "input_type": "ROOT",
                "input_raw_format": 0.5,
                "scheduler_legacy_mode": False,
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 4,
                    "max_buffer_size": 1048576,
                },
            },
        },
    },
    "HLT2-2024.Q3": {  # 2024.Q3 pp
        "application": {
            "name": "Moore",
            "version": "v55r11",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2",
            "extra_options": {
                "data_type": "2023",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=hlt2_pp_2024",
                "--flagging",
            ],
        },
    },
    "HLT2-2024.W31.34": {
        "application": {
            "name": "Moore",
            "version": "v55r11p7",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=hlt2_pp_2024",
                "--flagging",
            ],
        },
    },
    "HLT2-2024.W35.39": {
        "application": {
            "name": "Moore",
            "version": "v55r12p6",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=hlt2_pp_2024",
                "--flagging",
            ],
        },
    },
    "HLT2-2024.W40.42": {
        "application": {
            "name": "Moore",
            "version": "v55r13p5",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=hlt2_pp_2024",
                "--flagging",
            ],
        },
    },
    "HLT2_2024_PbPb": {
        "application": {
            "name": "Moore",
            "version": "v55r16p6",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:hlt2",
            "extra_options": {
                "data_type": "Upgrade",
                "simulation": True,
                "input_type": "ROOT",
                "output_type": "ROOT",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "input_raw_format": 0.5,
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=hlt2_PbPb_default_2024",
                "--flagging",
            ],
        },
    },
}
