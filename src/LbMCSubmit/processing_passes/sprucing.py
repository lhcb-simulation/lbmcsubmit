###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

SPRUCING = {
    "24c2": {
        "application": {
            "name": "Moore",
            "version": "v55r11p7",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:spruce",
            "extra_options": {
                "input_raw_format": 0.5,
                "input_type": "ROOT",
                "input_process": "Hlt2",
                "simulation": True,
                "data_type": "Upgrade",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "output_type": "ROOT",
                "process": "Spruce",
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=Sprucing_production_physics_pp_Collision24c2",
                "--flagging",
            ],
        },
    },
    "24c3": {
        "application": {
            "name": "Moore",
            "version": "v55r12p6",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:spruce",
            "extra_options": {
                "input_raw_format": 0.5,
                "input_type": "ROOT",
                "input_process": "Hlt2",
                "simulation": True,
                "data_type": "Upgrade",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "output_type": "ROOT",
                "process": "Spruce",
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=Sprucing_production_physics_pp_Collision24c3",
                "--flagging",
            ],
        },
    },
    "24c4": {
        "application": {
            "name": "Moore",
            "version": "v55r13p5",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
        "options": {
            "entrypoint": "Moore.production:spruce",
            "extra_options": {
                "input_raw_format": 0.5,
                "input_type": "ROOT",
                "input_process": "Hlt2",
                "simulation": True,
                "data_type": "Upgrade",
                "compression": {
                    "algorithm": "ZSTD",
                    "level": 1,
                    "max_buffer_size": 1048576,
                },
                "output_type": "ROOT",
                "process": "Spruce",
            },
            "extra_args": [
                "--",
                "--velo-source=VPRetinaCluster",
                "--settings=Sprucing_production_physics_pp_Collision24c4",
                "--flagging",
            ],
        },
    },
}
