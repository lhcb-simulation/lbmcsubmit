###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

TURBO = {
    "05-WithTurcal": {
        "application": "DaVinci/v44r7",
        "data-pkgs": ["TurboStreamProd.v4r2p10"],
    },
    "04a-WithTurcal": {
        "application": "DaVinci/v42r8p3",
        "data-pkgs": ["TurboStreamProd.v4r2p7"],
    },
    "03a": {
        "application": "DaVinci/v41r5",
        "data-pkgs": ["TurboStreamProd.v4r2p9"],
    },
    "02": {
        "application": "DaVinci/v40r1p3",
        "data-pkgs": ["TurboStreamProd.v2r0"],
    },
    "02a": {
        # Should be identical to 02 but changed for Sim10 out of excess of caution
        "application": "DaVinci/v41r5",
        "data-pkgs": ["TurboStreamProd.v4r2p13"],
    },
}
