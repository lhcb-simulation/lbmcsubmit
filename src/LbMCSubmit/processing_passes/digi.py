###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

DIGI = {
    "14c": {
        "application": "Boole/v30r4",
        "data-pkgs": [],
    },
    "15": {
        "application": "Boole/v33r3",
        "data-pkgs": [],
    },
    "16": {
        "application": "Boole/v44r0",
        "data-pkgs": [],
    },
    "17": {
        "application": "Boole/v45r0",
        "data-pkgs": [],
    },
    "17a": {
        "application": "Boole/v45r0",
        "data-pkgs": [],
    },
    "17b": {
        "application": {
            "name": "Boole",
            "version": "v46r0",
            "binary_tag": "x86_64_v2-el9-gcc13+detdesc-opt",
        },
        "data-pkgs": [],
    },
}
