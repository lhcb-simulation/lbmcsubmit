###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

RECO = {
    "18": {"application": "Brunel/v54r4", "data-pkgs": []},
    "17": {"application": "Brunel/v52r10", "data-pkgs": ["Det/SQLDDDB.v7r10"]},
    "17at5TeV": {"application": "Brunel/v52r10", "data-pkgs": []},
    "16": {"application": "Brunel/v50r7", "data-pkgs": []},
    "15a": {"application": "Brunel/v48r5", "data-pkgs": ["Det/SQLDDDB.v7r10"]},
    "15aLead15": {"application": "Brunel/v49r2p1", "data-pkgs": []},
    "14c": {
        "application": {
            "name": "Brunel",
            "version": "v43r2p13",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    "14r1": {
        "application": {
            "name": "Brunel",
            "version": "v43r2p13",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    "16pLead": {"application": "Brunel/v51r1", "data-pkgs": []},
    "16Smog": {"application": "Brunel/v50r2", "data-pkgs": []},
    "17aSmog": {"application": "Brunel/v52r10", "data-pkgs": ["Det/SQLDDDB.v7r10"]},
    "18Smog": {"application": "Brunel/v54r4", "data-pkgs": []},
    "18Lead": {"application": "Brunel/v54r4", "data-pkgs": []},
}
