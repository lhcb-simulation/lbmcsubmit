###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Mapping of non-deprecated Stripping versions to their DaVinci versions and
# data packages.

STRIPPING = {
    # 2018
    "34": {
        "application": "DaVinci/v44r7",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    "34r0p1": {
        "application": "DaVinci/v44r10p2",
        "data-pkgs": ["TMVAWeights.v1r12"],
    },
    "34r0p2": {
        "application": "DaVinci/v44r11p1",
        "data-pkgs": ["TMVAWeights.v1r17"],
    },
    "34r0p3": {
        "application": "DaVinci/v44r11p6",
        "data-pkgs": ["TMVAWeights.v1r18"],
    },
    "35r2": {
        "application": "DaVinci/v44r10p7",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    "35r3": {
        "application": "DaVinci/v44r10p7",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    # 2017
    "29r2": {
        "application": "DaVinci/v42r7p3",
        "data-pkgs": ["TMVAWeights.v1r9"],
    },
    "29r2p1": {
        "application": "DaVinci/v42r9p2",
        "data-pkgs": ["TMVAWeights.v1r16"],
    },
    "29r2p2": {
        "application": "DaVinci/v42r11p2",
        "data-pkgs": ["TMVAWeights.v1r16"],
    },
    "29r2p3": {
        "application": "DaVinci/v44r11p6",
        "data-pkgs": ["TMVAWeights.v1r18"],
    },
    "32": {
        "application": "DaVinci/v42r8p3",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    "33r2": {
        "application": "DaVinci/v44r10p6",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    # 2016
    "28r2": {
        "application": "DaVinci/v44r10p5",
        "data-pkgs": ["TMVAWeights.v1r16"],
    },
    "28r2p1": {
        "application": "DaVinci/v44r11p4",
        "data-pkgs": ["TMVAWeights.v1r17"],
    },
    "28r2p2": {
        "application": "DaVinci/v44r11p6",
        "data-pkgs": ["TMVAWeights.v1r18"],
    },
    "30r2": {
        "application": "DaVinci/v41r5",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    "30r3": {
        "application": "DaVinci/v41r5",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    "27": {
        "application": "DaVinci/v42r2",
        "data-pkgs": ["TMVAWeights.v1r8"],
    },
    # 2015
    "24r2": {
        "application": "DaVinci/v44r10p5",
        "data-pkgs": [],
    },
    "31r2": {
        "application": "DaVinci/v41r5",
        "data-pkgs": ["TMVAWeights.v1r10"],
    },
    "31r1": {
        "application": "DaVinci/v41r4p5",
        "data-pkgs": ["TMVAWeights.v1r9"],
    },
    "25": {
        "application": "DaVinci/v40r4",
        "data-pkgs": [],
    },
    # 2013
    "20r2": {
        "application": {
            "name": "DaVinci",
            "version": "v32r2p8",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    "20r3p1": {
        "application": {
            "name": "DaVinci",
            "version": "v32r2p18",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    "20r3": {
        "application": {
            "name": "DaVinci",
            "version": "v32r2p10",
            "binary_tag": "x86_64-slc5-gcc46-opt",
        },
        "data-pkgs": [],
    },
    # 2012
    "21": {
        "application": "DaVinci/v36r1p5",
        "data-pkgs": [],
    },
    "21r0p1": {
        "application": "DaVinci/v39r1p1",
        "data-pkgs": [],
    },
    "21r0p2": {
        "application": "DaVinci/v39r1p6",
        "data-pkgs": ["TMVAWeights.v1r14"],
    },
    # 2011
    "21r1": {
        "application": "DaVinci/v36r1p5",
        "data-pkgs": [],
    },
    "21r1p1": {
        "application": "DaVinci/v39r1p1",
        "data-pkgs": [],
    },
    "21r1p2": {
        "application": "DaVinci/v39r1p6",
        "data-pkgs": ["TMVAWeights.v1r14"],
    },
    # 2015 5 TeV
    "22": {
        "application": "DaVinci/v38r0",
        "data-pkgs": [],
    },
}
