###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import functools
import hashlib
import importlib.resources
import json
import os
from abc import abstractmethod
from pathlib import Path
from string import Template
from typing import Callable, Generic, Optional, Tuple, TypeVar, Union

import yaml
from rich.console import Console
from rich.markdown import Markdown
from rich.syntax import Syntax
from rich.text import Text


def make_step(
    name: str,
    processing_pass: str,
    pkgs: list[str],
    app: Union[str, dict],
    options: Union[list[str], dict],
    in_types: list[str],
    out_types: list[str],
    *,
    visible: bool = True,
    options_format: Optional[str] = None,
    db_tags: Optional[dict] = None,
) -> dict:
    step = {
        "name": name,
        "processing_pass": processing_pass,
        "visible": visible,
        "application": app,
        "options": options,
        "data_pkgs": pkgs,
        "input": [{"type": x, "visible": False} for x in in_types],
        "output": [{"type": x, "visible": False} for x in out_types],
    }
    if db_tags:
        step["dbtags"] = db_tags
    if options_format:
        step["options_format"] = options_format
    return step


def add_compression_to_step(step: dict, alg: str, level: int):
    opts_file = f"$APPCONFIGOPTS/Persistency/Compression-{alg}-{level}.py"
    if isinstance(step["options"], list):
        step["options"] += [opts_file]
    elif "files" in step["options"]:
        step["options"]["files"] += [opts_file]
    else:
        step["options"]["extra_options"]["compression"] = {
            "algorithm": alg,
            "level": level,
            "max_buffer_size": 1048576,
        }


def to_pol_str(polarity: str) -> str:
    pol = {
        "MagUp": "u",
        "MagDown": "d",
    }[polarity]
    return f"m{pol}100"


def parse_num_event(num_evt: Union[str, int]) -> int:
    if isinstance(num_evt, int):
        return num_evt

    si_suffix = {"k": 1000, "M": 1000000}
    num_evt = num_evt.replace("_", "")
    if num_evt[-1] in si_suffix:
        num_flt = float(num_evt[:-1])
        return int(num_flt * si_suffix[num_evt[-1]])

    return int(num_evt)


def split_sample(n_evts: str, fraction: float) -> int:
    """Take a fraction of a sample size, rounded to the nearest thousand and
    with a minimum size of 1000.
    """

    sample_size = parse_num_event(n_evts) * fraction
    sample_size = int(round(sample_size, -3))

    return max(sample_size, 1000)


def print_stage(data, title: str, verbose: int) -> None:
    console = Console(stderr=True)
    if verbose >= 3:
        console.print(Markdown(f"# {title.title()}"))
        data_str = yaml.dump(data, sort_keys=False)
        console.print(Syntax(data_str, "yaml", line_numbers=True))
    elif verbose >= 1:
        console.print(Text(f"Produced {title}", "bold green"))


T = TypeVar("T")


class classproperty(Generic[T]):
    def __init__(self, method: Callable[..., T]):
        self.method = method
        functools.update_wrapper(self, method)  # type: ignore

    def __get__(self, obj, cls=None) -> T:
        if cls is None:
            cls = type(obj)
        return self.method(cls)


def abstractproperty():
    @property
    @abstractmethod
    def func(cls):
        raise NotImplementedError()

    return func


def recursive_update(old: dict, new: dict) -> None:
    for k, v in new.items():
        if isinstance(v, dict) and k in old and not isinstance(old[k], bool):
            recursive_update(old[k], v)
        else:
            old[k] = v


def order_options(options: list[Tuple[int, str]]) -> list[str]:
    return [pair[1] for pair in sorted(options)]


def check_duplicate_prod_name(prod_data: list[dict]):
    prod_names = []
    for prod in prod_data:
        if prod["name"] not in prod_names:
            prod_names.append(prod["name"])
            continue

        success = False
        for j in range(1, 10):
            new_name = f"{prod['name']} - {j}"
            if new_name not in prod_names:
                success = True
                break

        if not success:
            raise NotImplementedError(
                f"Too many requests with the same name: {prod['name']}."
                "You should probably break up your request into multiple yaml files"
            )
        prod["name"] = new_name
        prod_names.append(prod["name"])


def write_filter_options(filter_type, hash_contents) -> str:
    var = "MCFILTERING_DATAPKG_PATH"

    if var not in os.environ:
        raise RuntimeError(f"{var} must be set to created filtered productions")
    datapkg_path = Path(os.environ[var])
    if not (datapkg_path / "MCFilteringOptions.xenv").exists():
        raise RuntimeError(
            f"{datapkg_path} appears to not be a valid clone of MCFilteringOptions"
        )
    template = (
        importlib.resources.files("LbMCSubmit.templates")
        .joinpath(f"{filter_type}_filtered.py.tpl")
        .read_text(encoding="ASCII")
    )
    hash_contents["template_sha256"] = hashlib.sha256(template.encode()).hexdigest()
    content_hash = make_filter_name_hash(**hash_contents)
    python_code = Template(template).substitute(
        hash_contents=json.dumps(hash_contents, indent=4),
        **{
            k: json.dumps(v, indent=4) if isinstance(v, list) else v
            for k, v in hash_contents.items()
        },
    )
    repo_path = Path(filter_type) / content_hash[:3] / f"{content_hash[3:]}.py"
    full_path = datapkg_path / repo_path
    full_path.parent.mkdir(parents=True, exist_ok=True)
    full_path.write_text(python_code)
    return f"$MCFILTERINGOPTIONS_BASE/{repo_path}"


def make_filter_name_hash(**hash_contents):
    hasher = hashlib.sha256()
    for k, v in hash_contents.items():
        hasher.update(k.encode())
        hasher.update(b"\x00\x00")
        if isinstance(v, (str, bool)):
            hasher.update(str(v).encode())
        elif isinstance(v, list):
            hasher.update("\x00".join(v).encode())
        else:
            raise NotImplementedError(type(v))
        hasher.update(b"\x00\x00")
    return hasher.hexdigest()


def run_from_data_type(data_type: str) -> int:

    run_years = {
        1: {"2010", "2011", "2012", "2013"},
        2: {"2015", "2016", "2017", "2018"},
        3: {"2022", "2023", "2024", "2025"},
    }
    for run, years in run_years.items():
        if any(
            [
                data_type.startswith(year) or data_type.startswith(f"expected-{year}")
                for year in years
            ]
        ):
            return run
    raise NotImplementedError(data_type)


def get_super_data_types(collision_type: str = "pp") -> dict[str, list[str]]:
    """Keep track of groupings of data-types that are commonly requested"""

    blocks_2024 = [
        "2024.Q1.2",
        "2024.W25.27",
        "2024.W29.30",
        "2024.W31.34",
        "2024.W35.37",
        "2024.W37.39",
        "2024.W40.42",
    ]

    groupings = {
        "pp": {
            # TODO: once lumi numbers are in lumi.py, add the following:
            # "Run1": ["2011", "2012"],
            # "Run2": ["2015", "2016", "2017", "2018"],
            # "Run3": ["2022", "2023", *blocks_2024],
            "2024": blocks_2024,
        }
        # TODO: add smog, heavy ions, pp ref runs
    }

    if collision_type in groupings:
        return groupings[collision_type]
    return {}


def flatten_app_name(app: Union[str, dict]) -> str:
    if isinstance(app, dict):
        return f"{app['name']}/{app['version']}"
    return app
