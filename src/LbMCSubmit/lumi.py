###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Iterable

LUMI_SPLIT = {
    # Initially expressed in percentage
    # TODO: replace with inverse picobarns
    "pp": {
        "2011": {
            "MagDown": 56,
            "MagUp": 44,
        },
        "2012": {
            "MagDown": 50,
            "MagUp": 50,
        },
        "2013": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2015": {
            "MagDown": 57,
            "MagUp": 43,
        },
        "2015-5TeV": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2016": {
            "MagDown": 53,
            "MagUp": 48,
        },
        "2017": {
            "MagDown": 51,
            "MagUp": 49,
        },
        "2017-5TeV": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2018": {
            "MagDown": 48,
            "MagUp": 52,
        },
        "2022": {  # One MagUp fill 8496 at the end of the year
            "MagDown": 90,
            "MagUp": 10,
        },
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2024.Q1.2": {
            "MagDown": 1282,  # /pb (rundb) Fills 9400 - 9644 and 9694 - 9801
            # 329 /pb + 953 /pb
            "MagUp": 644,  # /pb (rundb) Fills 9651 - 9691
        },
        # Block 4
        "2024.W25.27": {
            "MagDown": 1447,  # /pb (rundb)
            "MagUp": 0,
        },
        # Block 3
        "2024.W29.30": {
            "MagDown": 0,
            "MagUp": 928,  # /pb (rundb)
        },
        # Block 1 and 2
        "2024.W31.34": {
            "MagDown": 0,
            "MagUp": 1984,  # 1,358 /pb + 626 /pb (rundb)
        },
        # Block 5
        "2024.W35.37": {
            "MagDown": 0,
            "MagUp": 1176,  # /pb (rundb)
        },
        # Block 6
        "2024.W37.39": {
            "MagDown": 1035,  # /pb (rundb)
            "MagUp": 0,
        },
        # Block 7 and 8
        "2024.W40.42": {
            "MagDown": 745,  # /pb (rundb)
            "MagUp": 441,  # /pb (rundb)
        },
    },
    "Pbp": {
        "2013": {
            "MagDown": 53,
            "MagUp": 47,
        },
        "2016": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "pPb": {
        "2013": {
            "MagDown": 72,
            "MagUp": 28,
        },
        "2016": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2018": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "PbPb": {
        "2015": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2018": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2024": {
            "MagDown": 0,
            "MagUp": 100,
        },
    },
    # proton beams + Hydrogen SMOG
    "pH2": {
        "2024.Q1.2": {
            "MagDown": 16,
            "MagUp": 84,
        },
        # Block 1
        "2024.W32.34": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 2
        "2024.W31": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 3
        "2024.W29.30": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 5
        "2024.W35.37": {
            "MagDown": 0,
            "MagUp": 100,
        },
    },
    "H2p": {
        "2024.Q1.2": {
            "MagDown": 16,
            "MagUp": 84,
        },
        # Block 1
        "2024.W32.34": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 2
        "2024.W31": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 3
        "2024.W29.30": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 5
        "2024.W35.37": {
            "MagDown": 0,
            "MagUp": 100,
        },
    },
    "ppH2": {
        "2024.Q1.2": {
            "MagDown": 16,
            "MagUp": 84,
        },
        # Block 1
        "2024.W32.34": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 2
        "2024.W31": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 3
        "2024.W29.30": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 5
        "2024.W35.37": {
            "MagDown": 0,
            "MagUp": 100,
        },
    },
    # proton beams + Argon SMOG
    "pAr": {
        "2015": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2022": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2024.Q1.2": {
            "MagDown": 20,
            "MagUp": 80,
        },
        # Block 1
        "2024.W32.34": {
            "MagDown": 0,
            "MagUp": 100,
        },
    },
    "Arp": {
        "2022": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2024.Q1.2": {
            "MagDown": 20,
            "MagUp": 80,
        },
        # Block 1
        "2024.W32.34": {
            "MagDown": 0,
            "MagUp": 100,
        },
    },
    "ppAr": {
        "2022": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2024.Q1.2": {
            "MagDown": 20,
            "MagUp": 80,
        },
        # Block 1
        "2024.W32.34": {
            "MagDown": 0,
            "MagUp": 100,
        },
    },
    # proton beams + Helium SMOG
    "pHe": {
        "2016": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2016-13TeV": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2024.Q1.2": {
            "MagDown": 82,
            "MagUp": 18,
        },
        # Block 3
        "2024.W29.30": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 4
        "2024.W25.27": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "Hep": {
        "2024.Q1.2": {
            "MagDown": 82,
            "MagUp": 18,
        },
        # Block 3
        "2024.W29.30": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 4
        "2024.W25.27": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "ppHe": {
        "2024.Q1.2": {
            "MagDown": 82,
            "MagUp": 18,
        },
        # Block 3
        "2024.W29.30": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 4
        "2024.W25.27": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    # proton beams + Neon SMOG
    "pNe": {
        "2017": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2017-13TeV": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2024.Q1.2": {
            "MagDown": 100,
            "MagUp": 0,
        },
        # Block 5
        "2024.W35.37": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 6
        "2024.W37.39": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "Nep": {
        "2024.Q1.2": {
            "MagDown": 100,
            "MagUp": 0,
        },
        # Block 5
        "2024.W35.37": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 6
        "2024.W37.39": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "ppNe": {
        "2024.Q1.2": {
            "MagDown": 100,
            "MagUp": 0,
        },
        # Block 5
        "2024.W35.37": {
            "MagDown": 0,
            "MagUp": 100,
        },
        # Block 6
        "2024.W37.39": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    # Lead beams + Argon SMOG
    "PbAr": {
        "2015": {
            "MagDown": 100,
            "MagUp": 0,
        },
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "ArPb": {
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    "PbPbAr": {
        "2023": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
    # Lead beams + Neon SMOG
    "PbNe": {
        "2018": {
            "MagDown": 100,
            "MagUp": 0,
        },
    },
}


def get_sample_polarities(data_type: str, collision_type: str) -> Iterable[str]:
    try:
        lumi_split = LUMI_SPLIT[collision_type][data_type]
    except KeyError as e:
        msg = f"Default magnet polarities not known for {data_type} {collision_type}."
        if data_type.startswith("expected"):
            print(f"{msg} Assuming both MagUp and MagDown.")
            lumi_split = {"MagUp": 50, "MagDown": 50}
        else:
            raise NotImplementedError(
                " ".join(
                    [
                        msg,
                        'Please specify them with the "magnet-polarities" key',
                        f"or add them to the LUMI_SPLIT dict in {__file__}",
                    ]
                )
            ) from e

    for polarity, fraction in lumi_split.items():
        if fraction > 0:
            yield polarity


def get_lumi_weights(
    collision_type: str, data_types: list[str], magnet_polarities: list[str]
) -> dict[str, float]:
    if not magnet_polarities:
        magnet_polarities = ["MagUp", "MagDown"]
    lumi_weights = {
        data_type: {
            pol: LUMI_SPLIT[collision_type][data_type][pol] for pol in magnet_polarities
        }
        for data_type in data_types
    }
    total_lumi = sum(lw for ws in lumi_weights.values() for lw in ws.values())
    for data_type in data_types:
        for pol in magnet_polarities:
            lumi_weights[data_type][pol] /= total_lumi
    return lumi_weights
