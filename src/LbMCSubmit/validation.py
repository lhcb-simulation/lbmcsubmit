###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from LbMCSubmit.datapkgs import get_version_from_step, options_file_exists
from LbMCSubmit.eventtypes import (
    check_generator,
    check_pgun_spectrum_file,
    decfiles_path,
)
from LbMCSubmit.versioning import get_version


def validate_all(data: dict, steps: list[dict]):
    "Run all validation functions"
    check_eventtype_generator(data, steps)
    check_optsfiles(data, steps)
    check_step_io_types(steps)
    if (prod_tool := data["generation"]["production-tool"]) in {
        "Particle Gun",
        "Madgraph",
        "Powheg",
    }:
        check_generator_data(data, steps, prod_tool)


def get_versions(data: dict, steps: list[dict], prod_tool: str = "Pythia8") -> dict:
    """Dictionary of package/application versions used in checks
    NB: in theory, the AppConfig version can vary per step
    """
    versions = {
        "DecFiles": get_version_from_step(steps[0], "DecFiles"),
        "Gauss": get_version(steps[0]["application"]),
        "AppConfig": get_version_from_step(steps[0], "AppConfig"),
    }
    if prod_tool == "Particle Gun":
        versions["PGunsData"] = get_version_from_step(steps[0], "PGunsData")
    elif prod_tool == "Madgraph":
        versions["MadgraphData"] = get_version_from_step(steps[0], "MadgraphData")
    elif prod_tool == "Powheg":
        versions["PowhegboxData"] = get_version_from_step(steps[0], "PowhegboxData")
    if data_pkgs := data.get("stripping", {}).get("data-pkgs"):
        for data_pkg in data_pkgs:
            name, version = data_pkg.split(".")
            versions[name] = version
    return versions


def check_eventtype_generator(data: dict, steps: list[dict]):
    "Event type -- Generator combination check"
    prod_tool = data["generation"]["production-tool"]
    versions = get_versions(data, steps, prod_tool)
    for event_type in data["event-types"]:
        if not check_generator(versions["DecFiles"], event_type, prod_tool):
            path = decfiles_path(versions["DecFiles"]) / "options" / f"{event_type}.py"
            raise NotImplementedError(
                f"Event type {event_type} does not seem to be configured for "
                f"{prod_tool} in DecFiles {versions['DecFiles']}.\n"
                f"Please see the options file: {path}"
            )


def _expand_event_types(opts_files: list[str], event_types: list[str]):
    pattern = "@{eventType}"
    for filename in opts_files:
        if pattern in filename:
            for event_type in event_types:
                yield filename.replace(pattern, event_type)
        else:
            yield filename


def check_optsfiles(data: dict, steps: list[dict]):
    "Check that all options files exist in all steps"
    prod_tool = data["generation"]["production-tool"]
    versions = get_versions(data, steps, prod_tool)
    for step in steps:
        if any("AppConfig" in pkg for pkg in step["data_pkgs"]):
            versions["AppConfig"] = get_version_from_step(step, "AppConfig")
        if isinstance(step["options"], list):
            for path in _expand_event_types(step["options"], data["event-types"]):
                options_file_exists(path, versions)


def check_generator_data(data: dict, steps: list[dict], prod_tool: str):
    versions = get_versions(data, steps, prod_tool)
    beam_energy = (
        data["generation"]["beam"]["energy"].removeprefix("Beam").removesuffix("GeV")
    )
    if prod_tool == "Madgraph":
        platform = data["generation"]["application"]["binary_tag"]
        for event_type in data["event-types"]:
            path = "/".join(
                [
                    "$MADGRAPHDATAROOT",
                    "gridpacks",
                    platform,
                    f"{event_type}_{beam_energy}_{beam_energy}.tgz",
                ]
            )
            options_file_exists(path, versions)
    elif prod_tool == "Powheg":
        for event_type in data["event-types"]:
            path = "/".join(
                [
                    "$POWHEGBOXDATAROOT",
                    "gridpacks",
                    f"{event_type}_{beam_energy}_{beam_energy}.tgz",
                ]
            )
            options_file_exists(path, versions)
    elif prod_tool == "Particle Gun":
        spectrum_source = data["generation"]["options"]["PGun"]
        versions = get_versions(data, steps, prod_tool)
        beam_energy = data["generation"]["beam"]["energy"]
        for event_type in data["event-types"]:
            check_pgun_spectrum_file(
                versions["DecFiles"],
                versions["PGunsData"],
                event_type,
                spectrum_source,
                beam_energy,
            )
    else:
        raise NotImplementedError(f"Data-file checking not implemented for {prod_tool}")


def check_step_io_types(steps: list[dict]):
    "Check agreement between the input and output filetypes of consecutive steps"
    for i, step in enumerate(steps[:-1]):
        this_output_types = {f["type"] for f in step["output"]}
        next_input_types = {f["type"] for f in steps[i + 1]["input"]}
        if not this_output_types & next_input_types:
            raise RuntimeError(
                f"No overlap between step {i+1} output ({this_output_types})"
                f" and step {i+2} input ({next_input_types})"
            )
