###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import re
from fnmatch import fnmatch
from functools import cache
from pathlib import Path
from typing import Union

import requests

from .utils import flatten_app_name

ALLOWED_PATTERNS = re.compile(r"v(?:\d+r(?:\d+p)?)?\*")
assert ALLOWED_PATTERNS.fullmatch("v*")
assert ALLOWED_PATTERNS.fullmatch("v31r*")
assert ALLOWED_PATTERNS.fullmatch("v31r1p*")
assert not ALLOWED_PATTERNS.fullmatch("*")
assert not ALLOWED_PATTERNS.fullmatch("v31r*p")
assert not ALLOWED_PATTERNS.fullmatch("v31r*p*")
VERSION_PATTERN = re.compile(r"v(\d+)r(\d+)(?:p(\d+))?")

CVMFS_BASE = Path("/cvmfs/lhcb.cern.ch/lib")
CVMFS_PREFIX = CVMFS_BASE / "lhcb"
TMP_PROJINFO = Path("/tmp/LbMCSubmit/projects_info.json")


def latest_available(
    path_prefix: str, version_prefix: str = "", pattern: str = "v*"
) -> str:
    assert ALLOWED_PATTERNS.fullmatch(pattern), "Value for pattern is invalid"
    path = CVMFS_PREFIX / path_prefix
    if not path.is_dir():
        raise NotImplementedError(
            f"{path} is not a directory. Is /cvmfs/lhcb.cern.ch mounted?"
        )
    matches = {}
    for version_path in path.iterdir():
        if not version_path.is_dir():
            continue
        version = version_path.name.removeprefix(version_prefix)
        if match := VERSION_PATTERN.fullmatch(version):
            if not fnmatch(version, pattern):
                continue
            version_segments = tuple(0 if x is None else int(x) for x in match.groups())
            matches[version_segments] = version
    return max(matches.items())[1]


def latest_datapackage(package: str, pattern: str = "v*") -> str:
    return latest_available(f"DBASE/{package}", pattern=pattern)


def latest_application(package: str, pattern: str = "v*") -> str:
    return latest_available(f"{package.upper()}", f"{package.upper()}_", pattern)


def get_version(app: Union[str, dict]) -> str:
    if isinstance(app, str):
        # Assume we're being passed something like "Gauss/v49r21" or "v49r21"
        ver = app.split("/", 1)[-1]
    elif isinstance(app, dict):
        ver = app["version"]
    else:
        raise NotImplementedError(f"{app=} is neither a dict nor a str")
    return ver


def get_major_version(app: Union[str, dict]) -> str:
    "Get the major version of an LHCb Gaudi app according to the pattern vX[rY[pZ]]"
    ver = get_version(app)
    return re.match(r"v(\d+)", ver).group(1)


@cache
def _get_projects_info(try_local: bool = True):
    if try_local and TMP_PROJINFO.is_file():
        with open(TMP_PROJINFO, "r", encoding="utf8") as f:
            proj_info = json.load(f)
    elif CVMFS_BASE.is_dir():
        with open(
            CVMFS_BASE / "var/lib/softmetadata/projects-info.json", "r", encoding="utf8"
        ) as f:
            proj_info = json.load(f)
    else:
        # XXX: careful! this can end up making a lot of HTTPS requests to the
        # CERN GitLab if lb-mc is called many times without /cvmfs available
        # Call lb-mc-dump-proj-info first to download it once to /tmp
        artifact_url = "https://gitlab.cern.ch/{repo}/-/jobs/artifacts/{ref}/raw/{filename}?job={job}".format(
            **{
                "repo": "lhcb-core/lhcbstacks",
                "ref": "master",
                "filename": "projects_info.json",
                "job": "generate_projects_info",
            }
        )
        print(f"Fetching {artifact_url}")
        response = requests.get(artifact_url, allow_redirects=True, timeout=10)
        if (status_code := response.status_code) != 200:
            raise RuntimeError(
                f"Encountered status code {status_code} while fetching {artifact_url}"
            )
        content = response.content.decode("utf-8")
        proj_info = json.loads(content)
    assert "GAUSS" in proj_info
    return proj_info


def dump_projects_info():
    TMP_PROJINFO.parent.mkdir(parents=True, exist_ok=True)
    print(f"Writing to {TMP_PROJINFO}")
    with open(TMP_PROJINFO, "w", encoding="utf8") as f:
        json.dump(_get_projects_info(False), f)


def get_app_info(step: dict) -> dict:
    app_name, app_ver = flatten_app_name(step["application"]).split("/")
    try:
        app_info = _get_projects_info()[app_name.upper()][app_ver]
    except KeyError as e:
        raise RuntimeError(
            f"KeyError encountered while looking up info for {app_name}/{app_ver}"
        ) from e
    return app_info
