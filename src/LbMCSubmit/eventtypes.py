###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
from functools import lru_cache

from LbMCSubmit.datapkgs import CVMFS_PATHS, CVMFS_PREFIX


@lru_cache(maxsize=1024)
def decfiles_path(decfiles_version: str) -> str:
    if not CVMFS_PREFIX.is_dir():
        raise NotImplementedError("/cvmfs/lhcb.cern.ch is not mounted")
    path = CVMFS_PATHS["DECFILESROOT"]({"DecFiles": decfiles_version})
    if not path.is_dir():
        raise ValueError(
            f"DecFiles {decfiles_version} does not appear to be installed on CVMFS"
        )
    return path


# TODO: generalise this
@lru_cache(maxsize=1024)
def pguns_data_path(pguns_data_version: str) -> str:
    if not CVMFS_PREFIX.is_dir():
        raise NotImplementedError("/cvmfs/lhcb.cern.ch is not mounted")
    path = CVMFS_PATHS["PGUNSDATAROOT"]({"PGunsData": pguns_data_version})
    if not path.is_dir():
        raise ValueError(
            f"PGunsData {pguns_data_version} does not appear to be installed on CVMFS"
        )
    return path


@lru_cache(maxsize=1024)
def available(decfiles_version: str) -> list[str]:
    path = decfiles_path(decfiles_version)
    result = []
    for option_path in (path / "options").iterdir():
        if not option_path.is_file():
            continue
        if match := re.fullmatch(r"(\d+).py", option_path.name):
            (eventtype,) = match.groups()
            result += [eventtype]
    return result


def check_generator(  # pylint: disable=too-many-return-statements
    decfiles_version: str, event_type: str, generator: str
) -> bool:
    # All decfiles compatible with Pythia also work with Epos
    if generator == "Epos":
        return check_generator(decfiles_version, event_type, "Pythia8")
    if generator == "Particle Gun":  # Every DecFile should work with Particle Gun
        return True
    if event_type.startswith("5"):
        # EventTypes beginning with 5 only work with Particle Gun
        return generator == "Particle Gun"
    path = decfiles_path(decfiles_version) / "options" / f"{event_type}.py"
    #  6479 files in DecFiles v30r58 have 1 occurrence of ".ProductionTool = "
    #  However, 71 have >1, so we just take the first
    with open(path, "r", encoding="utf8") as f:
        if generator in {"Madgraph", "Powheg"}:
            sample_generation_pattern = re.compile(
                r"""SampleGenerationTool\s*=\s*['"]Special["']"""
            )
            sg_options_pattern = re.compile(
                r"""['"]Generator["']\s*:\s*['"]""" + generator + r"""["']"""
            )
            optsfile = f.read()
            if all(
                [
                    len(re.findall(sample_generation_pattern, optsfile)) > 0,
                    len(re.findall(sg_options_pattern, optsfile)) > 0,
                ]
            ):
                return True
        else:
            regex_pattern = re.compile(
                r"""ProductionTool\s*=\s*['"]""" + generator + r"""Production\w*["']"""
            )
            matches = re.findall(regex_pattern, f.read())
            if len(matches) > 0:
                return True
    if (
        generator.startswith("Pythia") and generator != "Pythia"
    ):  # TODO: and decfiles_version below some threshold
        return check_generator(decfiles_version, event_type, "Pythia")
    return False


def check_pgun_spectrum_file(
    decfiles_version: str,
    pguns_data_version: str,
    event_type: str,
    source: str,
    energy: str,
):
    decfile_path = decfiles_path(decfiles_version) / "options" / f"{event_type}.py"
    with open(decfile_path, "r", encoding="utf8") as f:
        signal_pid_code_pattern = re.compile(r"""SignalPdgCode = ([\-\d]+)""")
        optsfile = f.read()
        signal_pid_code_match = re.search(signal_pid_code_pattern, optsfile)
        signal_pid = int(signal_pid_code_match.group(1))
        datafile = (
            pguns_data_path(pguns_data_version)
            / "data"
            / energy.replace("Beam", "Ebeam")
            / source.replace("CollisionsPP", "ppCollisions")
            / f"MomentumSpectrum_{signal_pid}.root"
        )
        if not datafile.is_file():
            raise FileNotFoundError(datafile)
