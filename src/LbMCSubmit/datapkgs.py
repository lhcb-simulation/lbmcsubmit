###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
from pathlib import Path

from .organisation import _wg_config_name, working_groups
from .versioning import CVMFS_PREFIX, latest_datapackage

CVMFS_PATHS = {
    # Assume v is a dict containing {package_name: version} strings
    "DECFILESROOT": lambda v: CVMFS_PREFIX / f"DBASE/Gen/DecFiles/{v['DecFiles']}",
    "APPCONFIGOPTS": lambda v: CVMFS_PREFIX
    / f"DBASE/AppConfig/{v['AppConfig']}/options",  # noqa: W503
    "PGUNSDATAROOT": lambda v: CVMFS_PREFIX / f"DBASE/Gen/PGunsData/{v['PGunsData']}",
    "PGUNSDATAOPTS": lambda v: CVMFS_PREFIX
    / f"DBASE/Gen/PGunsData/{v['PGunsData']}/options",
    "MADGRAPHDATAROOT": lambda v: CVMFS_PREFIX
    / f"DBASE/Gen/MadgraphData/{v['MadgraphData']}",  # noqa: W503
    "POWHEGBOXDATAROOT": lambda v: CVMFS_PREFIX
    / f"DBASE/Gen/PowhegboxData/{v['PowhegboxData']}",  # noqa: W503
    # The below all take the Gauss version
    "GAUSSOPTS": lambda v: CVMFS_PREFIX / f"GAUSS/GAUSS_{v['Gauss']}/Sim/Gauss/options",
    "LBCRMCROOT": lambda v: CVMFS_PREFIX / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbCRMC",
    "LBPGUNSROOT": lambda v: CVMFS_PREFIX / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbPGuns",
    "LBMADGRAPHROOT": lambda v: CVMFS_PREFIX
    / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbMadgraph",  # noqa: W503
    "LBPOWHEGROOT": lambda v: CVMFS_PREFIX
    / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbPowheg",  # noqa: W503
    "LBPYTHIA8ROOT": lambda v: CVMFS_PREFIX / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbPythia8",
    "LBBCVEGPYROOT": lambda v: CVMFS_PREFIX / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbBcVegPy",
    "LBGENXICCROOT": lambda v: CVMFS_PREFIX / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbGenXicc",
    "LBSUPERCHIC2ROOT": lambda v: CVMFS_PREFIX
    / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbSuperChic2",  # noqa: W503
    "LBSTARLIGHTROOT": lambda v: CVMFS_PREFIX
    / f"GAUSS/GAUSS_{v['Gauss']}/Gen/LbStarLight",  # noqa: W503
}


def add_wg_package_paths():
    for wg in working_groups:
        wgconfig_name = _wg_config_name(wg)
        package_name = f"WG/{wgconfig_name}Config"
        env_var = f"{wgconfig_name.upper()}CONFIGOPTS"

        def make_lambda(_package):
            return lambda v: CVMFS_PREFIX / "DBASE" / _package / v[_package] / "options"

        CVMFS_PATHS[env_var] = make_lambda(package_name)


add_wg_package_paths()


def options_file_exists(path: str, versions: dict) -> bool:
    match = re.match(r"\$([a-zA-Z_][a-zA-Z_0-9]*)", path)
    try:
        optsfile = Path(path.replace(match[0], str(CVMFS_PATHS[match[1]](versions))))
    except TypeError as e:
        raise ValueError(
            f"The option file path {path} does not begin with "
            "an environment variable."
        ) from e
    except KeyError as e:
        raise KeyError(
            f"The environment variable {match[1]} is not known to LbMCSubmit, "
            f"or the package version is missing from {versions=}."
        ) from e
    if not optsfile.is_file():
        raise FileNotFoundError(optsfile)


def get_version_from_step(step: dict, package_name: str) -> str:
    matching_packages = [x for x in step["data_pkgs"] if package_name in x]
    if len(matching_packages) != 1:
        raise NotADirectoryError(matching_packages)
    _app, ver = matching_packages[0].split(".")
    return ver


def wg_config(wg: str, version: str = None) -> str:
    name = _wg_config_name(wg)
    package = f"WG/{name}Config"
    if version is None:
        version = latest_datapackage(package)
    return f"{package}.{version}"


appconfig = f"AppConfig.{latest_datapackage('AppConfig', 'v3r*')}"
