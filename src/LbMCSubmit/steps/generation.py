###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

from LbMCSubmit import datapkgs
from LbMCSubmit.properties import Stage6Properties
from LbMCSubmit.utils import make_step, run_from_data_type, to_pol_str


def get_steps(properties: Stage6Properties, gen: dict, fast_mc: dict, db_tags: dict):
    app = gen["application"]
    data_pkgs = [datapkgs.appconfig, f"Gen/DecFiles.{properties.decfiles}"]
    genlvl = properties.file_format.endswith("GEN")

    name = [
        properties.sim_version,
        properties.data_type,
        gen["beam"]["energy"],
        properties.polarity,
        gen["production-tool"],
        # TODO: also fast MC type
    ]
    processing_pass = [properties.sim_version]
    conditions = []
    options = []
    extra_options = gen.get("options", {})
    options += _beam_options(
        properties, gen["beam"], extra_options, conditions, fast_mc
    )
    # TODO if "$GAUSSOPTS/BeforeVeloGeometry.py" is added or not should depend
    # on the PVZ parameter of smog, which has not been implemented yet.
    if properties.smog is True:
        options += ["$GAUSSOPTS/BeforeVeloGeometry.py"]
    elif properties.smog is not False:
        # XXX: assert isinstance(properties.smog, bool) surely?
        raise NotImplementedError(properties.smog)
    # For generator-only we want the spillover in the conditions string, but no
    # spillover options files in the step
    spillover_options = list(_spillover_options(gen["beam"]["spillover"], conditions))
    if not genlvl:
        options += spillover_options

    if properties.data_type == "2022":
        options += ["$APPCONFIGOPTS/Gauss/Run3-detector-NoUT.py"]
    elif properties.data_type in {
        "2023",
        "2024.Q1.2",
        "2024.W25.27",
        "2024.W29.30",
        "2024.W31.34",
        "2024.W31",
        "2024.W32.34",
        "2024.W35.37",
        "2024.W37.39",
        "2024.W40.42",
        "expected-2024-5TeV",
        "expected-2024.Q3.4",
        "2024",
    }:
        options += ["$APPCONFIGOPTS/Gauss/Run3-detector.py"]

    if properties.collision_type in {"PbNe", "PbPb"} and properties.data_type == "2018":
        options += ["$APPCONFIGOPTS/Gauss/DataType-2017.py"]
    else:
        dataYear = re.sub(r"(.*)(\d{4})(.*)", r"\2", properties.data_type)
        options += [f"$APPCONFIGOPTS/Gauss/DataType-{dataYear}.py"]

    if not genlvl and run_from_data_type(properties.data_type) in {1, 2}:
        options += ["$APPCONFIGOPTS/Gauss/RICHRandomHits.py"]
    if properties.data_type in {"2011", "2012", "2013"}:
        options += ["$APPCONFIGOPTS/Gauss/NoPacking.py"]
    options += _decay_options(gen["decay-tool"])
    options += _fast_mc_options(properties, fast_mc, processing_pass)
    options += _IFT_collision_modes(properties, gen["production-tool"])
    options += _prod_options(
        properties,
        gen["production-tool"],
        extra_options,
        data_pkgs,
        conditions,
        processing_pass,
    )
    if genlvl:
        options += ["$GAUSSOPTS/GenStandAlone.py"]
    else:
        options += _material_options(properties, gen["material-tool"], fast_mc)

    step_output = "SIM"
    if properties.data_type == "2015" and properties.collision_type == "PbPb":
        # See https://its.cern.ch/jira/browse/LHCBGAUSS-1050
        step_output = "XSIM"
    if any(properties.file_format.endswith(x) for x in ["GEN", "SIM"]):
        step_output = properties.file_format

    if step_output == "XSIM":
        options += ["$APPCONFIGOPTS/Gauss/xsim.py"]
    properties.conditions_string = "-".join(conditions)

    # See https://its.cern.ch/jira/browse/LHCBGAUSS-2828
    # keep memory under control in Sim10b, should be unecessary in later versions
    if properties.sim_version.startswith("Sim10"):
        options += ["$APPCONFIGOPTS/Persistency/BasketSize-10.py"]

    # FIXME: Disable TCMalloc due to Gauss#140
    if gen["production-tool"] == "Powheg":
        disable_ld_preload = "\n".join(
            [
                r"import os,sys",
                r"if 'LD_PRELOAD' in os.environ:",
                r"    del os.environ['LD_PRELOAD']",
                r"    print('Restarting without LD_PRELOAD')",
                r"    os.execv(sys.executable, [sys.executable] + sys.argv)",
            ]
        )
        options = {"files": options, "gaudi_extra_options": disable_ld_preload}

    yield make_step(
        " - ".join(name),
        "-".join(processing_pass),
        data_pkgs,
        app,
        options,
        [],
        [step_output],
        db_tags=db_tags,
    )


def _beam_options(
    properties: Stage6Properties,
    beam: dict,
    extra_options: dict,
    conditions: list[str],
    fast_mc: dict,
):
    segments = []
    if all(
        [
            properties.data_type in {"2011", "2012"},
            properties.collision_type != "uniformHeadOn",
        ]
    ):
        segments += ["Sim08"]

    extra_opts_files = []
    # Move `Beam` in `f"Beam{beam['energy']}"` to the dicts in `beam_parameters.py`,
    # To be consistent with the string in BK of "0GeV-Beam6800GeV" instead of "Beam0GeV-Beam6800GeV"
    energy = f"{beam['energy']}"
    datatype = properties.data_type
    Beam_pID = properties.collision_type
    # Beam setting for fixed-target collisions in Run3
    pp_nu_mapping = {
        "2022": "nu2.1",
        "2023": "nu1.6",
        "2024.Q1.2": "nu7.6",
        "expected-2024": "nu2.9",  # 5TeV
        "2024.W25.27": "nu6.3",  # Block 4
        "2024.W29.30": "nu6.3",  # Block 3
        "2024.W31": "nu6.3",  # Block 2
        "2024.W32.34": "nu6.3",  # Block 1
        "2024.W35.37": "nu6.3",  # Block 5
        "2024.W37.39": "nu6.3",  # Block 6
    }

    pp_beamE_mapping = {
        "2022": "Beam6800GeV",
        "2023": "Beam6800GeV",
        "2024.Q1.2": "Beam6800GeV",
        "2024.W25.27": "Beam6800GeV",  # Block 4
        "2024.W29.30": "Beam6800GeV",  # Block 3
        "2024.W31": "Beam6800GeV",  # Block 2
        "2024.W32.34": "Beam6800GeV",  # Block 1
        "2024.W35.37": "Beam6800GeV",  # Block 5
        "2024.W37.39": "Beam6800GeV",  # Block 6
        "expected-2024": "Beam2680GeV",  # 5TeV
    }

    if datatype == "2016" and Beam_pID in {"Pbp", "pPb"}:
        datatype = f"pre{datatype}"
    if datatype in {"2015-5TeV", "2017-5TeV", "expected-2024-5TeV"}:
        datatype = f"{datatype}".removesuffix("-5TeV")

    if Beam_pID not in {"pp", "uniformHeadOn"}:
        conditions += [Beam_pID]
        segments += [Beam_pID]

    if Beam_pID == "uniformHeadOn":
        segments += [energy, Beam_pID]

    # BeamGas(+BeamBeam) in Run3
    elif properties.smog is True and datatype in {
        "2022",
        "2023",
        "2024.Q1.2",
        "2024.W25.27",  # Block 4
        "2024.W29.30",  # Block 3
        "2024.W31",  # Block 2
        "2024.W32.34",  # Block 1
        "2024.W35.37",  # Block 5
        "2024.W37.39",  # Block 6
        "expected-2024",  # 5TeV
        "2024",  # PbPb 5.36TeV
    }:
        if Beam_pID.startswith("p") or Beam_pID == "Arp":
            segments = [
                pp_beamE_mapping[datatype],
                to_pol_str(properties.polarity),
                datatype,
                pp_nu_mapping[datatype],
            ]
        elif Beam_pID.startswith("Pb") or Beam_pID == "ArPb":
            segments = [
                "PbPb-Beam2680GeV",
                to_pol_str(properties.polarity),
                datatype,
                "fix1",
            ]
        if Beam_pID in {"pAr", "Arp", "ppAr"}:
            if datatype == "2022":
                extra_opts_files += [
                    "$APPCONFIGOPTS/Gauss/pAr-Beam6800GeV-0GeV-md100-2022-0.37.py"
                ]
            elif datatype == "2023":
                extra_opts_files += [
                    "$APPCONFIGOPTS/Gauss/pAr-Beam6800GeV-0GeV-md100-expected-2023-0.37.py"
                ]
            elif datatype == "2024.W32.34":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Argon-2024.W32.34-nu0.025.py"
                ]
            else:
                extra_opts_files += ["$APPCONFIGOPTS/Gauss/SMOG-Argon-nu0.37.py"]

        elif Beam_pID in {"PbAr", "ArPb", "PbPbAr"}:
            extra_opts_files += ["$APPCONFIGOPTS/Gauss/SMOG-Argon-nu0.37.py"]
        elif Beam_pID in {"pHe", "ppHe", "PbHe", "PbPbHe"}:
            if datatype == "2024.W29.30":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Helium-2024.W29.30-nu0.007.py"
                ]
            elif datatype == "2024.W25.27":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Helium-2024.W25.27-nu0.007.py"
                ]
            else:
                extra_opts_files += ["$APPCONFIGOPTS/Gauss/SMOG-Helium-nu0.2.py"]
        elif Beam_pID in {"pH2", "ppH2", "PbH2", "PbPbH2"}:
            if datatype == "2024.W31":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Hydrogen-2024.W31-nu0.025.py"
                ]
            elif datatype == "2024.W32.34":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Hydrogen-2024.W32.24-nu0.029.py"
                ]
            elif datatype == "2024.W29.30":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Hydrogen-2024.W29.30-nu0.031.py"
                ]
            elif datatype == "2024.W35.37":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Hydrogen-2024.W35.37-nu0.027.py"
                ]
            else:
                extra_opts_files = ["$APPCONFIGOPTS/Gauss/SMOG-Hydrogen-nu0.087.py"]
        elif Beam_pID in {"pD2", "ppD2", "PbD2", "PbPbD2"}:
            extra_opts_files = ["$APPCONFIGOPTS/Gauss/SMOG-Deuteron-nu0.08.py"]
        elif Beam_pID in {"pN2", "ppN2", "PbN2", "PbPbN2"}:
            extra_opts_files = ["$APPCONFIGOPTS/Gauss/SMOG-Nitrogen-nu0.168.py"]
        elif Beam_pID in {"pO2", "ppO2", "PbO2", "PbPbO2"}:
            extra_opts_files = ["$APPCONFIGOPTS/Gauss/SMOG-Oxygen-nu0.185.py"]
        elif Beam_pID in {"pNe", "ppNe", "PbNe", "PbPbNe"}:
            if datatype == "2024.W35.37":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Neon-2024.W35.37-nu0.192.py"
                ]
            elif datatype == "2024.W37.39":
                extra_opts_files = [
                    "$APPCONFIGOPTS/Gauss/SMOG-Neon-2024.W37.39-nu0.052.py"
                ]
            else:
                extra_opts_files = ["$APPCONFIGOPTS/Gauss/SMOG-Neon-nu0.43.py"]
        elif Beam_pID in {"pKr", "ppKr", "PbKr", "PbPbKr"}:
            extra_opts_files = ["$APPCONFIGOPTS/Gauss/SMOG-Krypton-nu0.447.py"]
        elif Beam_pID in {"pXe", "ppXe", "PbXe", "PbPbXe"}:
            extra_opts_files = ["$APPCONFIGOPTS/Gauss/SMOG-Xenon-nu0.446.py"]

    elif datatype[-3:] == "-vo":
        segments += [
            energy,
            to_pol_str(properties.polarity),
            datatype[:-3],
            f"{beam['PileUp']}".lower(),
        ]
    elif datatype == "2024.Q1.2":
        segments += [
            energy,
            to_pol_str(properties.polarity),
            "2024.Q1.2",
            f"{beam['PileUp']}".lower(),
        ]
    else:
        segments += [
            energy,
            to_pol_str(properties.polarity),
            datatype,
            f"{beam['PileUp']}".lower(),
        ]

    if opt := extra_options.get("Beam"):
        if re.match(r"(.*)PV(.*)Z(.*)", opt):
            beam["PrimaryvertexLocation"] = opt
        elif re.match(r"(.*)SMOG2", opt):
            beam["SpecialDetectorConfigurations"] = opt
        elif re.match("CrossingAngle", opt):
            beam["SpecialDetectorConfigurations"] = opt
            beam["beamfile-suffix"] = "v2"

    if opt := extra_options.get("EPOS"):
        if re.match(r"Fix\d", opt):
            beam["PileUp"] = opt

    if fast_mc.get("tracker-only", False):
        beam["SpecialDetectorConfigurations"] = "TrackerOnly"

    if f"{beam['beamfile-suffix']}":
        segments += [f"{beam['beamfile-suffix']}"]

    conditions += [energy]

    if datatype.endswith("-vo"):
        conditions += [datatype.removesuffix("-vo")]
    elif datatype == "pre2016":
        conditions += [datatype.removeprefix("pre")]
    else:
        conditions += [datatype]

    if f"{beam['PrimaryvertexLocation']}":
        conditions += [f"{beam['PrimaryvertexLocation']}"]

    conditions += [properties.polarity]

    if f"{beam['SpecialDetectorConfigurations']}":
        conditions += [f"{beam['SpecialDetectorConfigurations']}"]

    if properties.pgun:
        conditions += ["Fix1"]
    elif f"{beam['PileUp']}":
        conditions += [f"{beam['PileUp']}"]

    yield f"$APPCONFIGOPTS/Gauss/{'-'.join(segments)}.py"
    yield from extra_opts_files

    if opt := extra_options.get("Beam"):
        yield {
            "PVDZ1300mm": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOG_pHe.py",
            "PVZpm250mm": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOG.py",
            "leftPVZ": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOG_leftPVZ.py",
            "rightPVZ": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOG_rightPVZ.py",
            "reducedPVZ": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOG_pNe_reducedPVZ.py",
            "SMOGBeam1": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOGBeam1_2m.py",
            "SMOGBeam2": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOGBeam2_2m.py",
            "SMOGBeam1and2": "$APPCONFIGOPTS/Gauss/VertexSmear_SMOGBeam1and2_2m.py",
            "SMOG2": "$APPCONFIGOPTS/Gauss/Smog2TriangularProfile.py",
            "andSMOG2": "$APPCONFIGOPTS/Gauss/Smog2TriangularProfile.py",
        }[opt]


def _spillover_options(spillover: dict, conditions: list[str]):
    if not spillover:
        return
    conditions += [spillover["spacing"]]
    yield f"$APPCONFIGOPTS/Gauss/EnableSpillover-{spillover['spacing']}.py"


def _decay_options(decay_tool: str):
    if decay_tool == "EvtGen":
        yield "$DECFILESROOT/options/@{eventType}.py"
    else:
        raise NotImplementedError(decay_tool)


# for multiple generation of BeamBeam, BeamGas and BeamGas+BeamBeam
# currently used for EPOS in Sim10
def _IFT_collision_modes(properties: Stage6Properties, prod_tool: str):
    if properties.sim_version.startswith("Sim10") and prod_tool == "Epos":
        if properties.collision_type in {"PbPb"}:
            yield "$GAUSSOPTS/BeamBeam.py"
        elif properties.collision_type in {
            "pAr",
            "pHe",
            "pNe",
            "pH2",
            "pD2",
            "pN2",
            "pO2",
            "pKr",
            "pXe",
            "PbAr",
            "PbNe",
            "PbHe",
            "PbH2",
            "PbD2",
            "PbN2",
            "PbO2",
            "PbKr",
            "PbXe",
        }:
            yield "$GAUSSOPTS/BeamGas.py"
        elif properties.collision_type in {"Arp", "ArPb"}:
            yield "$APPCONFIGOPTS/Gauss/GasBeam.py"
        elif properties.collision_type in {
            "ppAr",
            "ppHe",
            "ppH2",
            "ppD2",
            "ppN2",
            "ppO2",
            "ppKr",
            "ppXe",
            "ppNe",
            "PbPbAr",
            "PbPbNe",
            "PbPbH2",
            "PbPbD2",
            "PbPbN2",
            "PbPbO2",
            "PbPbHe",
            "PbPbKr",
            "PbPbXe",
        }:
            yield "$GAUSSOPTS/BeamGasWithBeamBeam.py"


def _prod_options(
    properties: Stage6Properties,
    prod_tool: str,
    extra_options: dict,
    data_pkgs: list[str],
    conditions: list[str],
    processing_pass: list,
):
    if prod_tool == "Pythia8":
        conditions += ["Pythia8"]
        if properties.data_type == "2011":
            yield "$LBPYTHIA8ROOT/options/Pythia8_7TeV.py"
        else:
            yield "$LBPYTHIA8ROOT/options/Pythia8.py"
    elif prod_tool == "BcVegPy":
        conditions += ["BcVegPyPythia8"]
        yield "$LBBCVEGPYROOT/options/BcVegPyPythia8.py"
    elif prod_tool == "GenXicc":  # NB: currently no steps for 2011
        conditions += ["GenXiccPythia8"]
        yield "$LBGENXICCROOT/options/GenXiccPythia8.py"
    elif prod_tool == "SuperChic2":  # NB: currently only steps for 2016-2018
        conditions += ["SuperChic2"]
        yield "$LBSUPERCHIC2ROOT/options/SuperChic2Pythia8.py"
        yield "$APPCONFIGOPTS/Gauss/OneFixedInteraction.py"
    elif prod_tool == "Madgraph":
        conditions += ["MadgraphPythia8"]
        yield "$LBMADGRAPHROOT/options/MadgraphPythia8.py"
        data_pkgs.append(properties.madgraph_version)
    elif prod_tool == "Powheg":
        conditions += ["PowhegPythia8"]
        yield "$LBPOWHEGROOT/options/PowhegPythia8.py"
        data_pkgs.append(properties.powheg_version)
    elif prod_tool == "Particle Gun":
        properties.fast_simulation_type = "PGun"
        conditions += ["pGun"]
        data_pkgs.append(properties.pgun_version)
        yield "$LBPGUNSROOT/options/PGuns.py"
        pgun_source = extra_options.get("PGun")
        yield f"$PGUNSDATAROOT/options/PGuns{pgun_source}.py"
        processing_pass += ["PGun", pgun_source]
    elif prod_tool == "Epos":
        if properties.collision_type in {
            "ppAr",
            "ppHe",
            "ppH2",
            "ppD2",
            "ppNe",
            "ppN2",
            "ppO2",
            "ppKr",
            "ppXe",
        }:
            conditions += ["Pythia8andEPOS"]
        else:
            conditions += ["EPOS"]
        if properties.sim_version.startswith("Sim09"):
            yield "$LBCRMCROOT/options/EPOS.py"
            if opt := extra_options.get("EPOS"):
                yield {
                    "Fix3": "$APPCONFIGOPTS/Gauss/FixThreeInteractionAfterEPOS.py",
                    "Fix4": "$APPCONFIGOPTS/Gauss/FixFourInteractionAfterEPOS.py",
                }[opt]
        elif properties.sim_version.startswith("Sim10"):
            if properties.smog is True:
                yield "$LBCRMCROOT/options/EPOSBeamGas.py"
            else:
                yield "$LBCRMCROOT/options/EPOSBeamBeam.py"
        if opt := extra_options.get("Centrality"):
            if opt == "central":
                yield "$LBCRMCROOT/options/ImpactParam_CENT.py"
                processing_pass += ["central"]
            elif opt == "peripheral":
                yield "$LBCRMCROOT/options/ImpactParam_PERIPH.py"
                processing_pass += ["peripheral"]
        if opt := extra_options.get("SignalMode"):
            yield {"embedding": "$GAUSSOPTS/Embedding.py"}[opt]
    elif prod_tool == "StarLight":
        yield "$LBSTARLIGHTROOT/options/StarLight.py"
        conditions += ["StarLight"]
    else:
        raise NotImplementedError(prod_tool)


def _fast_mc_options(
    properties: Stage6Properties, fast_mc: dict, processing_pass: list
):
    # TODO: the if...elif...else logic prevents mixing fast MC methods,
    # but some combinations are okay and should be allowed
    enabled_methods = {k for k, v in (fast_mc or {}).items() if v}
    if enabled_methods == {"redecay"} and fast_mc["redecay"]:
        redecay = fast_mc["redecay"]
        assert set(redecay.keys()) == {"mode", "num-redecays"}
        assert redecay["mode"] == "signal-and-heavier"
        yield f"$APPCONFIGOPTS/Gauss/ReDecay-{redecay['num-redecays']}times.py"
        yield "$APPCONFIGOPTS/Gauss/ReDecay-FullGenEventCutTool-fix.py"
        properties.fast_simulation_type = "ReDecay"
        if redecay["num-redecays"] != 100:
            processing_pass += ["ReDecay", f"{redecay['num-redecays']}times"]
        else:
            processing_pass += ["ReDecay01"]
    elif enabled_methods == {"splitsim"} and fast_mc["splitsim"]:
        splitsim = fast_mc["splitsim"]
        properties.fast_simulation_type = "SplitSim"
        yield "$APPCONFIGOPTS/Gauss/SplitSim-GammaConversionFilter.py"
        if (mode := splitsim["mode"]) == "default":
            processing_pass += ["SplitSim01"]
        elif mode == "extend-z":
            yield "$APPCONFIGOPTS/Gauss/GammaConversionFilter_ExtendZ.py"
            processing_pass += ["SplitSim02"]
        else:
            raise NotImplementedError(f"Unknown splitsim mode: {mode}")
    elif enabled_methods == {"tracker-only"} and fast_mc["tracker-only"]:
        properties.fast_simulation_type = "TrackerOnly"
        yield "$APPCONFIGOPTS/Gauss/no_calo_and_muon.py"
        # Nothing added to processing_pass
    elif enabled_methods:
        raise NotImplementedError(fast_mc)


def _material_options(properties: Stage6Properties, material_tool: str, fast_mc: dict):
    match = re.fullmatch(r"Sim([\d]+)[a-z]", properties.sim_version)
    if not match:
        raise NotImplementedError(properties.sim_version)
    major_sim_version = int(match.groups()[0])

    if material_tool == "Geant4":
        phys_list = [
            "G4PL",
            "FTFP",
            "BERT",
        ]
        phys_list += [
            {
                9: "EmNoCuts",
                10: "EmOpt2",
            }[major_sim_version]
        ]
        if fast_mc.get("tracker-only"):
            phys_list += [
                {
                    9: "noLHCbphys",
                    10: "noLHCbPhys",  # argh!
                }[major_sim_version]
            ]
        yield f"$APPCONFIGOPTS/Gauss/{'_'.join(phys_list)}.py"
    else:
        raise NotImplementedError(material_tool)
