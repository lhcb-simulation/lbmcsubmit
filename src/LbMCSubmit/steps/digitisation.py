###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Iterable

from LbMCSubmit import datapkgs
from LbMCSubmit.processing_passes import DIGI
from LbMCSubmit.properties import Stage6Properties
from LbMCSubmit.utils import make_step


def get_steps(properties: Stage6Properties, version: str, input_type: str = "SIM"):
    output_type = (
        "XDIGI"
        if properties.file_format in {"XDIGI", "XDST"}
        else "MDF" if properties.file_format == "MDF" else "DIGI"
    )
    if version == "14c":
        options = list(_options_14c(properties))
    elif version == "15":
        options = list(_options_15(properties))
    elif version in ["16", "17"]:
        options = list(_options_16(properties))
    elif version in ["17a", "17b"]:
        options = list(_options_17a(properties))
    else:
        raise NotImplementedError(version)
    app = DIGI[version]["application"]
    name = f"Digi{version} for {properties.data_type}"
    if properties.spillover:
        name += "+spillover"
    if properties.pgun:
        name += "+pgun"
    if output_type != "DIGI":
        name += f" {output_type} format"
    yield make_step(
        name,
        f"Digi{version}",
        [datapkgs.appconfig] + DIGI[version]["data-pkgs"],
        app,
        options,
        [input_type],
        [output_type],
        visible=False,
    )


def _options_14c(properties: Stage6Properties) -> Iterable[str]:
    yield "$APPCONFIGOPTS/Boole/Default.py"
    if properties.spillover:
        yield "$APPCONFIGOPTS/Boole/EnableSpillover.py"
    if properties.data_type in {"2015", "2016", "2017", "2018"}:
        yield "$APPCONFIGOPTS/Boole/DataType-2015.py"
    elif properties.data_type == "2013":
        yield "$APPCONFIGOPTS/Boole/DataType-2012.py"
        yield "$APPCONFIGOPTS/Boole/NoPacking.py"
    elif properties.data_type in {"2011", "2012"}:
        yield f"$APPCONFIGOPTS/Boole/DataType-{properties.data_type}.py"
        yield "$APPCONFIGOPTS/Boole/NoPacking.py"
    else:
        raise NotImplementedError(properties.data_type)

    yield "$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py"
    if properties.pgun:
        yield "$APPCONFIGOPTS/Persistency/PropagatePgunVertex.py"
    if properties.fast_simulation_type == "TrackerOnly":
        yield "$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py"
    if properties.smog:
        yield "$APPCONFIGOPTS/Boole/SMOG.py"
    if properties.file_format in {"XDIGI", "XDST"}:
        yield "$APPCONFIGOPTS/Boole/xdigi.py"


def _options_15(properties: Stage6Properties) -> Iterable[str]:
    # Options files for Digi15 happen to be the same as 14c
    return _options_14c(properties)


def _options_16(properties: Stage6Properties) -> Iterable[str]:
    yield "$APPCONFIGOPTS/Boole/Default.py"
    if properties.spillover:
        yield "$APPCONFIGOPTS/Boole/EnableSpillover.py"
    if properties.data_type == "2022":
        yield "$APPCONFIGOPTS/Boole/Boole-2022-noUT.py"
        yield "$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py"
        yield "$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py"
    elif properties.data_type in (
        "2023",
        "expected-2024",
        "expected-2024.Q1.2",
    ):
        yield "$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20200616.py"
        yield "$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py"
        yield "$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py"
    elif properties.data_type == "expected-2024.Q1.2":
        yield "$APPCONFIGOPTS/Boole/Run3-VP-NoSpillOver.py"
    else:
        raise NotImplementedError(properties.data_type)
    if properties.pgun:
        yield "$APPCONFIGOPTS/Persistency/PropagatePgunVertex.py"
    if properties.fast_simulation_type == "TrackerOnly":
        yield "$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py"
    if properties.smog and properties.collision_type not in {
        "ppAr",
        "ppHe",
        "ppH2",
        "ppD2",
        "ppN2",
        "ppO2",
        "ppNe",
        "ppKr",
        "PbPbAr",
        "Arp",
        "ArPb",
    }:
        yield "$APPCONFIGOPTS/Boole/SMOG.py"
    if properties.file_format in {"XDIGI", "XDST"}:
        yield "$APPCONFIGOPTS/Boole/xdigi.py"


def _options_17a(properties: Stage6Properties) -> Iterable[str]:
    yield "$APPCONFIGOPTS/Boole/Default.py"
    if properties.spillover:
        yield "$APPCONFIGOPTS/Boole/EnableSpillover.py"
    if properties.data_type == "2022":
        yield "$APPCONFIGOPTS/Boole/Boole-2022-noUT.py"
        yield "$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py"
        yield "$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py"
    elif properties.data_type in (
        "2023",
        "expected-2024-5TeV",
        "2024.Q1.2",
        "2024.W25.27",
        "2024.W29.30",
        "2024.W31",
        "2024.W31.34",
        "2024.W32.34",
        "2024.W35.37",
        "2024.W37.39",
        "2024.W40.42",
        "expected-2024.Q3.4",
        "2024",
    ):
        yield "$APPCONFIGOPTS/Boole/Boole-Upgrade-Baseline-20200616.py"
        yield "$APPCONFIGOPTS/Boole/Upgrade-RichMaPMT-NoSpilloverDigi.py"
        yield "$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi-0fb.py"
    else:
        raise NotImplementedError(properties.data_type)
    yield "$APPCONFIGOPTS/Boole/Run3-VP-NoSpillOver.py"
    yield "$APPCONFIGOPTS/Persistency/BasketSize-10.py"
    if properties.pgun:
        yield "$APPCONFIGOPTS/Persistency/PropagatePgunVertex.py"
    if properties.fast_simulation_type == "TrackerOnly":
        yield "$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py"
    if properties.smog and properties.collision_type not in {
        "ppAr",
        "ppHe",
        "ppH2",
        "ppD2",
        "ppN2",
        "ppO2",
        "ppNe",
        "ppKr",
        "ppXe",
        "PbPbAr",
        "PbPbNe",
        "PbPbH2",
        "PbPbD2",
        "PbPbO2",
        "PbPbN2",
        "PbPbKr",
        "PbPbXe",
        "PbPbHe",
        "Arp",
        "ArPb",
    }:
        yield "$APPCONFIGOPTS/Boole/SMOG.py"
    if properties.file_format in {"XDIGI", "XDST"}:
        yield "$APPCONFIGOPTS/Boole/xdigi.py"
