###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LbMCSubmit import datapkgs
from LbMCSubmit.utils import make_step, run_from_data_type
from LbMCSubmit.versioning import latest_application

# Keep this global so it can be monkeypatched (cf datapkgs.appconfig)
# TODO: turn into dict when adding Run 3
davinci = f"DaVinci/{latest_application('DaVinci', 'v46r*')}"


def get_steps(data_type: str, output_type: list[str]):
    if run_from_data_type(data_type) in {1, 2}:
        assert len(output_type) == 1
        options = [
            "$APPCONFIGOPTS/Merging/DVMergeDST.py",
            f"$APPCONFIGOPTS/DaVinci/DataType-{data_type}.py",
            "$APPCONFIGOPTS/Merging/WriteFSR.py",
            "$APPCONFIGOPTS/Merging/MergeFSR.py",
            "$APPCONFIGOPTS/DaVinci/Simulation.py",
        ]
        yield make_step(
            f"Merge for {output_type[0]}",
            "merging",
            [datapkgs.appconfig],
            davinci,
            options,
            output_type,
            output_type,
            visible=False,
            options_format="merge",
        )
