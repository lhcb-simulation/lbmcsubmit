###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
from fnmatch import fnmatch
from typing import Optional, Tuple

from LbMCSubmit import datapkgs
from LbMCSubmit.properties import Stage6Properties
from LbMCSubmit.utils import (
    make_step,
    order_options,
    run_from_data_type,
    write_filter_options,
)
from LbMCSubmit.versioning import get_major_version

STRIPPING_REGEX = re.compile(r"(\d+)(?:r(\d+)(?:p(\d+))?)?")


def get_steps(properties: Stage6Properties, data: dict):
    # Special case for Ultra-Peripheral Collisions
    properties.hlt_filters_in_reco = all(
        [
            properties.collision_type in {"PbPb"},
            properties.data_type in {"2015"},
            not data["generation"]["production-tool"] == "StarLight",
        ]
    )
    #
    if (run := run_from_data_type(properties.data_type)) in {1, 2}:
        yield from get_steps_run1_2(
            properties, data["reconstruction"], data["tesla"], data["stripping"]
        )
    elif run == 3:
        # Temporarily avoid sprucing on 2024
        if properties.data_type not in (
            "2022",
            "2023",
            "2024.Q1.2",
            "2024.W25.27",
            "2024.W29.30",
            "expected-2024.Q3.4",
        ):
            if "sprucing" in data:
                yield from get_steps_run3(properties, data["sprucing"])
    else:
        raise NotImplementedError(properties.data_type)


def get_steps_run1_2(
    properties: Stage6Properties,
    reco: dict[str, str],
    tesla: dict[str, str],
    stripping: dict[str, str],
):
    if properties.pgun:
        tesla = {}
        stripping = {}
    # Deduce input/output file formats for reco/tesla/stripping
    if properties.file_format in {"MDST", "DST"}:
        reco.update(
            {
                "input-name": "DIGI",
                "output-name": "DST",
            }
        )
        if tesla:
            tesla.update(
                {
                    "input-name": "DST",
                    "output-name": "DST",
                }
            )
        if stripping:
            stripping["input-name"] = "DST"  # output-name set elsewhere
    elif properties.file_format == "LDST":
        reco.update(
            {
                "input-name": "DIGI",
                "output-name": "LDST",
            }
        )
        if tesla:
            tesla.update(
                {
                    "input-name": "LDST",
                    "output-name": "LDST",
                }
            )
        if stripping:
            stripping["input-name"] = "LDST"
    elif properties.file_format == "XDST":
        reco.update(
            {
                "input-name": "XDIGI",
                "output-name": "XDST",
            }
        )
        tesla = {}
        stripping = {}
    else:
        raise NotImplementedError(properties.file_format)

    # Reconstruction
    reco_ver = reco["version"]
    app = reco["application"]
    options = list(_reco_options(properties, reco_ver))
    data_pkgs = [datapkgs.appconfig] + reco["data-pkgs"]
    app_major_ver = get_major_version(app)
    yield make_step(
        f"Reco{reco_ver} for MC {properties.data_type}",
        f"Reco{reco_ver}",
        data_pkgs,
        app,
        options,
        [reco["input-name"]],
        [reco["output-name"]],
    )
    # Assert our assumption of no Brunel versions below v43r* used in production
    assert int(app_major_ver) >= 43

    # Tesla
    if tesla:
        tesla_ver = tesla["version"]
        app = tesla["application"]
        options = list(_tesla_options(properties, tesla_ver))
        data_pkgs = [datapkgs.appconfig] + tesla["data-pkgs"]
        yield make_step(
            f"Turbo lines (MC) for {properties.data_type} Turbo{tesla_ver}",
            f"Turbo{tesla_ver}",
            data_pkgs,
            app,
            options,
            [tesla["input-name"]],
            [tesla["output-name"]],
            options_format="Tesla",
        )

    # Stripping
    if stripping:
        stripping_ver = stripping["version"]
        app = stripping["application"]
        data_pkgs = [datapkgs.appconfig]
        data_pkgs += stripping["data-pkgs"]
        base_options = list(_base_stripping_options(properties, stripping_ver))
        processing_pass = f"Stripping{stripping_ver}"

        if "filter" in stripping:
            options = list(_make_filtered_options(properties, stripping, base_options))
            processing_pass += "Filtered"
        elif "filtering-script" in stripping:
            options = [
                stripping["filtering-script"].format(
                    **{
                        "version": stripping_ver,
                        "year": properties.data_type,
                    }
                )
            ] + order_options(base_options)
            processing_pass += "Filtered"
        else:
            flagged_options = list(
                _flagged_stripping_options(properties, stripping_ver)
            )
            options = order_options(flagged_options + base_options)
            processing_pass += "NoPrescalingFlagged"
        yield make_step(
            f"{processing_pass} for {properties.data_type}",
            processing_pass,
            data_pkgs,
            app,
            options,
            [stripping["input-name"]],
            [stripping["output-name"]],
        )


def get_steps_run3(
    properties: Stage6Properties,
    sprucing: dict[str, str],
):
    if sprucing:
        title = f"Sprucing {sprucing['version']} for {properties.data_type} {properties.file_format}"
        proc_pass = f"Spruce{sprucing['version']}"
        yield make_step(
            title,
            proc_pass,
            sprucing["data-pkgs"],
            sprucing["application"],
            sprucing["options"],
            [f"HLT2.{properties.file_format}"],
            [f"SPRUCE.{properties.file_format}"],
            visible=True,
        )


def _get_pp_reco_options(properties: Stage6Properties, version: str):
    if version == "17at5TeV":
        yield "$APPCONFIGOPTS/Brunel/FastVelo-2017-5TeV.py"
    yield f"$APPCONFIGOPTS/Brunel/DataType-{properties.data_type}.py"
    yield "$APPCONFIGOPTS/Brunel/MC-WithTruth.py"
    if properties.file_format == "LDST":
        yield "$APPCONFIGOPTS/Brunel/ldst.py"
    elif properties.file_format == "XDST":
        yield "$APPCONFIGOPTS/Brunel/xdst.py"
    if properties.pgun:
        yield "$APPCONFIGOPTS/Brunel/pGun-Signal.py"
        yield "$APPCONFIGOPTS/Persistency/PropagatePgunVertex.py"

    if properties.data_type in {"2016", "2017", "2018"}:
        assert version in {"16", "17", "18", "17at5TeV"}
        assert version.startswith(properties.data_type[-2:])
        yield "$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py"
    elif properties.data_type in {"2015"}:
        assert version == "15a"
    elif properties.data_type in {"2011", "2012"}:
        assert version == "14c"
        yield "$APPCONFIGOPTS/Brunel/Sim09-Run1.py"
        yield f"$APPCONFIGOPTS/Persistency/DST-multipleTCK-{properties.data_type}.py"
    else:
        raise NotImplementedError(properties.data_type)


def _get_PbPb_reco_options(properties: Stage6Properties, version: str):
    assert version in {"18Lead", "15aLead15"}
    if properties.data_type in {"2018"}:
        yield "$APPCONFIGOPTS/Brunel/XeXe-GECs.py"
        yield f"$APPCONFIGOPTS/Brunel/DataType-{properties.data_type}.py"
    elif properties.data_type in {"2015"}:
        yield "$APPCONFIGOPTS/Brunel/PbPb-GECs.py"
        if properties.hlt_filters_in_reco:
            yield f"$APPCONFIGOPTS/Brunel/PbPb-{properties.data_type}-HLTFilters.py"
    else:
        raise NotImplementedError(properties.data_type)

    yield "$APPCONFIGOPTS/Brunel/PatPV3D-PVOfflineTool-BeamSpotRCut0.4.py"
    yield "$APPCONFIGOPTS/Brunel/FastVelo-MaxTrackClusterFrac1.0.py"
    yield "$APPCONFIGOPTS/Brunel/MC-WithTruth.py"
    if properties.data_type in {"2018"}:
        yield "$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py"


def _get_pPb_reco_options(properties: Stage6Properties, version: str):
    assert version in {"18Lead", "16pLead", "15a", "14r1"}
    if properties.data_type == "2013":
        yield "$APPCONFIGOPTS/Brunel/DataType-2012.py"
        yield "$APPCONFIGOPTS/Brunel/pA-GECs.py"
    else:
        yield f"$APPCONFIGOPTS/Brunel/DataType-{properties.data_type}.py"
    yield "$APPCONFIGOPTS/Brunel/MC-WithTruth.py"
    # TODO does particle gun work with non-pp collisions?
    if properties.pgun:
        yield "$APPCONFIGOPTS/Brunel/pGun-Signal.py"

    if properties.data_type in {"2016", "2015"}:
        yield "$APPCONFIGOPTS/Brunel/PbPb-GECs.py"
        # TODO is this meant to be like this for Pbp?
        if properties.collision_type != "Pbp":
            yield "$APPCONFIGOPTS/Brunel/FastVelo-MaxTrackClusterFrac1.0.py"
    elif properties.data_type == "2013":
        yield "$APPCONFIGOPTS/Brunel/Sim09-Run1.py"
    else:
        raise NotImplementedError(properties.data_type)


def _get_smog_reco_options(properties: Stage6Properties, version: str):
    assert version in {"18Smog", "17aSmog", "16Smog"}
    if properties.collision_type == "PbNe":
        yield (0, "$APPCONFIGOPTS/Brunel/XeXe-GECs.py")
    yield (1, f"$APPCONFIGOPTS/Brunel/DataType-{properties.data_type}.py")
    if properties.data_type in {"2015", "2016"}:
        yield (2, "$APPCONFIGOPTS/Brunel/PbPb-GECs.py")
        yield (3, "$APPCONFIGOPTS/Brunel/PVreco-smog2016.py")
    else:
        yield (
            0.5 if properties.collision_type == "PbNe" else 2,
            "$APPCONFIGOPTS/Brunel/PVreco-smog.py",
        )
    if properties.data_type == "2018":
        yield (3, "$APPCONFIGOPTS/Brunel/FastVelo-2017-5TeV.py")
    yield (4, "$APPCONFIGOPTS/Brunel/MC-WithTruth.py")
    if properties.pgun:
        yield (5, "$APPCONFIGOPTS/Brunel/pGun-Signal.py")


def _reco_options(properties: Stage6Properties, version: str):
    if properties.collision_type == "pp":
        yield from _get_pp_reco_options(properties, version)
    elif properties.smog:
        yield from order_options(_get_smog_reco_options(properties, version))
    elif properties.collision_type in {"pPb", "Pbp"}:
        yield from _get_pPb_reco_options(properties, version)
    elif properties.collision_type == "PbPb":
        yield from _get_PbPb_reco_options(properties, version)
    else:
        raise NotImplementedError(properties.collision_type)
    if properties.fast_simulation_type == "TrackerOnly":
        # TODO: should use this whenever no trigger is run
        yield "$APPCONFIGOPTS/Brunel/notKnownTCK.py"


def _tesla_options(properties: Stage6Properties, version: str):
    if version == "02":
        assert properties.data_type == "2015"
        yield "$APPCONFIGOPTS/Turbo/Tesla_AllHlt2Lines_v10r0_0x00fa0051.py"
        yield "$APPCONFIGOPTS/Turbo/Tesla_Simulation_2015_PVHLT2.py"
    else:
        dt = properties.data_type
        if properties.collision_type in {"pPb", "Pbp"}:
            yield f"$APPCONFIGOPTS/Turbo/Tesla_pA_Ap_MC_{dt}_fixwithTURCAL.py"
        else:
            suffix = "AndTurCal" if "WithTurcal" in version else ""
            yield f"$APPCONFIGOPTS/Turbo/Tesla_{dt}_LinesFromStreams{suffix}_MC.py"
        if version in {"02a", "03a"}:
            yield "$APPCONFIGOPTS/Turbo/Tesla_PR_Truth_2016.py"
        if version == "02a":
            yield f"$APPCONFIGOPTS/Turbo/Tesla_Simulation_{dt}_PVHLT2.py"
        else:
            yield f"$APPCONFIGOPTS/Turbo/Tesla_Simulation_{dt}.py"
        if properties.file_format in {"MDST", "LDST"}:
            # TODO: remove this for Turbo-filtered requests (see LHCBGAUSS-1776)
            yield "$APPCONFIGOPTS/Turbo/Tesla_FilterMC.py"


def _stripping_major_version(version: str):
    major, _, _ = STRIPPING_REGEX.fullmatch(version).groups()
    return major


# General options-file ordering for flagged Stripping steps
# 0: Stripping-MC-NoPrescaling
# 1: RedoCaloPID
# 2: DataType
# 3: InputType
# 4: Stripping-MC-muDST
# 5: RawEventJuggler
# 6: Sim09 deuteron~ name fix


def _base_stripping_options(properties: Stage6Properties, version: str):
    major = _stripping_major_version(version)
    # Version-specific extra options
    # RedoCaloPID
    if major == "21":
        yield (1, "$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py")
    elif major in {"24", "28"}:
        yield (1, "$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping_28_24.py")
    # Raw event juggler
    if major in {"22", "24"}:
        yield (5, "$APPCONFIGOPTS/DaVinci/DV-RawEventJuggler-0_3-to-4_3.py")
    elif major == "28":
        yield (5, "$APPCONFIGOPTS/DaVinci/DV-RawEventJuggler-4_3-to-4_3.py")

    # From scripts/find_deuteron_fix_versions.py
    if properties.sim_version.startswith("Sim09") and version in {
        "21r0p2",
        "21r1p2",
        "24r2",
        "28r2",
        "28r2p1",
        "29r2p1",
        "29r2p2",
        "33r2",
        "34r0p1",
        "34r0p2",
        "35r2",
        "35r3",
    }:
        yield (6, "$APPCONFIGOPTS/Conditions/Sim09-deuteron-nameFix.py")

    # Base options common to all versions
    if properties.data_type == "2013":
        yield (2, "$APPCONFIGOPTS/DaVinci/DataType-2012.py")
    else:
        yield (2, f"$APPCONFIGOPTS/DaVinci/DataType-{properties.data_type}.py")
    if properties.file_format == "LDST":
        input_format = "LDST"
    elif properties.file_format in {"DST", "MDST"}:
        input_format = "DST"
    else:
        raise NotImplementedError(properties.file_format)
    yield (3, f"$APPCONFIGOPTS/DaVinci/InputType-{input_format}.py")


def _no_prescaling_opt(version: str, fmt: Optional[str] = None) -> str:
    fname = f"DV-Stripping{version}-Stripping-MC-NoPrescaling"
    if fmt:
        fname += f"-{fmt}"
    return f"$APPCONFIGOPTS/DaVinci/{fname}.py"


def _flagged_stripping_options(properties: Stage6Properties, version: str):
    major = _stripping_major_version(version)
    if major in {"20", "30", "31", "33", "35"} or version in {
        "21",
        "21r1",
        "21r0p1",
        "21r1p1",
    }:
        yield (
            0,
            _no_prescaling_opt(version),
        )
    elif version == "22":
        fname = "DV-Stripping22-Stripping-noGECSemilep-MC-NoPrescaling"
        yield (0, f"$APPCONFIGOPTS/DaVinci/{fname}.py")
    elif major in {"24", "25", "27", "28", "29", "32", "34"} or version in {
        "21r0p2",
        "21r1p2",
    }:
        stripping_format = (
            properties.file_format
            if properties.file_format in {"LDST", "XDST"}
            else "DST"
        )
        yield (
            0,
            _no_prescaling_opt(version, stripping_format),
        )
    else:
        raise NotImplementedError(version)

    if major == "25":
        yield (
            1,
            f"$APPCONFIGOPTS/DaVinci/DV-Stripping{version}-Bugfix-SilenceDstWriters.py",
        )

    if properties.file_format == "MDST":
        yield (4, "$APPCONFIGOPTS/DaVinci/DV-Stripping-MC-muDST.py")
    elif properties.file_format == "LDST":
        yield (4, "$APPCONFIGOPTS/DaVinci/LDST-Output.py")


def _make_filtered_options(
    properties: Stage6Properties, stripping: dict, base_options: list[Tuple[int, str]]
):
    redo_calo_pid = any(
        fnmatch(x, "$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-*.py")
        for _, x in base_options
    )

    pattern = re.compile(r"([A-Za-z0-9_]{5,20})\.Strip\." + properties.file_format)
    match = pattern.fullmatch(stripping["output-name"])
    if not match:
        raise ValueError(
            f"Stripping output name {stripping['output-name']} "
            f"does not match required pattern ({pattern.pattern})"
        )
    analysis_name, output_format = match.groups()

    stripping_tck = "0x"
    stripping_tck += stripping_tck_segment(stripping["DaVinci"])
    stripping_tck += stripping_tck_segment(stripping["version"])

    lines = stripping["filter"]["lines"]
    if isinstance(lines, str):
        lines = repr(lines)
    hash_contents = {
        "version": "1",
        "stripping_name": repr(f"stripping{stripping['version']}"),
        "analysis_name": repr(analysis_name),
        "output_format": repr(output_format),
        "stripping_tck": stripping_tck,
        "lines": lines,
        "redo_calo_pid": redo_calo_pid,
    }

    yield write_filter_options("Stripping", hash_contents)
    yield from order_options(base_options)


def stripping_tck_segment(version: str) -> str:
    """Make half of the stripping TCK

    The stripping TCK takes the form (ABCDEFGH) where
       * vABrCpD is the DaVinci version
       * vEFrGpH is the Stripping version
    i.e. DaVinci 41r4p5 with Stripping28r1p1 becomes 0x41452811
    """
    pattern = re.compile(r"v?(\d\d)(?:r(\d+))?(?:p(\d+))?")
    x, y, z = [int(x) if x else 0 for x in pattern.fullmatch(version).groups()]
    result = f"{x}{y:X}{z:X}"
    if len(result) != 4:
        raise NotImplementedError(result)
    return result
