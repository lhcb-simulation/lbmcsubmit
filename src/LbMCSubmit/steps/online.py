###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Iterator

from LbMCSubmit import datapkgs
from LbMCSubmit.properties import Stage6Properties
from LbMCSubmit.utils import make_step, run_from_data_type


def get_steps(properties: Stage6Properties, trigger: dict):
    if (run := run_from_data_type(properties.data_type)) in {1, 2}:
        yield from get_steps_run1_2(properties, trigger)
    elif run == 3:
        yield from get_steps_run3(properties, trigger)
    else:
        raise NotImplementedError(properties.data_type)


def get_steps_run1_2(properties: Stage6Properties, trigger: dict):
    l0_tck = trigger["L0"]["TCK"]
    l0_app = trigger["L0"]["application"]
    options = get_l0(properties, l0_tck)
    yield make_step(
        f"L0 emulation for {properties.data_type} - TCK {l0_tck}",
        f"L0Trig{l0_tck}",
        [datapkgs.appconfig] + trigger["L0"]["data-pkgs"],
        l0_app,
        list(options),
        ["DIGI"],
        ["DIGI"],
        options_format="l0app",
        visible=False,
    )

    if "HLT" in trigger:
        tck = trigger["HLT"]["TCK"]
        hlt_app = trigger["HLT"]["application"]
        options = list(get_hlt_both(properties, tck))
        yield make_step(
            f"TCK-{tck} Flagged for {properties.data_type}",
            f"Trig{tck}",
            [datapkgs.appconfig] + trigger["HLT"]["data-pkgs"],
            hlt_app,
            options,
            ["DIGI"],
            ["DIGI"],
        )
    elif "HLT1" in trigger and "HLT2" in trigger:
        for i in [1, 2]:
            tck = trigger[f"HLT{i}"]["TCK"]
            hlt_app = trigger[f"HLT{i}"]["application"]
            options = list(get_hlt_split(properties, tck, i))
            yield make_step(
                f"TCK-{tck} (HLT{i}) Flagged for {properties.data_type}",
                f"Trig{tck}",
                [datapkgs.appconfig] + trigger[f"HLT{i}"]["data-pkgs"],
                hlt_app,
                options,
                ["DIGI"],
                ["DIGI"],
                visible=(i == 2),
            )
    else:
        raise NotImplementedError(trigger)


def get_steps_run3(properties: Stage6Properties, trigger: dict):
    for i in [1, 2]:
        tck = trigger[f"HLT{i}"]["TCK"]
        try:
            int(tck, 16)
            proc_pass = f"Trig{tck}"
        except ValueError:
            proc_pass = tck
        hlt_app = trigger[f"HLT{i}"]["application"]
        options = trigger[f"HLT{i}"]["options"]
        visible = (i == 2) or (properties.data_type == "2024.W31.34")

        yield make_step(
            f"TCK-{tck} (HLT{i}) Flagged for {properties.data_type}",
            proc_pass,
            trigger[f"HLT{i}"]["data-pkgs"],
            hlt_app,
            options,
            {1: ["DIGI"], 2: ["HLT1.DST"]}[i],
            {1: ["HLT1.DST"], 2: ["HLT2.DST"]}[i],
            visible=visible,
        )


def get_l0(properties: Stage6Properties, tck: str) -> Iterator[str]:
    yield "$APPCONFIGOPTS/L0App/L0AppSimProduction.py"
    yield f"$APPCONFIGOPTS/L0App/L0AppTCK-{tck}.py"

    if properties.data_type in {"2015", "2016", "2017", "2018"}:
        yield "$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py"
    elif properties.data_type not in ["2011", "2012", "2013"]:
        raise NotImplementedError(properties.data_type)

    if properties.data_type == "2018":
        yield "$APPCONFIGOPTS/L0App/DataType-2017.py"
    elif properties.data_type == "2013":
        yield "$APPCONFIGOPTS/L0App/DataType-2012.py"
    else:
        yield f"$APPCONFIGOPTS/L0App/DataType-{properties.data_type}.py"


def get_hlt_both(properties: Stage6Properties, tck: str) -> Iterator[str]:
    assert properties.data_type in {"2011", "2012", "2013", "2015"}
    if properties.data_type == "2015":
        yield "$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py"
    else:
        yield "$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep.py"
    yield f"$APPCONFIGOPTS/Conditions/TCK-{tck}.py"
    if properties.data_type == "2013":
        yield "$APPCONFIGOPTS/Moore/DataType-2012.py"
    else:
        yield f"$APPCONFIGOPTS/Moore/DataType-{properties.data_type}.py"


def get_hlt_split(properties: Stage6Properties, tck: str, hlt: int) -> Iterator[str]:
    assert properties.data_type in {"2016", "2017", "2018"}
    yield "$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py"
    yield f"$APPCONFIGOPTS/Conditions/TCK-{tck}.py"

    if properties.data_type == "2018":
        yield "$APPCONFIGOPTS/Moore/DataType-2017.py"
    else:
        yield f"$APPCONFIGOPTS/Moore/DataType-{properties.data_type}.py"

    yield f"$APPCONFIGOPTS/Moore/MooreSimProductionHlt{hlt}.py"
