###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

working_groups = [
    # PAWGs
    "B2CC",
    "B2OC",
    "BandQ",
    "BnoC",
    "Charm",
    "IFT",
    "QEE",
    "RD",
    "SL",
    # PPWGs
    # "Calib", # Possibly deprecated, not used since 2018
    "Luminosity",
    "PID",
    "Tagging",
    "Tracking",
    # Projects
    "Calo",
    "DPA",
    "HLT",
    "RTA",
    "Simulation",
]


def _wg_config_name(wg: str) -> str:
    assert wg in working_groups
    alt_names = {
        "BnoC": "BNoC",
        "SL": "Semilep",
        "Tracking": "Trk",
        "QEE": "Exotica",
    }
    if wg in alt_names:
        return alt_names[wg]
    return wg


priority_levels = ["1a", "1b", "2a", "2b"]
