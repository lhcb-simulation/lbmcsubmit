###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from collections import defaultdict

from pydantic.utils import deep_update  # pylint: disable = no-name-in-module

from LbMCSubmit import beam_parameters
from LbMCSubmit.utils import to_pol_str

from .simbase import SimBase


class Sim10(SimBase):
    data_types = {
        "2011",
        "2012",
        "2013",
        "2015",
        "2015-5TeV",
        "2016",
        "2017",
        "2017-5TeV",
        "2018",
        "2022",
        "2023",
        "2024.Q1.2",
        "2024.W25.27",
        "2024.W29.30",
        "2024.W31.34",
        "2024.W31",
        "2024.W32.34",
        "2024.W35.37",
        "2024.W37.39",
        "2024.W40.42",
        "2024",
        # NB: please use the prefix "expected-" for future data-taking periods
        # so that "Dev" is assigned as the MC config version
        # e.g. "expected-2024"
        # see SimBase.apply_stage_6
        "expected-2024-5TeV",
        "expected-2024.Q3.4",
    }
    magnet_polarities = {"MagDown", "MagUp"}
    collision_types = {
        "pp",
        "pAr",
        "pHe",
        "pH2",
        "pD2",
        "pO2",
        "pN2",
        "pNe",
        "pKr",
        "pXe",
        "Arp",
        "ppAr",
        "ppHe",
        "ppH2",
        "ppD2",
        "ppO2",
        "ppN2",
        "ppNe",
        "ppKr",
        "ppXe",
        "PbPb",
        "PbAr",
        "PbNe",
        "PbHe",
        "PbH2",
        "PbD2",
        "PbO2",
        "PbN2",
        "PbKr",
        "PbXe",
        "ArPb",
        "PbPbAr",
        "PbPbNe",
        "PbPbHe",
        "PbPbH2",
        "PbPbD2",
        "PbPbN2",
        "PbPbO2",
        "PbPbKr",
        "PbPbXe",
        "uniformHeadOn",
    }
    file_formats = {
        "XGEN",
        "RGEN",
        "SIM",
        "XSIM",
        "DIGI",
        "XDIGI",
        "MDF",
        "DST",
        "MDST",
        "LDST",
        "XDST",
    }

    production_tools = {
        "Pythia8",
        "BcVegPy",
        "GenXicc",
        "SuperChic2",
        "Epos",
        "StarLight",
        "Particle Gun",
        "Madgraph",
        "Powheg",
    }
    tags = {collision_type: {} for collision_type in collision_types}
    decay_tools = {"EvtGen"}
    material_tools = {"Geant4"}

    default_production_tool = "Pythia8"
    default_decay_tool = "EvtGen"
    default_material_tool = "Geant4"
    default_pgun_pattern = "v2r*"
    default_madgraph_pattern = "v20903r7p*"
    default_powheg_pattern = "v3744r7p*"

    @property
    def beam_parameters(self) -> dict:
        return beam_parameters

    @property
    def tcks(self) -> dict[str, str]:
        tcks = {
            "pp": defaultdict(dict),
            "pAr": defaultdict(dict),
            "pHe": defaultdict(dict),
            "pH2": defaultdict(dict),
            "pD2": defaultdict(dict),
            "pN2": defaultdict(dict),
            "pO2": defaultdict(dict),
            "pNe": defaultdict(dict),
            "pKr": defaultdict(dict),
            "pXe": defaultdict(dict),
            "ppAr": defaultdict(dict),
            "ppHe": defaultdict(dict),
            "ppH2": defaultdict(dict),
            "ppD2": defaultdict(dict),
            "ppN2": defaultdict(dict),
            "ppO2": defaultdict(dict),
            "ppNe": defaultdict(dict),
            "ppKr": defaultdict(dict),
            "ppXe": defaultdict(dict),
            "Arp": defaultdict(dict),
            "PbPb": defaultdict(dict),
            "PbAr": defaultdict(dict),
            "PbNe": defaultdict(dict),
            "PbHe": defaultdict(dict),
            "PbH2": defaultdict(dict),
            "PbD2": defaultdict(dict),
            "PbN2": defaultdict(dict),
            "PbO2": defaultdict(dict),
            "PbKr": defaultdict(dict),
            "PbXe": defaultdict(dict),
            "ArPb": defaultdict(dict),
            "PbPbAr": defaultdict(dict),
            "PbPbNe": defaultdict(dict),
            "PbPbHe": defaultdict(dict),
            "PbPbH2": defaultdict(dict),
            "PbPbD2": defaultdict(dict),
            "PbPbO2": defaultdict(dict),
            "PbPbN2": defaultdict(dict),
            "PbPbKr": defaultdict(dict),
            "PbPbXe": defaultdict(dict),
        }

        tcks["pp"]["2011"]["L0"] = "0x0037"
        tcks["pp"]["2011"]["HLT"] = "0x40760037"

        tcks["pp"]["2012"]["L0"] = "0x0045"
        tcks["pp"]["2012"]["HLT"] = "0x409f0045"

        tcks["pp"]["2015"]["L0"] = "0x00a2"
        tcks["pp"]["2015"]["HLT"] = "0x411400a2"

        tcks["pp"]["2016"]["L0"] = "0x160F"
        tcks["pp"]["2016"]["HLT1"] = "0x5138160F"
        tcks["pp"]["2016"]["HLT2"] = "0x6139160F"

        tcks["pp"]["2017"]["L0"] = "0x1709"
        tcks["pp"]["2017"]["HLT1"] = "0x51611709"
        tcks["pp"]["2017"]["HLT2"] = "0x62661709"

        tcks["pp"]["2017-5TeV"]["L0"] = "0x1725"
        tcks["pp"]["2017-5TeV"]["HLT1"] = "0x51641725"
        tcks["pp"]["2017-5TeV"]["HLT2"] = "0x61641725"

        tcks["pp"]["2018"]["L0"] = "0x18a4"
        tcks["pp"]["2018"]["HLT1"] = "0x517a18a4"
        tcks["pp"]["2018"]["HLT2"] = "0x617d18a4"

        tcks["pp"]["2022"]["HLT1"] = "hlt1_pp_no_gec_no_ut"
        tcks["pp"]["2022"]["HLT2"] = "HLT2-2022-pp"

        tcks["pp"]["2023"]["HLT1"] = "hlt1_2023"
        tcks["pp"]["2023"]["HLT2"] = "HLT2-2023"

        tcks["pp"]["expected-2024.Q3.4"]["HLT1"] = "hlt1_2024.Q3"
        tcks["pp"]["expected-2024.Q3.4"]["HLT2"] = "HLT2-2024.Q3"

        tcks["pp"]["2024.W31.34"]["HLT1"] = "HLT1_2024.W31.34_noUT"
        tcks["pp"]["2024.W31.34"]["HLT2"] = "HLT2-2024.W31.34"

        tcks["pp"]["2024.W35.37"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["pp"]["2024.W35.37"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["pp"]["2024.W37.39"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["pp"]["2024.W37.39"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["pp"]["2024.W40.42"]["HLT1"] = "HLT1_2024.W40.42"
        tcks["pp"]["2024.W40.42"]["HLT2"] = "HLT2-2024.W40.42"

        tcks["pH2"]["2024.W32.34"]["HLT1"] = "HLT1_2024.W31.34_noUT"
        tcks["pH2"]["2024.W32.34"]["HLT2"] = "HLT2-2024.W31.34"

        tcks["pH2"]["2024.W31"]["HLT1"] = "HLT1_2024.W31.34_noUT"
        tcks["pH2"]["2024.W31"]["HLT2"] = "HLT2-2024.W31.34"

        tcks["ppH2"]["2024.W32.34"]["HLT1"] = "HLT1_2024.W31.34_noUT"
        tcks["ppH2"]["2024.W32.34"]["HLT2"] = "HLT2-2024.W31.34"

        tcks["ppH2"]["2024.W31"]["HLT1"] = "HLT1_2024.W31.34_noUT"
        tcks["ppH2"]["2024.W31"]["HLT2"] = "HLT2-2024.W31.34"

        tcks["pAr"]["2024.W32.34"]["HLT1"] = "HLT1_2024.W31.34_noUT"
        tcks["pAr"]["2024.W32.34"]["HLT2"] = "HLT2-2024.W31.34"

        tcks["ppAr"]["2024.W32.34"]["HLT1"] = "HLT1_2024.W31.34_noUT"
        tcks["ppAr"]["2024.W32.34"]["HLT2"] = "HLT2-2024.W31.34"

        tcks["pH2"]["2024.W35.37"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["pH2"]["2024.W35.37"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["ppH2"]["2024.W35.37"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["ppH2"]["2024.W35.37"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["pNe"]["2024.W35.37"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["pNe"]["2024.W35.37"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["ppNe"]["2024.W35.37"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["ppNe"]["2024.W35.37"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["pNe"]["2024.W37.39"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["pNe"]["2024.W37.39"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["ppNe"]["2024.W37.39"]["HLT1"] = "HLT1_2024.W35.39"
        tcks["ppNe"]["2024.W37.39"]["HLT2"] = "HLT2-2024.W35.39"

        tcks["PbPb"]["2024"]["HLT1"] = "HLT1_2024_PbPb"
        tcks["PbPb"]["2024"]["HLT2"] = "HLT2_2024_PbPb"

        tcks["uniformHeadOn"] = tcks["pp"]

        return tcks[self.collision_type][self.data_type]

    @property
    def reco_version(self) -> str:
        reco_versions = {
            "pp": {
                "2011": "14c",
                "2012": "14c",
                "2015": "15a",
                "2016": "16",
                "2017": "17",
                "2017-5TeV": "17at5TeV",
                "2018": "18",
            },
        }
        reco_versions["uniformHeadOn"] = reco_versions["pp"]

        return reco_versions[self.collision_type][self.data_type]

    @property
    def turbo_version(self) -> str:

        turbo_versions = {
            "pp": {
                "2015": "02a",
                "2016": "03a",
                "2017": "04a-WithTurcal",
                "2018": "05-WithTurcal",
            },
        }
        turbo_versions["uniformHeadOn"] = turbo_versions["pp"]

        return turbo_versions[self.collision_type][self.data_type]

    @property
    def stripping_version(self) -> str:
        stripping_versions = {
            "pp": {
                "2011": "21r1",
                "2012": "21",
                "2013": "20r2",  # 2.76 TeV
                "2015": "24r2",
                "2016": "28r2",
                "2017": "29r2",
                "2017-5TeV": "32",
                "2018": "34",
            },
            "Pbp": {
                "2013": "20r3p1",  # Not found in http://cern.ch/lhcbdoc/stripping/
                "2016": "30r2",
            },
            "pPb": {
                "2013": "20r3p1",  # Not found in http://cern.ch/lhcbdoc/stripping/
                "2016": "30r3",
                "2018": "35",
            },
            "PbPb": {
                "2015": "31r2",
                "2018": "35r2",
            },
            "pAr": {
                "2015": "25",
            },
            "PbAr": {
                "2015": "31r1",
            },
            "pHe": {
                "2016": "27",
            },
            "pNe": {
                "2017": "33r2",
            },
            "PbNe": {
                "2018": "35r3",
            },
        }
        stripping_versions["uniformHeadOn"] = stripping_versions["pp"]

        return stripping_versions[self.collision_type][self.data_type]

    @property
    def sprucing_version(self) -> str:
        sprucing_versions = {
            "pp": {
                "2024.W31.34": "24c2",
                "2024.W35.37": "24c3",
                "2024.W37.39": "24c3",
                "2024.W40.42": "24c4",
            },
        }
        sprucing_versions["uniformHeadOn"] = sprucing_versions["pp"]
        return sprucing_versions[self.collision_type][self.data_type]

    @property
    def db_tags(self) -> tuple[str, str]:
        self.tags["uniformHeadOn"] = self.tags["pp"]
        dddb, simcond = self.tags[self.collision_type][self.data_type]

        return (dddb, simcond.format(pol=to_pol_str(self.polarity)))

    @property
    def digi_version(self) -> str:
        if self.collision_type in {
            "pp",
            "pAr",
            "pHe",
            "pH2",
            "pD2",
            "pN2",
            "pO2",
            "pNe",
            "pKr",
            "pXe",
            "Arp",
            "ppAr",
            "ppHe",
            "ppH2",
            "ppD2",
            "ppN2",
            "ppO2",
            "ppNe",
            "ppKr",
            "ppXe",
            "PbPb",
            "PbAr",
            "PbNe",
            "PbHe",
            "PbH2",
            "PbD2",
            "PbO2",
            "PbN2",
            "PbKr",
            "PbXe",
            "ArPb",
            "PbPbAr",
            "PbPbNe",
            "PbPbHe",
            "PbPbH2",
            "PbPbD2",
            "PbPbO2",
            "PbPbN2",
            "PbPbKr",
            "PbPbXe",
            "uniformHeadOn",
        }:
            if self.data_type in {
                "2011",
                "2012",
                "2015",
                "2015-5TeV",
                "2016",
                "2017",
                "2017-5TeV",
                "2018",
            }:
                version = "15"
            elif self.data_type in {
                "2022",
                "2023",
            }:
                version = "17a"
            elif self.data_type in {
                "2024.Q1.2",
                "2024.W25.27",
                "2024.W29.30",
                "2024.W31.34",
                "2024.W32.34",
                "2024.W31",
                "2024.W35.37",
                "2024.W37.39",
                "2024.W40.42",
                "expected-2024.Q3.4",
                "expected-2024-5TeV",
                "2024",
            }:
                version = "17b"
            else:
                raise NotImplementedError(self.data_type)
        else:
            raise NotImplementedError(self.collision_type)
        return version


class Sim10a(Sim10):
    """Sim10a

    Validation Release of Sim10
    First Gauss version used: v55r0
    Small double counting of some B --> KS branching fractions
    """

    default_gauss_version = "v55r4"
    default_gauss_binary_tag = "x86_64_v2-centos7-gcc11-opt"
    default_decfiles_pattern = "v31r*"

    tags = {
        "pp": {
            "2022": ("dddb-20221004", "sim-20221220-vc-{pol}"),
            "2018": ("dddb-20210528-8", "sim-20201113-8-vc-{pol}-Sim10"),
            "2017": ("dddb-20210528-7", "sim-20201113-7-vc-{pol}-Sim10"),
            "2016": ("dddb-20210528-6", "sim-20201113-6-vc-{pol}-Sim10"),
            "2015": ("dddb-20210528-5", "sim-20201113-5-vc-{pol}-Sim10"),
            "2012": ("dddb-20210528-2", "sim-20201113-2-vc-{pol}-Sim10"),
            "2011": ("dddb-20210528-1", "sim-20201113-1-vc-{pol}-Sim10"),
        },
        "pAr": {"2022": ("dddb-20221004", "sim-20221220-vc-md100")},
        "Arp": {"2022": ("dddb-20221004", "sim-20221220-vc-md100")},
        "ppAr": {"2022": ("dddb-20221004", "sim-20221220-vc-md100")},
    }


class Sim10b(Sim10a):
    """Sim10b

    First release for 2016-2018 + Upgrade Data
    First Gauss version used: v56r0
    See https://gitlab.cern.ch/lhcb/Gauss/-/tags/v56r0 and
    https://gitlab.cern.ch/lhcb/Gauss/-/tags/v56r1
    DDDB tags were changed through campaign, but as change made no difference to what we have done we did not change Sim version.
    """

    default_gauss_version = "v56r5"
    default_decfiles_pattern = "v32r*"

    tags = deep_update(
        Sim10a.tags,
        {
            "pp": {
                "2018": ("dddb-20220927-2018", "sim-20201113-8-vc-{pol}-Sim10"),
                "2017": ("dddb-20220927-2017", "sim-20201113-7-vc-{pol}-Sim10"),
                "2017-5TeV": ("dddb-20220927-2017", "sim-20230523-vc-{pol}-Sim10"),
                "2016": ("dddb-20220927-2016", "sim-20201113-6-vc-{pol}-Sim10"),
                "2015": ("dddb-20220927-2015", "sim-20201113-5-vc-{pol}-Sim10"),
                "2012": ("dddb-20220927-2012", "sim-20201113-2-vc-{pol}-Sim10"),
                "2011": ("dddb-20220927-2011", "sim-20201113-1-vc-{pol}-Sim10"),
            },
        },
    )


class Sim10c(Sim10b):
    """Sim10c

    First Gauss version used: v56r7
    See https://gitlab.cern.ch/lhcb/Gauss/-/tags/v56r7
    """

    default_gauss_version = "v56r7"
    default_decfiles_pattern = "v32r*"

    tags = deep_update(
        Sim10b.tags,
        {
            "pp": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
                "expected-2024.Q1.2": ("dddb-20240311", "sim-20231017-vc-{pol}"),
            },
            "pAr": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "Arp": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "ppAr": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "pHe": {
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "ppHe": {
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "PbPb": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "PbAr": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "ArPb": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
            "PbPbAr": {
                "2023": ("dddb-20231010", "sim-20231116-vc-{pol}"),
                "expected-2024": ("dddb-20231017", "sim-20231017-vc-{pol}"),
            },
        },
    )


class Sim10d(Sim10c):
    """Sim10d

    First Gauss version used: v56r8
    See https://gitlab.cern.ch/lhcb/Gauss/-/tags/v56r8
    """

    default_gauss_version = "v56r8"
    default_decfiles_pattern = "v32r*"
    pGas = [
        "pH2",
        "pD2",
        "pO2",
        "pN2",
        "pAr",
        "pHe",
        "pNe",
        "pKr",
        "pXe",
        "Arp",
        "ppH2",
        "ppD2",
        "ppO2",
        "ppN2",
        "ppAr",
        "ppHe",
        "ppNe",
        "ppKr",
        "ppXe",
    ]
    PbGas = [
        "PbH2",
        "PbD2",
        "PbO2",
        "PbN2",
        "PbAr",
        "PbNe",
        "PbHe",
        "PbKr",
        "PbXe",
        "ArPb",
        "PbPbH2",
        "PbPbD2",
        "PbPbO2",
        "PbPbN2",
        "PbPbAr",
        "PbPbNe",
        "PbPbHe",
        "PbPbKr",
        "PbPbXe",
    ]

    tags = deep_update(
        Sim10c.tags,
        {
            "pp": {
                "2024.Q1.2": ("dddb-20240427", "sim10-2024.Q1.2-v1.1-{pol}"),
                "2024.W25.27": ("dddb-20240427", "sim10-2024.W25.27-v1.2-{pol}"),
                "2024.W29.30": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
                "2024.W31.34": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
                "2024.W35.37": ("dddb-20240427", "sim10-2024.W35.37-v00.00-{pol}"),
                "2024.W37.39": ("dddb-20240427", "sim10-2024.W37.39-v00.00-{pol}"),
                "2024.W40.42": ("dddb-20240427", "sim10-2024.W40.42-v00.00-{pol}"),
                "expected-2024.Q3.4": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
                "expected-2024-5TeV": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
            },
            "PbPb": {
                "2024.Q1.2": ("dddb-20240427", "sim10-2024.Q1.2-v1.1-{pol}"),
                "expected-2024.Q3.4": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
            },
            **{
                pgas: {
                    "2024.Q1.2": ("dddb-20240427", "sim10-2024.Q1.2-v1.1-{pol}"),
                    "2024.W31.34": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
                    "expected-2024-5TeV": (
                        "dddb-20240427",
                        "sim10-2024.Q3.4-v1.3-{pol}",
                    ),
                }
                for pgas in pGas
            },
            **{
                Pbgas: {"2024.Q1.2": ("dddb-20240427", "sim10-2024.Q1.2-v1.1-{pol}")}
                for Pbgas in PbGas
            },
        },
    )


class Sim10e(Sim10d):
    """Sim10e

    First Gauss version used: v56r9
    See https://gitlab.cern.ch/lhcb/Gauss/-/tags/v56r9
    """

    default_gauss_version = "v56r9"
    default_decfiles_pattern = "v32r*"

    pGas = ["pH2", "pAr", "pHe", "pNe", "Arp", "ppH2", "ppAr", "ppHe", "ppNe"]
    tags = deep_update(
        Sim10d.tags,
        {
            "pp": {
                "2018": ("2018-v03.06", "sim-20201113-8-vc-{pol}-Sim10"),
                "2017": ("2017-v03.06", "sim-20201113-7-vc-{pol}-Sim10"),
                "2017-5TeV": ("2017-v03.06", "sim-20230523-vc-{pol}-Sim10"),
                "2016": ("2016-v03.06", "sim-20201113-6-vc-{pol}-Sim10"),
                "2015": ("2015-v03.06", "sim-20201113-5-vc-{pol}-Sim10"),
                "2013": ("2013-v03.06", "sim-20201113-3-vc-{pol}-Sim10"),
                "2012": ("2012-v03.06", "sim-20201113-2-vc-{pol}-Sim10"),
                "2011": ("2011-v03.06", "sim-20201113-1-vc-{pol}-Sim10"),
                "2010": ("2010-v03.06", "sim-20201113-0-vc-{pol}-Sim10"),
            },
            "PbPb": {
                "2024": ("dddb-20240427", "sim10-2024.W45.W47-v00.00-{pol}"),
            },
            **{
                pgas: {
                    "2024.W32.34": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
                    "2024.W31": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
                    "2024.W25.27": ("dddb-20240427", "sim10-2024.W25.27-v1.2-{pol}"),
                    "2024.W29.30": ("dddb-20240427", "sim10-2024.Q3.4-v1.3-{pol}"),
                    "2024.W35.37": ("dddb-20240427", "sim10-2024.W35.37-v00.00-{pol}"),
                    "2024.W37.39": ("dddb-20240427", "sim10-2024.W37.39-v00.00-{pol}"),
                }
                for pgas in pGas
            },
        },
    )
