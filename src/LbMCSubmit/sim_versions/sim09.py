###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from collections import defaultdict

from pydantic.utils import deep_update  # pylint: disable = no-name-in-module

from LbMCSubmit import beam_parameters
from LbMCSubmit.utils import classproperty, to_pol_str

from .simbase import SimBase


class Sim09(SimBase):
    # data_types defined in sub-versions of Sim09
    magnet_polarities = {"MagDown", "MagUp"}
    collision_types = {
        "pp",
        "pPb",
        "Pbp",
        "pNe",
        "pHe",
        "pAr",
        "PbNe",
        "PbPb",
        "uniformHeadOn",
    }
    file_formats = {
        "XGEN",
        "RGEN",
        "SIM",
        "XSIM",
        "DIGI",
        "XDIGI",
        "MDF",
        "DST",
        "MDST",
        "LDST",
        "XDST",
    }
    production_tools = {
        "default",
        "Pythia8",
        "Epos",
        "BcVegPy",
        "GenXicc",
        "SuperChic2",
        "StarLight",
        "Particle Gun",
    }
    tags = {collision_type: {} for collision_type in collision_types}
    decay_tools = {"EvtGen"}
    material_tools = {"Geant4"}

    default_pgun_pattern = "v2r*"
    default_madgraph_pattern = None  # not in Sim09
    default_powheg_pattern = None  # not in Sim09
    default_production_tool = "default"
    default_decay_tool = "EvtGen"
    default_material_tool = "Geant4"

    @classproperty
    def default_madgraph_version(cls) -> str:
        return None

    @classproperty
    def default_powheg_version(cls) -> str:
        return None

    @property
    def beam_parameters(self) -> dict:
        return beam_parameters

    @property
    def tcks(self) -> dict[str, str]:
        tcks = {
            "pp": defaultdict(dict),
            "pPb": defaultdict(dict),
            "pAr": defaultdict(dict),
            "pHe": defaultdict(dict),
            "pNe": defaultdict(dict),
            "PbNe": defaultdict(dict),
            "PbPb": defaultdict(dict),
        }

        tcks["pp"]["2011"]["L0"] = "0x0037"
        tcks["pp"]["2011"]["HLT"] = "0x40760037"

        tcks["pp"]["2012"]["L0"] = "0x0045"
        tcks["pp"]["2012"]["HLT"] = "0x409f0045"

        tcks["pp"]["2015"]["L0"] = "0x00a2"
        tcks["pp"]["2015"]["HLT"] = "0x411400a2"

        tcks["pp"]["2016"]["L0"] = "0x160F"
        tcks["pp"]["2016"]["HLT1"] = "0x5138160F"
        tcks["pp"]["2016"]["HLT2"] = "0x6139160F"

        tcks["pp"]["2017"]["L0"] = "0x1709"
        tcks["pp"]["2017"]["HLT1"] = "0x51611709"
        tcks["pp"]["2017"]["HLT2"] = "0x62661709"

        tcks["pp"]["2017-5TeV"]["L0"] = "0x1725"
        tcks["pp"]["2017-5TeV"]["HLT1"] = "0x51641725"
        tcks["pp"]["2017-5TeV"]["HLT2"] = "0x61641725"

        tcks["pp"]["2018"]["L0"] = "0x18a4"
        tcks["pp"]["2018"]["HLT1"] = "0x517a18a4"
        tcks["pp"]["2018"]["HLT2"] = "0x617d18a4"

        tcks["uniformHeadOn"] = tcks["pp"]

        tcks["pPb"]["2013"]["L0"] = "0x1710"
        tcks["pPb"]["2013"]["HLT"] = "0x406a1710"

        tcks["pPb"]["2016"]["L0"] = "0x1621"
        tcks["pPb"]["2016"]["HLT1"] = "0x51431621"
        tcks["pPb"]["2016"]["HLT2"] = "0x61421621"

        tcks["Pbp"] = tcks["pPb"]

        tcks["pAr"]["2015"]["L0"] = "0x024e"
        tcks["pAr"]["2015"]["HLT"] = "0x4113024e"

        tcks["pHe"]["2016"]["L0"] = "0x1620"
        tcks["pHe"]["2016"]["HLT1"] = "0x513d1620"
        tcks["pHe"]["2016"]["HLT2"] = "0x613c1620"

        tcks["pHe"]["2016-13TeV"]["L0"] = "0x1608"
        tcks["pHe"]["2016-13TeV"]["HLT1"] = "0x512e1608"
        tcks["pHe"]["2016-13TeV"]["HLT2"] = "0x612e1608"

        tcks["pNe"]["2017"]["L0"] = "0x1725"
        tcks["pNe"]["2017"]["HLT1"] = "0x51641725"
        tcks["pNe"]["2017"]["HLT2"] = "0x61641725"

        # Same as pHe 2016
        tcks["pNe"]["2017-13TeV"]["L0"] = "0x1620"
        tcks["pNe"]["2017-13TeV"]["HLT1"] = "0x513d1620"
        tcks["pNe"]["2017-13TeV"]["HLT2"] = "0x613c1620"

        tcks["PbNe"]["2018"]["L0"] = "0x1827"
        tcks["PbNe"]["2018"]["HLT1"] = "0x51861827"
        tcks["PbNe"]["2018"]["HLT2"] = "0x61851827"

        tcks["PbPb"]["2015"]["L0"] = "0x0243"
        tcks["PbPb"]["2015"]["HLT"] = "0x41260243"

        tcks["PbPb"]["2018"]["L0"] = "0x1827"
        tcks["PbPb"]["2018"]["HLT1"] = "0x51861827"
        tcks["PbPb"]["2018"]["HLT2"] = "0x61851827"

        return tcks[self.collision_type][self.data_type]

    @property
    def reco_version(self) -> str:
        reco_versions = {
            "pp": {
                "2011": "14c",
                "2012": "14c",
                "2015": "15a",
                "2015-5TeV": "15a",  # TODO
                "2016": "16",
                "2017": "17",
                "2017-5TeV": "17at5TeV",
                "2018": "18",
            },
            "pPb": {
                "2013": "14r1",
                "2016": "16pLead",
            },
            "pAr": {"2015": "16Smog"},
            "pHe": {"2016": "16Smog", "2016-13TeV": "16Smog"},
            "pNe": {"2017": "17aSmog", "2017-13TeV": "17aSmog"},
            "PbNe": {"2018": "18Smog"},
            "PbPb": {
                "2015": "15aLead15",
                "2018": "18Lead",
            },
        }
        reco_versions["uniformHeadOn"] = reco_versions["pp"]
        reco_versions["Pbp"] = reco_versions["pPb"]

        return reco_versions[self.collision_type][self.data_type]

    @property
    def turbo_version(self) -> str:
        turbo_versions = {
            "pp": {
                "2015": "02",
                "2016": "03a",
                "2017": "04a-WithTurcal",
                "2018": "05-WithTurcal",
            },
            "pPb": {"2016": "03a"},
            "pAr": {},
            "pHe": {},
            "pNe": {},
            "PbNe": {},
            "PbPb": {},
        }
        turbo_versions["uniformHeadOn"] = turbo_versions["pp"]
        turbo_versions["Pbp"] = turbo_versions["pPb"]

        return turbo_versions[self.collision_type][self.data_type]

    @property
    def stripping_version(self) -> str:
        stripping_versions = {
            "pp": {
                "2011": "21r1",
                "2012": "21",
                "2013": "20r2",  # 2.76 TeV
                "2015": "24r2",
                "2015-5TeV": "22",
                "2016": "28r2",
                "2017": "29r2",
                "2017-5TeV": "32",
                "2018": "34",
            },
            "Pbp": {
                "2013": "20r3p1",  # Not found in http://cern.ch/lhcbdoc/stripping/
                "2016": "30r2",
            },
            "pPb": {
                "2013": "20r3p1",  # Not found in http://cern.ch/lhcbdoc/stripping/
                "2016": "30r3",
                "2018": "35",
            },
            "PbPb": {
                "2015": "31r2",
                "2018": "35r2",
            },
            "pAr": {
                "2015": "25",
            },
            "PbAr": {
                "2015": "31r1",
            },
            "pHe": {
                "2016": "27",
            },
            "pNe": {
                "2017": "33r2",
            },
            "PbNe": {
                "2018": "35r3",
            },
        }
        stripping_versions["uniformHeadOn"] = stripping_versions["pp"]

        return stripping_versions[self.collision_type][self.data_type]

    # Spucing not supported in Sim09
    @property
    def sprucing_version(self) -> str:
        return None

    def apply_stage_5(self, data: dict):
        data = super().apply_stage_5(data)
        if data["generation"]["production-tool"] != "default":
            return data

        if data["collision-type"] in {"pp", "uniformHeadOn"}:
            data["generation"]["production-tool"] = "Pythia8"
        else:
            data["generation"]["production-tool"] = "Epos"
        return data

    @property
    def db_tags(self) -> tuple[str, str]:
        self.tags["uniformHeadOn"] = self.tags["pp"]
        dddb, simcond = self.tags[self.collision_type][self.data_type]

        return (dddb, simcond.format(pol=to_pol_str(self.polarity)))

    @property
    def digi_version(self) -> str:
        if self.collision_type in self.collision_types:
            version = "14c"
        else:
            raise NotImplementedError(self.collision_type)
        return version


class Sim09a(Sim09):
    r"""Sim09a

        Generators: * 2015: lower tracking $\epsilon$ due to 2nd metal layer problem * EvtGen updated to PDG2014 particle properties
        Geant4 9.6, no significant differences observed
        VELO: error parameterisation and 2nd metal layer parameterisation

    Known issues

        2015: lower tracking efficiency due to 2nd metal layer problem
    """  # noqa: B950

    data_types = {"2011", "2012", "2013", "2015", "2015-5TeV"}
    default_gauss_version = "v49r4"
    default_gauss_binary_tag = "x86_64-slc6-gcc48-opt"
    default_decfiles_pattern = "v29r*"

    tags = {
        "pp": {
            "2010": ("dddb-20160318", "sim-20160614-0-vc-{pol}"),
            "2011": ("dddb-20160318-1", "sim-20160614-1-vc-{pol}"),
            "2012": ("dddb-20150928", "sim-20160321-2-vc-{pol}"),
            "2013": ("dddb-20150928", "sim-20160321-3-vc-{pol}"),  # 2.75 TeV
            "2015": ("dddb-20150724", "sim-20160606-vc-{pol}"),
            "2015-early": ("dddb-20150724", "sim-20160321-vc-{pol}"),
            "2015-5TeV": ("dddb-20150724", "sim-20160623-vc-{pol}"),
            "2016-summer": ("dddb-20150724", "sim-20160820-vc-{pol}"),
        },
    }


class Sim09b(Sim09a):
    r"""Sim09b

        First Gauss version used: v49r5
        First release for 2016
        Fixed 2nd metal layer handling
        Updated $\tau$ handling with Tauola
        Change in SIMCOND tag for 2016, see https://its.cern.ch/jira/browse/LHCBGAUSS-918

    Known issues

        2016: L0 ECAL calibration correct (but incorrect in data)
    """  # noqa: B950

    data_types = Sim09a.data_types | {"2016", "2016-13TeV"}
    default_gauss_version = "v49r7"

    tags = deep_update(
        Sim09a.tags,
        {
            "pp": {
                "2015": ("dddb-20150724", "sim-20161124-vc-{pol}"),
                "2016": ("dddb-20150724", "sim-20161124-2-vc-{pol}"),
                "2015-early": ("dddb-20150724", "sim-20161124-2015early-vc-{pol}"),
                "2015-5TeV": ("dddb-20150724", "sim-20161124-2015at5TeV-vc-{pol}"),
                "2016-summer": ("dddb-20150724", "sim-20161124-1-vc-{pol}"),
            },
            "pPb": {
                "2013": ("dddb-20150928", "sim-20170407-2013pPb-vc-{pol}"),
                "2016": ("dddb-20150724", "sim-20161215-2016pPbAt8TeV-vc-{pol}"),
            },
            "Pbp": {
                "2013": ("dddb-20150928", "sim-20170407-2013Pbp-vc-{pol}"),
                "2016": ("dddb-20150724", "sim-20161215-2016PbpAt8TeV-vc-{pol}"),
            },
            "PbPb": {
                "2015": ("dddb-20150724", "sim-20161215-2015PbPb-vc-{pol}"),
            },
            # 2024-09-27 we retroactively call this "2016-13TeV" because a lot
            # more data was collected with 4 TeV beams later in the year
            "pHe": {
                "2016-13TeV": (
                    "dddb-20150724",
                    "sim-20170516-2016pHeBeam6500-vc-{pol}",
                ),
            },
            "pAr": {
                "2015": ("dddb-20150724", "sim-20161215-2015pAr-vc-{pol}"),
            },
        },
    )


class Sim09c(Sim09b):
    r"""Sim09c

        First Gauss version used: v49r8
        Set correctly incorrect ECAL threshold for 2016
        New major DecFiles version and corresponding ParticleTable for light baryons
        Fixes to EvtDDalitz (for \Dp)
        Fixed incorrect handling of $\Bs\rightarrow \bar{K}^{*}\mup\mum$, see https://its.cern.ch/jira/browse/LHCBGAUSS-1097
        Change of DDDB tag for all years and SIMCOND tags for 2015 and 2016, see details at https://its.cern.ch/jira/browse/LHCBGAUSS-918

    Known issues

        None
    """  # noqa: B950

    default_gauss_version = "v49r9"
    default_decfiles_pattern = "v30r*"

    tags = deep_update(
        Sim09b.tags,
        {
            "pp": {
                "2010": ("dddb-20170721", "sim-20160614-0-vc-{pol}"),
                "2011": ("dddb-20170721-1", "sim-20160614-1-vc-{pol}"),
                "2012": ("dddb-20170721-2", "sim-20160321-2-vc-{pol}"),
                "2013": ("dddb-20170721-2", "sim-20160321-3-vc-{pol}"),
                "2015": ("dddb-20170721-3", "sim-20161124-vc-{pol}"),
                "2016": ("dddb-20170721-3", "sim-20170721-2-vc-{pol}"),
                "2015-early": ("dddb-20170721-3", "sim-20161124-2015early-vc-{pol}"),
                "2015-5TeV": ("dddb-20170721-3", "sim-20161124-2015at5TeV-vc-{pol}"),
                "2016-summer": ("dddb-20170721-3", "sim-20170721-1-vc-{pol}"),
            },
            "pPb": {
                "2013": ("dddb-20170721-2", "sim-20170407-2013pPb-vc-{pol}"),
                "2016": ("dddb-20170721-3", "sim-20170721-2016pPbAt8TeV-vc-{pol}"),
            },
            "Pbp": {
                "2013": ("dddb-20170721-2", "sim-20170407-2013Pbp-vc-{pol}"),
                "2016": ("dddb-20170721-3", "sim-20170721-2016PbpAt8TeV-vc-{pol}"),
            },
            "PbPb": {
                "2015": ("dddb-20170721-3", "sim-20161215-2015PbPb-vc-{pol}"),
            },
            "pHe": {
                "2016-13TeV": (
                    "dddb-20170721-3",
                    "sim-20170516-2016pHeBeam6500-vc-{pol}",
                ),
            },
            "pAr": {
                "2015": ("dddb-20170721-3", "sim-20161215-2015pAr-vc-{pol}"),
            },
        },
    )


class Sim09d(Sim09c):
    r"""Sim09d

        First Gauss version used: v49r10
        Updated particle properties (when Pythia called by EvtGen), for decays to partons
        Resonance from Pythia with non-zero width
        Modified model for $B_c^-\rightarrow\Dstz\mum\antinumu$

    Known issues

        Underlying event multiplicity due to handling of wide resonances, see https://indico.cern.ch/event/787606/contributions/3272825/attachments/1830771/2998119/Sim09c_vs_def.pdf
    """  # noqa: B950

    default_gauss_version = "v49r10"


class Sim09e(Sim09d):
    """
    Sim09e

        First Gauss version used: v49r11
        First release for 2017
        Changes to quarkonia from Pythia8, see https://its.cern.ch/jira/browse/LHCBGAUSS-1167.

    Known issues

        Underlying event multiplicity due to handling of wide resonances, see https://indico.cern.ch/event/787606/contributions/3272825/attachments/1830771/2998119/Sim09c_vs_def.pdf
        2017: Incorrect ECAL threshold used (same as 2016 data)
    """  # noqa: B950

    data_types = Sim09d.data_types | {"2017"}
    default_gauss_version = "v49r11"

    tags = deep_update(
        Sim09d.tags,
        {
            "pp": {
                "2017": ("dddb-20170721-3", "sim-20180411-vc-{pol}"),
            },
            "pNe": {
                "2017": (
                    "dddb-20170721-3",
                    "sim-20181008-2017pNeBeam2500-vc-{pol:.2s}",
                ),
            },
        },
    )


class Sim09f(Sim09e):  # pylint: disable=R0901
    r"""Sim09f

        First Gauss version used: v49r12
        First release for 2018
        Fix for angular model for $\Lbz \rightarrow \Lz\lepp\lepm$
        Fix for BeautyTomuCharmTo3h to select oscillated particles

    Known issues

        Underlying event multiplicity due to handling of wide resonances, see https://indico.cern.ch/event/787606/contributions/3272825/attachments/1830771/2998119/Sim09c_vs_def.pdf
        2017+2018: Incorrect ECAL threshold used (same as 2016 data)
    """  # noqa: B950

    data_types = Sim09e.data_types | {"2018"}
    default_gauss_version = "v49r12"

    tags = deep_update(
        Sim09e.tags,
        {
            "pp": {
                "2018": ("dddb-20170721-3", "sim-20190128-vc-{pol}"),
            },
        },
    )


class Sim09g(Sim09f):  # pylint: disable=R0901
    """
    Sim09g

        First Gauss version used: v49r13
        Only bug-fixes
        Change of SIMCOND tags for 2017 and 2018, see details at https://its.cern.ch/jira/browse/LHCBGAUSS-918

    Known issues

        Pythia8 can generate a10 that EvtGen cannot decay, resulting in infinite loop (does not affect physics)
        Broken signal particle gun
    """  # noqa: B950

    data_types = Sim09f.data_types | {"2017-5TeV"}
    default_gauss_version = "v49r13"

    tags = deep_update(
        Sim09f.tags,
        {
            "pp": {
                "2017": ("dddb-20170721-3", "sim-20190430-1-vc-{pol}"),
                "2017-5TeV": (
                    "dddb-20170721-3",
                    "sim-20181008-2017pNeBeam2500-vc-{pol:.2s}",
                ),
                "2018": ("dddb-20170721-3", "sim-20190430-vc-{pol}"),
            },
            "pHe": {
                "2016": ("dddb-20170721-3", "sim-20170907-2016pHeBeam4000-vc-{pol}"),
                "2016-13TeV": (
                    "dddb-20170721-3",
                    "sim-20170721-2016pHeBeam6500-vc-{pol}",
                ),
            },
        },
    )


class Sim09h(Sim09g):  # pylint: disable=R0901
    """Sim09h

        First Gauss version used: v49r14
        Fixed Sim09g problem in EvtGen itself
        Many updates to AmpGen, see https://gitlab.cern.ch/lhcb/Gauss/merge_requests/438
        New model for Lc2pKpi

    Known issues

        Broken signal particle gun
    """  # noqa: B950

    default_gauss_version = "v49r16"

    tags = deep_update(
        Sim09g.tags,
        {
            "PbNe": {
                "2018": ("dddb-20170721-3", "sim-20190507-vc-{pol}-Sim09"),
            },
            "PbPb": {
                "2018": ("dddb-20170721-3", "sim-20190507-vc-{pol}-Sim09"),
            },
        },
    )


class Sim09i(Sim09h):  # pylint: disable=R0901
    """
    Sim09i

        First Gauss version used: v49r17
        new default PDF for SuperChic2 (https://gitlab.cern.ch/lhcb/Gauss/-/issues/9)
    """  # noqa: B950

    default_gauss_version = "v49r17"


class Sim09j(Sim09i):  # pylint: disable=R0901
    """Sim09j

    Please see https://gitlab.cern.ch/lhcb/Gauss/-/issues/11

        First Gauss version used: v49r18
        New decay models in EvtGen, MR !626 (merged) and port of !568 from master/HepForge. Does not effect physics
        Update and new models in AmpGen, MR !640 (merged), triggering letter change
        Allow axions to be generated with StarLight, !643 (merged)
        Fix issues with gluons from Pythia8 without end vertex, !645 (merged), should not effect physics, but triggers letter change
    """  # noqa: B950

    default_gauss_version = "v49r19"


class Sim09k(Sim09j):  # pylint: disable=R0901
    """Sim09k

    Please see https://gitlab.cern.ch/lhcb/Gauss/-/issues/18

        First Gauss version used: v49r20
        cut for diBoson events !672
        new decay model for hyperons !676
        bug fix in a decay model for Bs -> mu mu K K, triggering the new Sim09 version !693
        new 4-body phase-space decay model in restricted mass window
    """  # noqa: B950

    default_gauss_version = "v49r21"


class Sim09l(Sim09k):  # pylint: disable=R0901
    """Sim09l

    Please see https://gitlab.cern.ch/lhcb/Gauss/-/issues/41

        First Gauss version used: v49r22
        Update of Bc_VHAD model to include new decays
        New signal allowed with EPOS
        Improvement in polarisation exchange between starlight and EvtGen
        Bug fix for a generator level cut. See LHCBGAUSS-2468 and !792
    """

    default_gauss_version = "v49r24"


class Sim09m(Sim09l):  # pylint: disable=R0901
    """Sim09m

    Please see https://gitlab.cern.ch/lhcb/Gauss/-/issues/104

        First Gauss version used: v49r25
        Update of EvtPhiDalitz for phi->pipipi0 to describe helicity angle distributions and bug fix in EvtVector3R model
        Change of SIMCOND tags for 2017 pNe, see details at https://its.cern.ch/jira/browse/LHCBGAUSS-918
    """

    data_types = Sim09l.data_types | {"2017-13TeV"}
    default_gauss_version = "v49r25"

    tags = deep_update(
        Sim09l.tags,
        {
            "pp": {
                "2017-5TeV": (
                    "dddb-20170721-3",
                    "sim-20190501-2-vc-{pol}",
                ),
            },
            "pNe": {
                "2017": (
                    "dddb-20170721-3",
                    "sim-20190501-2-vc-{pol}",
                ),
                "2017-13TeV": (
                    "dddb-20170721-3",
                    "sim-20210701-vc-{pol}",
                ),
            },
        },
    )
