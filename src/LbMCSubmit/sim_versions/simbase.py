###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import inspect
import re
from abc import ABC
from collections import defaultdict
from copy import deepcopy

import LbMCSubmit.processing_passes as pp
from LbMCSubmit import datapkgs, eventtypes, utils
from LbMCSubmit.properties import Properties, Stage6Properties
from LbMCSubmit.steps import digitisation, generation, merging, offline, online
from LbMCSubmit.utils import (
    abstractproperty,
    add_compression_to_step,
    classproperty,
    run_from_data_type,
)
from LbMCSubmit.versioning import get_app_info


class SimBase(ABC):  # noqa: B024
    data_types: set[str]
    magnet_polarities: set[str]
    collision_types: set[str]
    file_formats: set[str]

    production_tools: set[str]
    decay_tools: set[str]
    material_tools: set[str]

    default_production_tool: str
    default_decay_tool: str
    default_material_tool: str

    default_gauss_version: str = abstractproperty()
    default_gauss_binary_tag: str = abstractproperty()
    default_decfiles_pattern: str = abstractproperty()
    default_pgun_pattern: str = abstractproperty()
    default_madgraph_pattern: str = abstractproperty()
    default_powheg_pattern: str = abstractproperty()

    beam_parameters: dict = abstractproperty()
    tcks: dict[str, str] = abstractproperty()
    reco_version: str = abstractproperty()
    turbo_version: str = abstractproperty()
    stripping_version: str = abstractproperty()
    sprucing_version: str = abstractproperty()
    digi_version: str = abstractproperty()
    db_tags: tuple[str, str] = abstractproperty()

    @classproperty
    def default_decfiles(cls) -> str:
        return datapkgs.latest_datapackage("Gen/DecFiles", cls.default_decfiles_pattern)

    @classproperty
    def default_pgun_version(cls) -> str:
        return datapkgs.latest_datapackage("Gen/PGunsData", cls.default_pgun_pattern)

    @classproperty
    def default_madgraph_version(cls) -> str:
        return datapkgs.latest_datapackage(
            "Gen/MadgraphData", cls.default_madgraph_pattern
        )

    @classproperty
    def default_powheg_version(cls) -> str:
        return datapkgs.latest_datapackage(
            "Gen/PowhegboxData", cls.default_powheg_pattern
        )

    @classproperty
    def eventtypes(cls) -> list[str]:
        return list(eventtypes.available(cls.default_decfiles))

    @classproperty
    def version_tuple(cls) -> tuple[str, str]:
        if match := re.fullmatch(
            r"Sim(\d{2})([a-z])", cls.__name__  # pylint: disable=no-member
        ):
            assert not inspect.isabstract(cls), f"{cls} has abstract methods"
            major, minor = match.groups()
        else:
            raise TypeError(f"The class {cls} is not an valid Sim version")
        return major, minor

    @classproperty
    def version(cls) -> str:
        major, minor = cls.version_tuple
        return f"{major}{minor}"

    @classproperty
    def available_versions(cls) -> dict[str, type[SimBase]]:
        to_search: list[type[SimBase]] = (
            cls.__subclasses__()  # pylint: disable=no-member
        )
        versions: dict[str, dict[str, type[SimBase]]] = defaultdict(dict)
        while to_search:
            x = to_search.pop()
            to_search.extend(x.__subclasses__())
            try:
                major, minor = x.version_tuple
            except TypeError:
                pass
            else:
                versions[major][minor] = x

        result = {}
        for major, minor_verions in versions.items():
            result.update({f"{major}{k}": v for k, v in minor_verions.items()})
            result[major] = minor_verions[max(minor_verions)]

        return result

    @property
    def collision_type(self) -> str:
        return self.properties.collision_type

    @property
    def data_type(self) -> str:
        return self.properties.data_type

    @property
    def smog(self) -> bool:
        return self.properties.smog

    @property
    def polarity(self) -> str:
        return self.properties.polarity

    @property
    def file_format(self) -> str:
        return self.properties.file_format

    @classmethod
    def apply_stage(cls, sample: dict, stage: int) -> dict:
        SimClass = cls.available_versions[sample["sim-version"]](
            sample["collision-type"],
            sample["smog"],
            sample["generation"]["production-tool"] == "Particle Gun",
            str(sample["data-type"]),
            sample["magnet-polarity"],
            sample["file-format"],
            sample.get("pileup-nu"),
        )
        return getattr(SimClass, f"apply_stage_{stage}")(sample)

    @property
    def default_digi(self) -> dict[str, str]:
        version = self.digi_version
        return {"version": version} | pp.DIGI[version]

    @property
    def default_trigger(self) -> dict[str, dict[str, str]]:
        tcks = self.tcks

        trigger = {
            level: {"TCK": tcks[level]} | getattr(pp, level)[tcks[level]]
            for level in ["L0", "HLT", "HLT1", "HLT2"]
            if level in tcks
        }
        return trigger

    @property
    def default_reconstruction(self) -> dict[str, str]:
        try:
            version = self.reco_version
            return {"version": version} | pp.RECO[version]
        except KeyError:
            return None

    @property
    def default_tesla(self) -> dict[str, str]:
        try:
            version = self.turbo_version
            return {"version": version} | pp.TURBO[version]
        except KeyError:
            return None

    @property
    def default_stripping(self) -> dict[str, str]:
        try:
            version = self.stripping_version
            return {"version": version} | pp.STRIPPING[version]
        except KeyError:
            return None

    @property
    def default_sprucing(self) -> dict[str, str]:
        try:
            version = self.sprucing_version
            return {"version": version} | pp.SPRUCING[version]
        except KeyError:
            return None

    def __init__(
        self,
        collision_type: str,
        smog: bool,
        pgun: bool,
        data_type: str,
        polarity: str,
        file_format: str,
        custom_nu: str,
    ):
        self.properties = Properties(
            collision_type=collision_type,
            data_type=data_type,
            smog=smog,
            pgun=pgun,
            polarity=polarity,
            sim_version=self.__class__.__name__,
            file_format=file_format,
            decfiles=self.default_decfiles,
            custom_nu=custom_nu,
        )

    # Transformations
    def apply_stage_4(self, data: dict):
        data["generation"]["application"] = {
            "name": "Gauss",
            "version": self.default_gauss_version,
            "binary_tag": self.default_gauss_binary_tag,
        }
        data["generation"]["beam"] = self.beam_parameters.get(self.properties)
        data["digitisation"] = deepcopy(self.default_digi)
        data["trigger"] = deepcopy(self.default_trigger)
        data["reconstruction"] = deepcopy(self.default_reconstruction)
        data["tesla"] = deepcopy(self.default_tesla)
        # Allow stripping version to be customised early on
        if self.default_stripping:
            if version := data.get("stripping", {}).get("version"):
                default_stripping = pp.STRIPPING[version]
            else:
                default_stripping = self.default_stripping
            data["stripping"] = deepcopy(default_stripping | data.get("stripping", {}))
            if "output-name" not in data["stripping"]:
                if (
                    analysis_name := data["stripping"]
                    .get("filter", {})
                    .get("analysis-name")
                ):
                    output_name = f"{analysis_name}.Strip.{self.file_format}"
                else:
                    output_name = f"ALLSTREAMS.{self.file_format}"
                data["stripping"]["output-name"] = output_name

            # Ensure all-caps and sanity check on file suffix
            data["stripping"]["output-name"] = data["stripping"]["output-name"].upper()
            assert data["stripping"]["output-name"].endswith("." + self.file_format)
        else:
            # May seem redundant but handles case where there is no default
            # stripping step *and* no stripping specified by the user
            data["stripping"] = data.get("stripping", {})
        # Add optional sprucing if it is requested by user
        if data.get("sprucing", {}):
            if isinstance(data.get("sprucing"), bool) and data.get("sprucing"):
                sprucingVersion = self.default_sprucing
                if sprucingVersion:
                    data["sprucing"] = sprucingVersion
                else:
                    raise NotImplementedError(
                        "Requested sprucing without specifying version for data-type which does not have default sprucing defined"
                    )
            elif isinstance(data.get("sprucing"), dict):
                sprucingVersion = data.get("sprucing").get("version")
                data["sprucing"].update(pp.SPRUCING[sprucingVersion])
            else:
                sprucingVersion = data.get("sprucing", "")
                data["sprucing"] = {"version": sprucingVersion}
                data["sprucing"].update(pp.SPRUCING[sprucingVersion])

        data["dbtags"] = dict(zip(["DDDB", "CondDB"], self.db_tags))
        # TODO: This needs to be generalised somehow
        data.setdefault("fast-mc", {})
        if redecay := data["fast-mc"].get("redecay"):
            data["fast-mc"]["redecay"] = {
                "mode": "signal-and-heavier",
                "num-redecays": 100,
            }
            if isinstance(redecay, dict):
                data["fast-mc"]["redecay"].update(redecay)
        if splitsim := data["fast-mc"].get("splitsim"):
            data["fast-mc"]["splitsim"] = {
                "mode": "default",
            }
            if isinstance(splitsim, dict):
                data["fast-mc"]["splitsim"].update(splitsim)
        if data["fast-mc"].get("tracker-only"):
            for unneeded_stage in ["trigger", "tesla", "stripping"]:
                if unneeded_stage in data:
                    data[unneeded_stage] = {}
        return data

    def apply_stage_5(self, data: dict):
        if data["generation"]["production-tool"] == "Particle Gun":
            assert not data["fast-mc"], "Fast MC is not supported with Particle Gun"
            data["trigger"] = {}
        return data

    def apply_stage_6(self, data: dict):
        properties = Stage6Properties(
            **self.properties.asdict(),
            spillover=bool(data["generation"]["beam"]["spillover"]),
            pgun_version=f"Gen/PGunsData.{self.default_pgun_version}",
            madgraph_version=f"Gen/MadgraphData.{self.default_madgraph_version}",
            powheg_version=f"Gen/PowhegboxData.{self.default_powheg_version}",
        )
        verbose_data_type = properties.data_type
        if alt_energy_match := re.match(r"(20\d\d)-([\d\.]+TeV)", properties.data_type):
            properties.data_type = alt_energy_match.groups()[0]
        steps = []
        if any(
            self.file_format.endswith(x) for x in ["GEN", "SIM", "DIGI", "MDF", "DST"]
        ):

            run = run_from_data_type(properties.data_type)
            # Generation
            steps += generation.get_steps(
                properties, data["generation"], data["fast-mc"], data["dbtags"]
            )
            if any(self.file_format.endswith(x) for x in ["DIGI", "MDF", "DST"]):
                # Digitisation
                gen_output = [x["type"] for x in steps[-1]["output"]]
                # Really assume the Gauss step only has 1 output filetype
                assert len(gen_output) == 1
                steps += digitisation.get_steps(
                    properties, data["digitisation"]["version"], gen_output[0]
                )
                # MDF should only be written by Boole (maybe HLT in future)
                if self.file_format != "MDF":
                    # We want to run only Boole for XDIGI. For DIGI in
                    # Run 1/2 we run trigger, but in Run 3 we skip trigger.
                    # DST needs offline processing in Run 1/2, but in Run 3
                    # also trigger belongs to DST.
                    if run == 3 and not self.file_format.endswith("DIGI"):
                        if data["trigger"]:
                            steps += online.get_steps(properties, data["trigger"])
                            steps += offline.get_steps(properties, data)
                        else:
                            raise NotImplementedError(
                                f"{properties.data_type} cannot produce DST without trigger"
                            )
                    elif run < 3:  # This is case of Run 1/2
                        # Online
                        if data["trigger"] and not self.file_format.endswith("XDIGI"):
                            steps += online.get_steps(properties, data["trigger"])
                        # Offline
                        if self.file_format.endswith("DST"):
                            steps += offline.get_steps(properties, data)
        else:
            raise NotImplementedError(f"{self.file_format=} not supported")
        # Merging
        last_step_output = [x["type"] for x in steps[-1]["output"]]
        filtered = any(
            [
                data.get("retention-rate", 1) < 1,
                "filter" in data.get("stripping", {}),
                "filtering-script" in data.get("stripping", {}),
                data["fast-mc"].get("splitsim"),
            ]
        )

        if filtered and "retention-rate" not in data:
            raise ValueError("Please specify the retention-rate")

        if self.file_format.endswith("DST") and (
            filtered or self.file_format == "MDST"
        ):
            steps += merging.get_steps(properties.data_type, last_step_output)

        # Ensure that final step output is visible
        for file_type in steps[-1]["output"]:
            file_type["visible"] = True

        if isinstance(data["event-types"], dict) and "num-events" in data.keys():
            raise ValueError("Can't use num-events if specified using a dictionary")

        evt_types = [
            {
                "id": k,
                "num_events": (
                    utils.parse_num_event(data["event-types"][k])
                    if isinstance(data["event-types"], dict)
                    else utils.parse_num_event(data["num-events"])
                ),
                "num_test_events": data["num-test-events"],
            }
            for k in data["event-types"]
        ]

        # Use fast compression for all steps except the last one

        for i, step in enumerate(steps[:-1]):
            this_app = get_app_info(step)
            next_app = get_app_info(steps[i + 1])
            comp_alg = (
                "ZSTD" if this_app["read_zstd"] and next_app["write_zstd"] else "ZLIB"
            )
            add_compression_to_step(step, comp_alg, 1)
        # Use higher compression for the last step
        add_compression_to_step(steps[-1], "LZMA", 4)

        data = {
            "type": "Simulation",
            "priority": data["priority"],
            "name": f"{data['name']} {verbose_data_type} "
            f"{properties.collision_type} {properties.polarity}",
            "mc_config_version": properties.data_type,
            "sim_condition": properties.conditions_string,
            "author": None,
            "inform": deepcopy(data["inform"]),
            "comment": data["comment"],
            "wg": data["WG"],
            "retention_rate": data.get("retention-rate", 1.0),
            "event_types": evt_types,
            "steps": steps,
        }
        if properties.data_type.startswith("expected"):
            data["mc_config_version"] = "Dev"
        data["mc_config_version"] = re.sub(
            r"(\d{4})(\.(Q|W).*)", r"\1", data["mc_config_version"]
        )
        if properties.data_type == "2024.Q1.2" and "Pb" in properties.collision_type:
            data["mc_config_version"] = "Dev"

        assert (
            data["mc_config_version"] in {"Upgrade", "Dev"}
            or 2008 <= int(data["mc_config_version"]) <= 2041
        )

        if properties.fast_simulation_type:
            data["fast_simulation_type"] = properties.fast_simulation_type
        return data
