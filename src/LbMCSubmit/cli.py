###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

import typer
import yaml
from rich.console import Console
from rich.text import Text

from LbMCSubmit import run

_verbose = typer.Option(0, "--verbose", "-v", count=True)
_in_stage = typer.Option(0, min=0, max=5)
_out_stage = typer.Option(6, min=1, max=6)


def main(
    input_path: Path,
    output_path: Path,
    verbose: int = _verbose,
    validation: bool = True,
    in_stage: int = _in_stage,
    out_stage: int = _out_stage,
):
    console = Console(stderr=True)
    console.print(Text(f"Loading {input_path}"))

    data = run(
        input_path,
        in_stage,
        out_stage,
        verbose=verbose,
        validation=validation,
        log_fcn=lambda s: console.print(Text(s)),
    )

    yaml.Dumper.ignore_aliases = lambda *args: True
    output_path.write_text(yaml.dump(data, sort_keys=False))


def app() -> None:
    typer.run(main)
