###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from copy import copy

from LbMCSubmit.properties import Properties


def get(properties: Properties):
    if properties.collision_type == "uniformHeadOn":
        result = get_headOn(properties)
    elif get_func := globals().get(f"get_{properties.collision_type}"):
        result = get_func(properties)
    else:
        raise NotImplementedError(properties.collision_type)

    assert set(result.keys()) == {
        "energy",
        "PrimaryvertexLocation",
        "SpecialDetectorConfigurations",
        "PileUp",
        "spillover",
        "beamfile-suffix",
    }
    return result


DEFAULT_BEAM_PARAMETERS = {
    # Keys that are almost always empty
    "PrimaryvertexLocation": "",
    "SpecialDetectorConfigurations": "",
    "beamfile-suffix": "",
}


def get_pp(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2022"}:
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["SpecialDetectorConfigurations"] = "NoUT"
        nu = {
            "MagDown": "2.1",
            "MagUp": "3.2",
        }[properties.polarity]
        beam_parameters["PileUp"] = f"Nu{nu}"
        beam_parameters["spillover"] = {"spacing": "25ns"}
    elif properties.data_type in {"2023"}:
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu1.6"
        beam_parameters["spillover"] = {"spacing": "25ns"}
    elif properties.data_type in {"2024.Q1.2", "expected-2024.Q3.4", "2024.W40.42"}:
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6"
        beam_parameters["spillover"] = {"spacing": "25ns"}
    elif properties.data_type in {
        "2024.W25.27",
        "2024.W29.30",
        "2024.W31.34",
        "2024.W35.37",
        "2024.W37.39",
    }:
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3"
        beam_parameters["spillover"] = {"spacing": "25ns"}
    elif properties.data_type in {"expected-2024-5TeV"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Nu2.9"
        beam_parameters["spillover"] = {"spacing": "25ns"}
    elif properties.data_type in {"2015", "2016", "2017", "2018"}:
        beam_parameters["energy"] = "Beam6500GeV"
        beam_parameters["PileUp"] = "Nu1.6"
        beam_parameters["spillover"] = {"spacing": "25ns"}
    elif properties.data_type in {"2015-5TeV", "2017-5TeV"}:
        beam_parameters["energy"] = "Beam2510GeV"
        beam_parameters["PileUp"] = "Nu1.5"
        beam_parameters["spillover"] = {"spacing": "25ns"}
    elif properties.data_type in {"2012"}:
        beam_parameters["energy"] = "Beam4000GeV"
        beam_parameters["PileUp"] = "Nu2.5"
        beam_parameters["spillover"] = False
    elif properties.data_type in {"2011"}:
        beam_parameters["energy"] = "Beam3500GeV"
        beam_parameters["PileUp"] = "Nu2"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    if custom_nu := properties.custom_nu:
        custom_nu = properties.custom_nu
        beam_parameters["PileUp"] = f"Nu{custom_nu}"

    if properties.pgun:
        beam_parameters.update(
            {
                "spillover": False,
            }
        )

    return beam_parameters


def get_headOn(properties: Properties):
    beam_parameters = {
        "PileUp": "Fix1-UniformHeadOn",
        "spillover": False,
        "PrimaryvertexLocation": "",
        "SpecialDetectorConfigurations": "",
        "beamfile-suffix": "fix1",
    }
    if properties.data_type in {"2015", "2016", "2017", "2018"}:
        beam_parameters["energy"] = "Beam6500GeV"
    elif properties.data_type == "2012":
        beam_parameters["energy"] = "Beam4000GeV"
    elif properties.data_type == "2011":
        beam_parameters["energy"] = "Beam3500GeV"
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_pPb(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2016":
        beam_parameters["energy"] = "Beam6500GeV-2560GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Fix1"
    elif properties.data_type == "2013":
        beam_parameters["energy"] = "Beam4000GeV-1580GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["beamfile-suffix"] = "v2"
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_Pbp(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2016":
        beam_parameters["energy"] = "Beam2560GeV-6500GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Fix1"
    elif properties.data_type == "2013":
        beam_parameters["energy"] = "Beam1580GeV-4000GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["beamfile-suffix"] = "v2"
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_pNe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2017":
        beam_parameters["energy"] = "Beam2510GeV-0GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Fix1"
    elif properties.data_type == "2017-13TeV":
        beam_parameters["energy"] = "Beam6500GeV-0GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Fix1"
    elif properties.data_type == "2024.W35.37":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.192"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W37.39":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.052"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Nu0.43"
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Nu0.43"
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_ppNe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.W35.37":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.192"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W37.39":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.052"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Nu7.6andNu0.43"
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["spillover"] = False
        beam_parameters["PileUp"] = "Nu2.9andNu0.43"
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_pHe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2016":
        beam_parameters["energy"] = "Beam4000GeV-0GeV"
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2016-13TeV":
        beam_parameters["energy"] = "Beam6500GeV-0GeV"
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.2"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W29.30":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.007"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W25.27":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.007"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.2"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppHe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.2"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W29.30":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.007"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W25.27":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.007"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Nu2.9andNu0.2"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_pAr(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.W32.34":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.025"
        beam_parameters["spillover"] = False
    elif properties.data_type in {"2022", "2023", "2024.Q1.2"}:
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.37"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.37"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2015":
        beam_parameters["energy"] = "Beam6500GeV-0GeV"
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppAr(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.W32.34":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.025"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2022":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu2.1andNu0.37"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2023":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu1.6andNu0.37"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.37"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Nu2.9andNu0.37"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_Arp(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2022":
        beam_parameters["energy"] = "0GeV-Beam6800GeV"
        beam_parameters["PileUp"] = "Nu0.37"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2023":
        beam_parameters["energy"] = "0GeV-Beam6800GeV"
        beam_parameters["PileUp"] = "Nu0.37"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_pH2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.W32.34":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.029"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W31":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.025"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W29.30":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.031"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W35.37":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.027"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.087"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.087"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppH2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.W32.34":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.029"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W31":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.025"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W29.30":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.031"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.W35.37":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu6.3andNu0.027"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.087"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Nu2.9andNu0.087"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_pD2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.08"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.08"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppD2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.08"
        beam_parameters["spillover"] = False
    elif properties.data_type == "expected-2024-5TeV":
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Nu2.9andNu0.08"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_pN2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.168"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppN2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.168"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_pO2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.185"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppO2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.185"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_pKr(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.447"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppKr(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.447"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_pXe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.446"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_ppXe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam6800GeV"
        beam_parameters["PileUp"] = "Nu7.6andNu0.446"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters


def get_PbNe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type == "2018":
        beam_parameters["energy"] = "Beam2510GeV-0GeV"
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["spillover"] = False
    elif properties.data_type == "2024.Q1.2":
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.43"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPb(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2015", "2018"}:
        beam_parameters["energy"] = "Beam2510GeV"
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["spillover"] = False
    elif properties.data_type in {"2023", "2024.Q1.2", "expected-2024.Q3.4", "2024"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbAr(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2023", "2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.37"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbH2(properties: Properties):
    assert properties.data_type in {"2024.Q1.2"}
    beam_parameters = {
        "energy": "Beam2680GeV-0GeV",
        "PrimaryvertexLocation": "",
        "SpecialDetectorConfigurations": "",
        "PileUp": "Nu0.087",
        "spillover": False,
        "beamfile-suffix": "",
    }
    return beam_parameters


def get_PbD2(properties: Properties):
    assert properties.data_type in {"2024.Q1.2"}
    beam_parameters = {
        "energy": "Beam2680GeV-0GeV",
        "PrimaryvertexLocation": "",
        "SpecialDetectorConfigurations": "",
        "PileUp": "Nu0.08",
        "spillover": False,
        "beamfile-suffix": "",
    }
    return beam_parameters


def get_PbO2(properties: Properties):
    assert properties.data_type in {"2024.Q1.2"}
    beam_parameters = {
        "energy": "Beam2680GeV-0GeV",
        "PrimaryvertexLocation": "",
        "SpecialDetectorConfigurations": "",
        "PileUp": "Nu0.185",
        "spillover": False,
        "beamfile-suffix": "",
    }
    return beam_parameters


def get_PbN2(properties: Properties):
    assert properties.data_type in {"2024.Q1.2"}
    beam_parameters = {
        "energy": "Beam2680GeV-0GeV",
        "PrimaryvertexLocation": "",
        "SpecialDetectorConfigurations": "",
        "PileUp": "Nu0.168",
        "spillover": False,
        "beamfile-suffix": "",
    }
    return beam_parameters


def get_PbHe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV-0GeV"
        beam_parameters["PileUp"] = "Nu0.2"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbKr(properties: Properties):
    assert properties.data_type in {"2024.Q1.2"}
    beam_parameters = {
        "energy": "Beam2680GeV-0GeV",
        "PrimaryvertexLocation": "",
        "SpecialDetectorConfigurations": "",
        "PileUp": "Nu0.447",
        "spillover": False,
        "beamfile-suffix": "",
    }
    return beam_parameters


def get_PbXe(properties: Properties):
    assert properties.data_type in {"2024.Q1.2"}
    beam_parameters = {
        "energy": "Beam2680GeV-0GeV",
        "PrimaryvertexLocation": "",
        "SpecialDetectorConfigurations": "",
        "PileUp": "Nu0.446",
        "spillover": False,
        "beamfile-suffix": "",
    }
    return beam_parameters


def get_ArPb(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2023", "2024.Q1.2"}:
        beam_parameters["energy"] = "0GeV-Beam2680GeV"
        beam_parameters["PileUp"] = "Nu0.37"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbAr(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2023", "2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.37"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbNe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.43"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbH2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.087"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbD2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.08"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbO2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu.185"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbN2(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.168"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbHe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.2"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbKr(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.447"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)

    return beam_parameters


def get_PbPbXe(properties: Properties):
    beam_parameters = copy(DEFAULT_BEAM_PARAMETERS)
    if properties.data_type in {"2024.Q1.2"}:
        beam_parameters["energy"] = "Beam2680GeV"
        beam_parameters["PileUp"] = "Fix1andNu0.446"
        beam_parameters["spillover"] = False
    else:
        raise NotImplementedError(properties.data_type)
    return beam_parameters
