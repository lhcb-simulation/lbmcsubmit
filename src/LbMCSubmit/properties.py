###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from dataclasses import asdict, dataclass
from typing import Optional


@dataclass
class Properties:  # pylint: disable=too-many-instance-attributes
    data_type: str
    collision_type: str
    smog: bool
    pgun: bool
    polarity: str
    sim_version: str
    file_format: str
    decfiles: str
    custom_nu: str

    def asdict(self):
        return asdict(self)


@dataclass
class Stage6Properties(Properties):
    spillover: bool
    pgun_version: str
    madgraph_version: str
    powheg_version: str
    # This is set by the generation.get_steps
    conditions_string: Optional[str] = None
    fast_simulation_type: Optional[str] = None
    # This is set by offline.get_steps
    # It is only relevant for UPC in 2015 PbPb(!)
    hlt_filters_in_reco: Optional[bool] = False
