###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

import pytest

from .test_references import REF_DECFILES


def test_latest_available(monkeypatch):
    monkeypatch.setattr("LbMCSubmit.versioning.ALLOWED_PATTERNS", re.compile(".*"))
    from LbMCSubmit.datapkgs import latest_datapackage

    assert latest_datapackage("AppConfig", "v3r3*") == "v3r399"


@pytest.mark.parametrize(
    "event_type, generator, compatible",
    [
        ("10000000", "Pythia8", True),  # pp => [<Xb>]cc ...
        ("11102021", "Pythia8", True),  # [B0 -> K+ K-]cc
        ("12103001", "Pythia8", True),  # [B+ -> pi+ pi- pi+]cc
        (
            "13104032",
            "Pythia8",
            True,
        ),  # [B_s0 -> (phi(1020) -> K+ K-) (f0(980) -> pi+ pi-)]cc
        ("14103048", "Pythia8", False),  # [B_c+ -> (B_s0 -> pi+ pi-) pi+]cc
        ("14103048", "BcVegPy", True),  # [B_c+ -> (B_s0 -> pi+ pi-) pi+]cc
        (
            "15198010",
            "Pythia8",
            True,
        ),  # [Lambda_b0 -> K- pi+ (Lambda_c+ -> p+ K- pi+) (D- -> K+ pi- pi-)]cc
        ("20000000", "Pythia8", True),  # pp => [<Xc>]cc ...
        ("21103013", "Pythia8", True),  # [D+ -> (phi -> K+ K-) pi+]cc
        ("22162004", "Pythia8", True),  # [D0 => K- pi+]cc
        ("23103014", "Pythia8", True),  # [D_s+ -> pi- pi+ pi+]cc
        ("24142005", "Pythia8", True),  # J/psi(1S) -> mu+ mu-
        ("25163000", "Pythia8", True),  # [ Lambda_c+ -> (D0 -> K- pi+) p+ ]cc
        ("34102104", "Pythia8", True),  # K_S0 -> pi+ pi-
        ("26264053", "Pythia8", False),  # [Xi_cc++ -> (Xi_c+ -> p K- pi+) pi+ ]cc
        ("26264053", "GenXicc", True),  # [Xi_cc++ -> (Xi_c+ -> p K- pi+) pi+ ]cc
        ("49152113", "SuperChic2", True),  # chi_c0 -> (psi(1S) -> e+ e-) gamma
        ("49100040", "Madgraph", True),  # pp -> (ALP -> gamma gamma)
        ("47100205", "StarLight", True),  # axion -> gamma gamma
        (
            "47134000",
            "OniaPairs",
            True,
        ),  # pp => ( J/psi(1S) -> mu+ mu ) ( J/psi(1S) -> mu+ mu- ) ...
        ("60001010", "Hijing", True),  # p Ne20[0.0] => ?
    ],
)
def test_generator_compatibility(event_type: str, generator: str, compatible: bool):
    from LbMCSubmit.eventtypes import check_generator

    decfiles_version = REF_DECFILES["sim10e"]  # TODO: should this be pinned?

    assert check_generator(decfiles_version, event_type, generator) == compatible
