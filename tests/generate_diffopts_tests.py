###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from collections import defaultdict
from pathlib import Path

import yaml

import LbMCSubmit
from test_references import (
    IGNORABLE,
    REF_APPCONFIG,
    REF_DECFILES,
    REF_MADGRAPHDATA,
    REF_MERGING_DAVINCI,
    REF_PGUN_VERSIONS,
    build_diffopts_script,
    remove_keys,
    sim_versions,
)

UNIQUE_TESTS = set([])


def test_check_references(sim_version, sim_cls, ref_name, ref_fn):
    major_sim_version = sim_version[:-1]
    default_decfiles = sim_cls.default_decfiles
    default_pgun_version = sim_cls.default_pgun_version
    default_madgraph_version = sim_cls.default_madgraph_version
    default_appconfig = LbMCSubmit.datapkgs.appconfig
    default_merging_davinci = LbMCSubmit.steps.merging.davinci
    sim_cls.default_decfiles = REF_DECFILES[f"sim{sim_version}"]
    sim_cls.default_pgun_version = REF_PGUN_VERSIONS[f"sim{major_sim_version}"]
    sim_cls.default_madgraph_version = REF_MADGRAPHDATA
    LbMCSubmit.datapkgs.appconfig = REF_APPCONFIG
    LbMCSubmit.steps.merging.davinci = REF_MERGING_DAVINCI
    tests = defaultdict(list)
    try:
        actual = LbMCSubmit.run(ref_fn, 0, 6)
    finally:
        LbMCSubmit.datapkgs.appconfig = default_appconfig
        LbMCSubmit.steps.merging.davinci = default_merging_davinci
        sim_cls.default_decfiles = default_decfiles
        sim_cls.default_pgun_version = default_pgun_version
        sim_cls.default_madgraph_version = default_madgraph_version

    with open(str(ref_fn).replace(".stage0.yaml", ".stage6.yaml")) as fp:
        expected = yaml.safe_load(fp.read())

    for i, (e, a) in enumerate(zip(expected, actual)):
        assert e["mc_config_version"] == a["mc_config_version"]
        year = a["mc_config_version"]
        for j, (step_e, step_a) in enumerate(zip(e["steps"], a["steps"])):
            app = step_e["application"]
            name = step_e["name"]
            if remove_keys(step_e, IGNORABLE) == remove_keys(step_a, IGNORABLE):
                continue
            script = list(build_diffopts_script(step_e, step_a, year))
            if "\n".join(script) in UNIQUE_TESTS:
                continue
            else:
                UNIQUE_TESTS.add("\n".join(script))
            test = f"@test \"{ref_name.split('.')[0]}: step {i}.{j} ({name})\" {{\n"
            for line in script:
                test += f"    {line}\n"
            test += "}\n\n"
            tests[app].extend([test])
    return tests


PREAMBLE = """#!/usr/bin/env bats

setup() {
    cd $(mktemp -d)
}

teardown() {
    echo -e "filename\twarn\terror\tfatal"
    for FILE in {old,new}.std{out,err}; do
        if [ -e ${FILE} ]; then
            echo -n "${FILE}"
            for STATUS in WARNING ERROR FATAL;do
                echo -ne "\t$(grep -c ${STATUS} ${FILE})"
            done
            echo ""
        fi
    done
    rm -f {old,new}.{opts,stdout,stderr}
}
"""

if __name__ == "__main__":
    all_tests = defaultdict(list)
    for args in sim_versions:
        for app, tests in test_check_references(*args).items():
            all_tests[app].extend(tests)
    n_apps = len(all_tests)
    n_tests = sum(map(len, all_tests.values()))
    print(
        f"BATS test summary: {n_tests} test{'' if n_tests == 1 else 's'}",
        f"covering {n_apps} application{'' if n_apps == 1 else 's'}",
    )
    for app, tests in all_tests.items():
        print(f"\t{app}:", len(tests))
        with open(Path("tests") / f"{app.replace('/','_')}.bats", "w") as f:
            f.write(PREAMBLE)
            for test in tests:
                f.write(test)
