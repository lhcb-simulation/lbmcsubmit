#!/bin/bash
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail

cd $(dirname $0)

function print_versions() {
	simver=${1}
	simmaj=${simver::-1}
	python - <<-EOF
	from test_references import REF_DECFILES, REF_PGUN_VERSIONS, REF_MADGRAPHDATA, REF_APPCONFIG, REF_MERGING_DAVINCI
	print(REF_DECFILES["${simver}"], REF_PGUN_VERSIONS["${simmaj}"], REF_MADGRAPHDATA, REF_APPCONFIG, REF_MERGING_DAVINCI)
	EOF
}

for simver in $(find references -type d -name "sim*" -exec basename {} \;)
do
	versions=($(print_versions $simver))
	decfiles="DecFiles.${versions[0]}"
	pgun="PGunsData.${versions[1]}"
	madgraph="MadgraphData.${versions[2]}"
	appconfig=${versions[3]}
	davinci=${versions[4]}
	for stage0 in references/${simver}*/*stage0.yaml
	do
		(
		stage6=${stage0//stage0/stage6}
		lb-mc --validation $stage0 $stage6
		sed -i -r "s|DecFiles\.v[rp0-9]+|${decfiles}|g" $stage6
		sed -i -r "s|PGunsData\.v[rp0-9]+|${pgun}|g" $stage6
		sed -i -r "s|MadgraphData\.v[rp0-9]+|${madgraph}|g" $stage6
		sed -i -r "s|AppConfig\.v[rp0-9]+|${appconfig}|g" $stage6
		sed -i -r "/merging/{n;n;s|DaVinci/v[rp0-9]+|${davinci}|}" $stage6
		) &
	done
	wait
done
