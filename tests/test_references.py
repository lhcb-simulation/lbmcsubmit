###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

import pytest
import yaml

import LbMCSubmit.cli

REF_DIR = Path(__file__).parent / "references"
REF_DECFILES = (
    {f"sim09{v}": "v29r25" for v in "ab"}
    | {f"sim09{v}": "v30r94" for v in "cdefghijklm"}  # noqa: W503
    | {f"sim10{v}": "v32r23" for v in "cde"}  # noqa: W503
)
REF_PGUN_VERSIONS = {"sim09": "v2r5", "sim10": "v2r5"}
REF_APPCONFIG = "AppConfig.v3r444"
REF_MERGING_DAVINCI = "DaVinci/v46r13"
REF_MADGRAPHDATA = "v20903r7p1"
REF_POWHEGBOXDATA = "v3744r7p0"

# List of keys to ignore when deciding that a step is exactly-identical
IGNORABLE = ["name"]

sim_versions = [
    (a, b, c.name, c)
    for a, b in LbMCSubmit.SimBase.available_versions.items()
    for c in (REF_DIR / f"sim{a}").glob("*.stage0.yaml")
]


def compare_step(a: dict, b: dict) -> None:
    """
    Go through each key in the dicts a and b and compare values.
    Assume all lists are order-independent, so sort before comparing.
    """
    assert a.keys() == b.keys()
    for (a_key, a_val), (b_key, b_val) in zip(sorted(a.items()), sorted(b.items())):
        assert a_key == b_key  # Perhaps a bit overly-cautious
        assert isinstance(a_val, list) == isinstance(b_val, list)
        if isinstance(a_val, list) and isinstance(b_val, list):
            assert sorted(a_val) == sorted(b_val)
        else:
            assert a_val == b_val


def remove_keys(d: dict, keys: list[str]) -> dict:
    """
    Return a copy of the dict d without the specified keys
    """
    return {i: d[i] for i in d if i not in keys}


def compare_request(a: dict, b: dict) -> None:
    """
    Do a straightforward comparison of dicts a and b without the "steps" key.
    Pass each element of a["steps"] and b["steps"] to compare_steps
    """
    assert remove_keys(a, ["steps"] + IGNORABLE) == remove_keys(
        b, ["steps"] + IGNORABLE
    )
    assert len(a["steps"]) == len(b["steps"])
    for step_a, step_b in zip(a["steps"], b["steps"]):
        compare_step(remove_keys(step_a, IGNORABLE), remove_keys(step_b, IGNORABLE))


def build_lbrun_cmd(step: dict) -> None:
    yield "lb-run"
    yield "--force"
    yield "--siteroot=/cvmfs/lhcb.cern.ch/lib"
    for package in sorted(step["data_pkgs"]):
        yield f"--use={package}"
    if isinstance(step, str):
        yield step["application"]
    else:
        if "binary_tag" in step["application"]:
            yield "-c"
            yield step["application"]["binary_tag"]
        yield f"{step['application']['name']}/{step['application']['version']}"


def build_dryrun_cmd(output: str, options: list[str], year: str) -> None:
    yield "gaudirun.py"
    yield "--dry-run"
    yield f"--output={output}"
    for fname in options:
        # Lack of ProdConf means:
        # - Ignore the event type opts file
        # - Set run/event number with Gauss-Job.py
        # - Set tags with DBTags-{year}.py
        if "@{eventType}" in fname:
            yield "$GAUSSOPTS/Gauss-Job.py"
            yield f"$GAUSSOPTS/DBTags-{year}.py"
            continue
        yield fname
    stdout = output.replace("opts", "stdout")
    stderr = output.replace("opts", "stderr")
    yield f"1>{stdout} 2>{stderr}"


def build_diffopts_cmd(old_opts: str, new_opts: str) -> None:
    import sys

    yield "diff"
    if sys.stdout.isatty():
        yield "--color"
    yield old_opts
    yield new_opts


def build_diffopts_script(step_a: dict, step_b: dict, year: str) -> None:
    """
    Create a script to test the equivalence of two Gaudi application configurations.
    """
    lbrun_prefix = " ".join(list(build_lbrun_cmd(step_a)))
    # One lb-run command is used for both steps to reduce the overhead
    # Therefore we should assert that the commands are identical
    other_lbrun_prefix = " ".join(list(build_lbrun_cmd(step_b)))
    assert lbrun_prefix == other_lbrun_prefix, f"{lbrun_prefix} vs {other_lbrun_prefix}"
    ofile_map = {
        "old.opts": step_a,
        "new.opts": step_b,
    }
    dryrun_cmds = [
        " ".join(list(build_dryrun_cmd(output, step["options"], year)))
        for output, step in ofile_map.items()
    ]
    diff_cmd = " ".join(list(build_diffopts_cmd(*ofile_map.keys())))
    assert len(dryrun_cmds) == 2
    test_script = "; ".join(dryrun_cmds)
    yield f"{lbrun_prefix} bash -c '{test_script}'"
    yield diff_cmd


@pytest.mark.parametrize(
    "sim_version,sim_cls,ref_name,ref_fn",
    sim_versions,
)
def test_check_references(
    sim_version: str,
    sim_cls: type[LbMCSubmit.SimBase],
    ref_name: str,
    ref_fn: str,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    # Set the version of Gen/DecFiles to be a default value so that reference stage 6
    # yaml files do not have to be updated every time a new version is released.
    major_sim_version = sim_version[:-1]
    monkeypatch.setattr(
        sim_cls,
        "default_decfiles",
        REF_DECFILES[f"sim{sim_version}"],
        raising=True,
    )
    monkeypatch.setattr(
        sim_cls,
        "default_pgun_version",
        REF_PGUN_VERSIONS[f"sim{major_sim_version}"],
        raising=True,
    )
    monkeypatch.setattr(
        sim_cls,
        "default_madgraph_version",
        REF_MADGRAPHDATA,
        raising=True,
    )
    monkeypatch.setattr(
        sim_cls,
        "default_powheg_version",
        REF_POWHEGBOXDATA,
        raising=True,
    )
    monkeypatch.setattr("LbMCSubmit.datapkgs.appconfig", REF_APPCONFIG)
    monkeypatch.setattr("LbMCSubmit.steps.merging.davinci", REF_MERGING_DAVINCI)
    actual = LbMCSubmit.run(ref_fn, 0, 6)
    # TODO: Need to include processing pass data in the stage 6 output
    # assert set(s["sim-version"] for s in actual) == {sim_version}
    with open(str(ref_fn).replace(".stage0.yaml", ".stage6.yaml")) as fp:
        expected = yaml.safe_load(fp.read())

    for e, a in zip(expected, actual):
        compare_request(e, a)
