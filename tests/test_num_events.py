###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest


@pytest.mark.parametrize(
    "user_input, expected",
    [
        ("1000", 1000),
        ("1_000", 1000),
        ("1k", 1000),
        ("1M", 1000000),
        ("1_000k", 1000000),
        ("0.1M", 100000),
        ("3.14k", 3140),
        (".5k", 500),
    ],
)
def test_parse_num_events(user_input, expected):
    from LbMCSubmit.utils import parse_num_event

    assert parse_num_event(user_input) == expected
