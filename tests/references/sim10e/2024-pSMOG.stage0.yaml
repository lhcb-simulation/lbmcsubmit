sim-version: 10

name: My Analysis
inform:
  - auser
  - sam.doe@cern.ch
WG: IFT
#file-format: DIGI

generation:
    production-tool: Epos

# some examples:
samples:
  # Block 1: Ar,H2
  # support up to sprucing
  # MinBias in SMOG2
  - event-types:
      - 30000000
    data-types:
      - 2024.W32.34
    num-events: 1_000_000
    collision-types:
      - pAr
      - pH2
    smog: True
    file-format: DST
    sprucing: 24c2 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: SMOG2
  # Embedding signal in pp+SMOG2
  - event-types:
      - 24142001
    data-types:
      - 2024.W32.34
    num-events: 1_000_000
    collision-types:
      - ppAr
      - ppH2
    smog: True
    file-format: DST
    sprucing: 24c2 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: andSMOG2
        SignalMode: embedding

  # Block 2: H2
  # support up to sprucing
  # MinBias in SMOG2
  - event-types:
      - 30000000
    data-types:
      - 2024.W31
    num-events: 1_000_000
    collision-types:
      - pH2
    smog: True
    file-format: DST
    sprucing: 24c2 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: SMOG2
  # Embedding signal in pp+SMOG2
  - event-types:
      - 24142001
    data-types:
      - 2024.W31
    num-events: 1_000_000
    collision-types:
      - ppH2
    smog: True
    file-format: DST
    sprucing: 24c2 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: andSMOG2
        SignalMode: embedding

  # Block 3: H2. He
  # Support only up to DIGI (Boole)
  # MinBias in SMOG2
  - event-types:
      - 30000000
    data-types:
      - 2024.W29.30
    num-events: 1_000_000
    collision-types:
      - pH2
      - pHe
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: SMOG2
  # Embedding signal in pp+SMOG2
  - event-types:
      - 24142001
    data-types:
      - 2024.W29.30
    num-events: 1_000_000
    collision-types:
      - ppH2
      - ppHe
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: andSMOG2
        SignalMode: embedding

  # Block 4: H2. He
  # Support only up to DIGI (Boole)
  # MinBias in SMOG2
  - event-types:
      - 30000000
    data-types:
      - 2024.W25.27
    num-events: 1_000_000
    collision-types:
      - pHe
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: SMOG2
  # Embedding signal in pp+SMOG2
  - event-types:
      - 24142001
    data-types:
      - 2024.W25.27
    num-events: 1_000_000
    collision-types:
      - ppHe
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: andSMOG2
        SignalMode: embedding

  # Block 5: H2, Ne
  # support up to sprucing
  # MinBias in SMOG2
  - event-types:
      - 30000000
    data-types:
      - 2024.W35.37
    num-events: 1_000_000
    collision-types:
      - pH2
      - pNe
    smog: True
    file-format: DST
    sprucing: 24c3 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: SMOG2
  # Embedding signal in pp+SMOG2
  - event-types:
      - 24142001
    data-types:
      - 2024.W35.37
    num-events: 1_000_000
    collision-types:
      - ppH2
      - ppNe
    smog: True
    file-format: DST
    sprucing: 24c3 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: andSMOG2
        SignalMode: embedding

  # Block 6: Ne
  # support up to sprucing
  # MinBias in SMOG2
  - event-types:
      - 30000000
    data-types:
      - 2024.W37.39
    num-events: 1_000_000
    collision-types:
      - pNe
    smog: True
    file-format: DST
    sprucing: 24c3 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: SMOG2
  # Embedding signal in pp+SMOG2
  - event-types:
      - 24142001
    data-types:
      - 2024.W37.39
    num-events: 1_000_000
    collision-types:
      - ppNe
    smog: True
    file-format: DST
    sprucing: 24c3 # remove this line if you don't want sprucing
    generation:
      options:
        Beam: andSMOG2
        SignalMode: embedding


#----------------------------- Below are expected configurations --------------------------
  # 2024.Q1.2 p{Ar,He,H2,D2,O2,Ne,Kr,Xe} SMOG2, PVZ in [-541,-341]mm
  # MinBias
  - event-types:
      - 30000000
    data-types:
      - 2024.Q1.2
    num-events: 1_000_000
    collision-types:
      - pAr
      - pHe
      - pNe
      - pH2
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: SMOG2
  # Embeding
  - event-types:
      - 24142001
    data-types:
      - 2024.Q1.2
    num-events: 1_000_000
    collision-types:
      - pAr
      - pHe
      - pNe
      - pH2
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: SMOG2
        SignalMode: embedding

  # 2024.Q1.2 pp{Ar,He,H2,D2,O2,Ne,Kr,Xe} SMOG2, PVZ in BeamGas([-541,-341]mm) and BeamBeam([-200,200]mm)
  # MinBias
  - event-types:
      - 30000000
    data-types:
      - 2024.Q1.2
    num-events: 1_000_000
    collision-types:
      - ppAr
      - ppHe
      - ppNe
      - ppH2
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: andSMOG2
   # Embeding
  - event-types:
      - 24142001
    data-types:
      - 2024.Q1.2
    num-events: 1_000_000
    collision-types:
      - ppAr
      - ppHe
      - ppNe
      - ppH2
    smog: True
    file-format: DIGI
    generation:
      options:
        Beam: andSMOG2
        SignalMode: embedding
