# LbMCSubmit

[![pipeline status](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/badges/main/pipeline.svg)](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/pipelines/latest)
[![coverage report](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/badges/main/coverage.svg)](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/pipelines/latest)
[![Latest Release](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/badges/release.svg)](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/releases)

This package provides a mechanism to define, test and submit Monte Carlo production requests in LHCb by creating a YAML document.

## Table of contents

<!-- Generated with https://github.com/ekalinin/github-markdown-toc -->
* [Stage 0 specification](#stage-0-specification)
  * [Example: Basic](#example-basic)
  * [Example: Different number of events in Run 1 and Run 2](#example-different-number-of-events-in-run-1-and-run-2)
  * [Example: ReDecay](#example-redecay)
  * [Example: ReDecay for Run 2 only](#example-redecay-for-run-2-only)
  * [Example: Particle Gun](#example-particle-gun)
  * [Example: lead-proton collisions](#example-lead-proton-collisions)
  * [Example: SMOG](#smog)
  * [Example: Stripping filtered](#example-stripping-filtered)
* [Testing a production](#testing-a-production)
* [Submitting a production](#submitting-a-production)
* [Advanced documentation](#advanced-documentation)
  * [Contributing guide](#contributing-guide)
  * [Package structure](#package-structure)
  * [How is the Stage 0 input converted to a production request?](#how-is-the-stage-0-input-converted-to-a-production-request)

## Stage 0 specification

Most requests can be submitted by creating a Stage 0 input file from the following template. If this is insufficient please contact your working group's simulation liaison.

**NOTE:** Some of the options in this example are mutually exclusive, see the examples for working combinations of options

```yaml
# Specify the simulation version, major versions are automatically changed to
# the latest patch release during submission (e.g. 09 becomes 09j)
sim-version: 09

# Metadata about the analysis
name: My Analysis
inform:
  - acernusername
  - firstname.surname@cern.ch
WG: Charm

# Samples contains a list of one or more mappings describing the productions
samples:
  - # The event-types, data-types and magnet-polarities are expanded as a product
    event-types:
      - 23103006
      - 27165175
      - 30000000
    # The data taking conditions to generate
    data-types:
      - 2011
      - 2012
      - 2015
    # The magnet polarities to generate, defaults to ["MagDown", "MagUp"]
    magnet-polarities:
      - MagDown
      - MagUp
    # The collision types to generate, defaults to ["pp"]
    collision-types:
      - pp
    # The number of events to generate in each sample, in this example there are
    # 3 event-types * 3 data-types * 2 magnet-polarities = 9 million total events
    num-events: 500_000
    # Optional other top level keys can be overridden here such as fast-mc or generator

# The remaining keys are optional for customising how the sample is generated

file-format: MDST

generation:
  # Use a non-standard production tool (e.g. Particle Gun, EPOS, etc.)
  production-tool: Particle Gun

fast-mc:
  # Enable ReDecay, can be either a boolean or a mapping to override advanced parameters
  redecay: # If using the default parameters, just put "yes" or "no" here
    num-redecays: 25
```

### Example: Basic

<!-- TEST START -->
```yaml
sim-version: 09

name: My Analysis
inform:
  - acernusername
  - firstname.surname@cern.ch
WG: Charm

samples:
  - event-types:
      - 23103006
      - 27165175
      - 30000000
    data-types:
      - 2011
      - 2012
      - 2015
      - 2016
      - 2017
      - 2018
    num-events: 500_000
```
<!-- TEST END -->

### Example: Different number of events in Run 1 and Run 2

<!-- TEST START -->
```yaml
sim-version: 09

name: My Analysis
inform:
  - acernusername
  - firstname.surname@cern.ch
WG: Charm

samples:
  - event-types:
      - 23103006
      - 27165175
      - 30000000
    data-types:
      - 2011
      - 2012
      # 2015 was shorter so less MC is required
      - 2015
    # 500,000 events per magnet polarity = 1 million events per year
    num-events: 500_000
  - event-types:
      - 23103006
      - 27165175
      - 30000000
    data-types:
      - 2016
      - 2017
      - 2018
    # 2.5 million events per magnet polarity = 5 million events per year
    num-events: 2_500_000
```
<!-- TEST END -->

### Example: ReDecay

<!-- TEST START -->
```yaml
sim-version: 09

name: My Analysis
inform:
  - acernusername
  - firstname.surname@cern.ch
WG: Charm

samples:
  - event-types:
      - 23103006
      - 27165175
      - 30000000
    data-types:
      - 2011
      - 2012
      - 2015
      - 2016
      - 2017
      - 2018
    num-events: 1_000_000

fast-mc:
  redecay: yes
```
<!-- TEST END -->

### Example: ReDecay for Run 2 only

<!-- TEST START -->
```yaml
sim-version: 09

name: My Analysis
inform:
  - acernusername
  - firstname.surname@cern.ch
WG: Charm

samples:
  - event-types:
      - 23103006
      - 27165175
      - 30000000
    data-types:
      - 2011
      - 2012
      - 2015
    num-events: 500_000
  - event-types:
      - 23103006
      - 27165175
      - 30000000
    data-types:
      - 2016
      - 2017
      - 2018
    num-events: 2_500_000
    fast-mc:
      redecay: yes
```
<!-- TEST END -->

### Example: Particle Gun

<!-- TEST START -->
```yaml
sim-version: 09

name: My Analysis
inform:
  - acernusername
  - firstname.surname@cern.ch
WG: Charm

samples:
  - event-types:
      - 23103006
      - 27165175
      - 30000000
    data-types:
      - 2011
      - 2012
      - 2015
      - 2016
      - 2017
      - 2018
    num-events: 500_000

generation:
  production-tool: Particle Gun
```
<!-- TEST END -->

### Example: lead collisions

<!-- TEST START -->
```yaml
sim-version: 09k

name: My Analysis
inform:
  - auser
WG: IFT

samples:
  - event-types:
      - 35103100
      - 36103100
    data-types:
      # Supported data types for pPb/Pbp collisions:
      - 2013
    collision-types:
      - Pbp
      - pPb
    num-events: 10_000

  - event-types:
      - 28142001
    data-types:
      # Supported data types for PbPb collisions:
      - 2015
      - 2018
    collision-types:
      - PbPb
    magnet-polarities:
      # Only MagDown is supported for PbPb
      - MagDown
    num-events: 10_000

# non-pp collisions cannot be stored as mDST
file-format: DST
# The production tool will automatically be Epos for non-pp collisions.
```
<!-- TEST END -->

### SMOG

<!-- TEST START -->
```yaml
sim-version: 09k

name: My Analysis
inform:
  - auser
WG: IFT

samples:
  - event-types:
      - 30000000
    data-types:
      # Note that certain SMOG collision types only work with certain data types
      - 2016
    num-events: 10_000
    collision-types:
      - pHe   # Supported collision types: pNe, pHe, pAr, PbNe
    smog: True
    magnet-polarities:
      - MagDown
    file-format: DST
```
<!-- TEST END -->

### Example: Stripping filtered

TODO

## Testing a production

TODO

## Submitting a production
To convert a stage 0 yaml file (called `stage0-input-file.yaml` in the example below) to a stage 6 yaml file (called `stage6-output-file.yaml` in the example below), which then can be used to submit production requests, run
```bash
lb-mc stage0-input-file.yaml stage6-output-file.yaml
```
in the terminal. Additionally, the `lb-mc` command can be used to convert a yaml file from any stage to any higher stage, e.g., a stage 2 to a stage 4 file. For more information, run `lb-mc --help` in the terminal.

TODO

## Advanced documentation

### Contributing guide

See [`docs/contributing.md`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/docs/contributing.md).


### Package structure

See [`docs/package-structure.md`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/docs/package-structure.md).

### How is the Stage 0 input converted to a production request?

See [`docs/transformations.md`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/docs/transformations.md).
