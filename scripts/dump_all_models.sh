#!/bin/bash
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -eu

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

ALLMODELS=(
# Pythia8
52497,52496,48239,48235,40870,40869,40874,40873,42245,42242,42219,42218
# Pythia8+ReDecay
52509,52508,50382,50381,47205,47199,47207,47206,47209,47208,47211,47210
# BcVegPy+Pythia8
52504,52505,50383,50384,40881,40882,40885,40886,44296,44297,44259,44260
# GenXicc+Pythia8
52506,52507,50385,50386,40883,40884,40887,40888,50674,50675
# EPOS
62120,59739,57935,57934,50257,41329,41103,41102,40916,40915,40914,40913,37530,37529,71483,89035
# SuperChic2
62116,62115,61370,61369,62118,62117
# Pythia8+TrkOnlyPhys
63655,63654,63653,63652,49660,49659,47274,47273
# BcVegPy+TrkOnlyPhys
91172,91171,91170,91169,91168,91167,91166,91165
# PGun
71551,71550,71549,71548,40979,40978,40981,40980,50032,50031
# Starlight
66515,71474,80215
# Bound deuteron
55831,55832

)
for models in ${ALLMODELS[@]}
do
	lb-dirac "${SCRIPT_DIR}/dump_models.py" ${models}
done
