#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Print the DecFiles version for the given list of request model IDs
"""

import json

import yaml
from DIRAC.Core.Base import Script
from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
    ProductionRequestClient,
)

Script.setUsageMessage(
    __doc__
    + "\n".join(  # noqa: W503
        [
            "\nArguments:",
            "  requestID: comma-separated model IDs",
        ]
    )
)
Script.parseCommandLine()

pc = ProductionRequestClient()

BOLD = "\033[1m"
END = "\033[0m"

KEYS = {
    "name": "Name",
    "processing_path": "Pass",
    "visible": "Vis",
    # "application": "",
    "options": "Opt",
    "data_pkgs": "EP",
    "input_types": "IFT",
    "output_types": "OFT",
}

LIST_KEYS = {"options", "data_pkgs", "input_types", "output_types"}


def from_html(text):
    return text.replace("<b>", BOLD).replace("</b>", END).replace("<br/>", "\n")


for req_id in [
    int(x.strip()) for arg in Script.getPositionalArgs() for x in arg.split(",")
]:
    print(f"\n{BOLD}Request {req_id}:{END}\n\n")
    request = pc.getProductionRequest([req_id])["Value"][req_id]
    prod_detail = request["ProDetail"]
    parsed_prod_detail = json.loads(prod_detail)
    step_names = parsed_prod_detail["pAll"].split(",")
    steps = []
    for i, _ in enumerate(step_names, start=1):
        print(from_html(parsed_prod_detail[f"p{i}Html"]))
        steps += [
            {
                key: parsed_prod_detail[f"p{i}{name}"]
                for key, name in KEYS.items()
                if f"p{i}{name}" in parsed_prod_detail
            }
        ]
        steps[-1]["application"] = "/".join(
            [parsed_prod_detail[key] for key in [f"p{i}App", f"p{i}Ver"]]
        )
        for key in LIST_KEYS:
            if key in steps[-1]:
                steps[-1][key] = steps[-1][key].split(";")
    output = {
        "type": request["RequestType"],
        "priority": request["RequestPriority"],
        "name": request["RequestName"],
        "mc_version": json.loads(request["Extra"])["mcConfigVersion"],
        "sim_condition": request["SimCondition"],
        "inform": [],
        "wg": request["RequestWG"],
        "num_events": 1,
        "event_types": [],
        "dbtags": {
            "DDDB": parsed_prod_detail["p1DDDb"],
            "CondDB": parsed_prod_detail["p1CDb"],
        },
        "steps": steps,
    }
    if request["FastSimulationType"] != "None":
        output["fast_simulation_type"] = request["FastSimulationType"]
    with open(f"{req_id}.stage6.yaml", "w") as f:
        yaml.dump(output, f, sort_keys=False)
