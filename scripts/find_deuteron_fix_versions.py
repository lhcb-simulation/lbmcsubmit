#!/usr/bin/env lb-dirac python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import requests
from DIRAC import gLogger
from DIRAC.Core.Base import Script
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient


def main():
    bk_client = BookkeepingClient()
    fixme = {}
    # >>> from LbMCSubmit.sim_versions.stripping import STRIPPING
    # >>> list(STRIPPING.keys())
    for version in [
        "34",
        "34r0p1",
        "34r0p2",
        "35r2",
        "35r3",
        "29r2",
        "29r2p1",
        "29r2p2",
        "32",
        "33r2",
        "28r2",
        "28r2p1",
        "30r2",
        "30r3",
        "27",
        "24r2",
        "31r2",
        "31r1",
        "25",
        "20r2",
        "20r3p1",
        "20r3",
        "21",
        "21r0p1",
        "21r0p2",
        "21r1",
        "21r1p1",
        "21r1p2",
    ]:
        query = {"ProcessingPass": f"/Stripping{version}", "Usable": "No"}
        result = returnValueOrRaise(bk_client.getStepsMetadata(query))
        steps = [dict(x) for _, x in result["Records"].items()]
        if n := len(steps) == 0:
            gLogger.error(f"Cannot find usable steps for Stripping{version}")
            continue
        elif n > 1:
            gLogger.warn(f"{n} steps matching {version} found, taking the last one")
        dddb = steps[-1]["DDDB"]
        url = (
            "https://gitlab.cern.ch/lhcb-conddb/DDDB/"
            f"-/raw/{dddb}/param/ParticleTable.txt"
        )
        table = requests.get(url).content
        fixme[version] = b"deuteron~" in table
    print(sorted([version for version, fix in fixme.items() if fix]))


if __name__ == "__main__":
    Script.parseCommandLine(ignoreErrors=True)
    main()
