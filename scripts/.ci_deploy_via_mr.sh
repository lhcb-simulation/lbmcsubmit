#!/bin/sh
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

set -euxo pipefail

# Configuration
NEW_BRANCH="update_lbmcsubmit_${CI_COMMIT_TAG}"
COMMIT_MESSAGE="Update LbMCSubmit to ${CI_COMMIT_TAG}"
MR_TITLE="${COMMIT_MESSAGE}"
MR_DESCRIPTION="See ${CI_PROJECT_URL}/-/tags/${CI_COMMIT_TAG}"
CHECKOUT_DIR="target_deployment_repo"
API_ENDPOINT="https://gitlab.cern.ch/api/v4/projects"
PAYLOAD_FILE="payload.json"
TARGET_PROJECT_URL="ssh://git@gitlab.cern.ch:7999/${TARGET_PROJECT_SLUG}.git"
export GIT_SSH_COMMAND="ssh -i $(readlink -f ${DEPLOY_KEY}) -o StrictHostKeyChecking=no"

# Cleanup from previous runs
rm -rf ${CHECKOUT_DIR}
rm -f ${PAYLOAD_FILE}

# Check out the target project
git clone ${TARGET_PROJECT_URL} -b ${TARGET_PROJECT_BRANCH} ${CHECKOUT_DIR}
cd ${CHECKOUT_DIR}
git checkout -b ${NEW_BRANCH}

# Replace the tag
sed -i "s/lbmcsubmit.git@.*$/lbmcsubmit.git@${CI_COMMIT_TAG}/g" ${TARGET_FILE}

# Commit and push
git add ${TARGET_FILE}
git commit -m "${COMMIT_MESSAGE}"
git push -u origin ${NEW_BRANCH}

# Create the merge request
cat <<EOF > ${PAYLOAD_FILE}
{
	"id": ${TARGET_PROJECT_ID},
	"source_branch": "${NEW_BRANCH}",
	"target_branch": "${TARGET_PROJECT_BRANCH}",
	"title": "${MR_TITLE}",
	"description": "${MR_DESCRIPTION}",
	"remove_source_branch": true,
	"squash": true
}
EOF

curl --request POST "${API_ENDPOINT}/${TARGET_PROJECT_ID}/merge_requests" \
     --header "Content-Type: application/json" \
     --header "PRIVATE-TOKEN: ${TARGET_PROJECT_TOKEN}" \
     --data @${PAYLOAD_FILE}
