# LbMCSubmit Stage Transformations

The basic idea is to start from a YAML file which can normally be simple enough for analysts to write themselves. A series of stages are then applied to validate and transform the data into a fully specified chain of processing steps. Experts could bypass some of the earlier steps to be able to submit specialised productions.

Transforming the YAML between stages and validating would be done by a dedicated command. Ideally it should be relatively simple to "drop down a stage" whenever the need to do something specialised arises. TODO: Reference documentation for how experts can do this

## Table of contents

<!-- Generated with https://github.com/ekalinin/github-markdown-toc -->
* [Stage 0](#stage-0)
* [Stage 1](#stage-1)
* [Stage 2](#stage-2)
* [Stage 3](#stage-3)
* [Stage 4](#stage-4)
* [Stage 5](#stage-5)
* [Stage 6](#stage-6)

## Stage 0

The "Stage 0" format is the format provided by typical analysts that covers most use cases. This is documented on the [main README](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/README.md). For the purposes of this example we will use the following:

```yaml
sim-version: 09
name: My Analysis
inform:
  - cburr
  - sam.doe@cern.ch
WG: Charm
samples:
  - event-types:
      - 23613900
      - 27613500
      - 30000000
    data-types:
      - 2018
    num-events: 2_500_000
```

## Stage 1

During stage 1 the

- `sim-version` key is replaced with the latest version (if required)
- top level parameters are replaced by default parameters which are determined from the sim version

This results in:

```yaml
sim-version: 09j
name: My Analysis
inform:
- cburr
- sam.doe@cern.ch
WG: Charm
samples:
- event-types:
  - 23613900
  - 27613500
  - 30000000
  data-types:
  - 2018
  num-events: 2500000
generation:
  production-tool: Pythia8
  decay-tool: EvtGen
  material-tool: Geant4
```

## Stage 2

During stage 2 the top level parameters are layered in to the samples. This results in:

```yaml
- sim-version: 09j
  name: My Analysis
  inform:
  - cburr
  - sam.doe@cern.ch
  WG: Charm
  generation:
    production-tool: Pythia8
    decay-tool: EvtGen
    material-tool: Geant4
  event-types:
  - 23613900
  - 27613500
  - 30000000
  data-types:
  - 2018
  num-events: 2500000
```

## Stage 3

During stage 3 the top level parameters (`event-type`, `magnet-polarity`, `data-type`, `collision-type`) are layered in to the samples. This results in:

```yaml
- sim-version: 09j
  name: My Analysis
  inform:
  - cburr
  - sam.doe@cern.ch
  WG: Charm
  generation:
    production-tool: Pythia8
    decay-tool: EvtGen
    material-tool: Geant4
  event-types:
  - 23613900
  - 27613500
  - 30000000
  num-events: 2500000
  magnet-polarity: MagUp
  data-type: 2018
  collision-type: pp
- sim-version: 09j
  name: My Analysis
  inform:
  - cburr
  - sam.doe@cern.ch
  WG: Charm
  generation:
    production-tool: Pythia8
    decay-tool: EvtGen
    material-tool: Geant4
  event-types:
  - 23613900
  - 27613500
  - 30000000
  num-events: 2500000
  magnet-polarity: MagDown
  data-type: 2018
  collision-type: pp
```

## Stage 4

During stage 4 conditions specific information is added to each sample such as:

- The beam conditions and database tags
- Further processing in the trigger, reconstruction, tesla, ...

This results in:

```yaml
- sim-version: 09j
  name: My Analysis
  inform:
  - cburr
  - sam.doe@cern.ch
  WG: Charm
  generation:
    production-tool: Pythia8
    decay-tool: EvtGen
    material-tool: Geant4
    Gauss: v49r19
    beam:
      energy: 6500GeV
      spillover:
        spacing: 25ns
      nu: 1.6
  event-types:
  - 23613900
  - 27613500
  - 30000000
  num-events: 2500000
  magnet-polarity: MagUp
  data-type: 2018
  collision-type: pp
  digitisation:
    version: 14c
  trigger:
    L0:
      TCK: '0x18a4'
      Moore: v28r3p1
    HLT1:
      TCK: '0x517a18a4'
      Moore: v28r3p1
    HLT2:
      TCK: '0x617d18a4'
      Moore: v28r3p1
  reconstruction:
    version: '18'
    Brunel: v54r2
  tesla:
    version: 05-WithTurcal
    DaVinci: v44r7
  dbtags:
    DDDB: dddb-20170721-3
    CondDB: sim-20190430-vc-mu100
  filtering: {}
  fast-mc: {}
- sim-version: 09j
  name: My Analysis
  inform:
  - cburr
  - sam.doe@cern.ch
  WG: Charm
  generation:
    production-tool: Pythia8
    decay-tool: EvtGen
    material-tool: Geant4
    Gauss: v49r19
    beam:
      energy: 6500GeV
      spillover:
        spacing: 25ns
      nu: 1.6
  event-types:
  - 23613900
  - 27613500
  - 30000000
  num-events: 2500000
  magnet-polarity: MagDown
  data-type: 2018
  collision-type: pp
  digitisation:
    version: 14c
  trigger:
    L0:
      TCK: '0x18a4'
      Moore: v28r3p1
    HLT1:
      TCK: '0x517a18a4'
      Moore: v28r3p1
    HLT2:
      TCK: '0x617d18a4'
      Moore: v28r3p1
  reconstruction:
    version: '18'
    Brunel: v54r2
  tesla:
    version: 05-WithTurcal
    DaVinci: v44r7
  dbtags:
    DDDB: dddb-20170721-3
    CondDB: sim-20190430-vc-md100
  filtering: {}
  fast-mc: {}
```

## Stage 5

During stage 5, final transformations and sanity checks can be applied. In this example no changes are made to the output of step 4 however if Particle Gun is being used the trigger processing will be disabled:

```diff
--- a/stage5-normal.yaml
+++ b/stage5-pgun.yaml
@@ -5,7 +5,7 @@
   - sam.doe@cern.ch
   WG: Charm
   generation:
-    production-tool: Pythia8
+    production-tool: Particle Gun
     decay-tool: EvtGen
     material-tool: Geant4
     Gauss: v49r19
@@ -35,16 +35,7 @@
   collision-type: pp
   digitisation:
     version: 14c
-  trigger:
-    L0:
-      TCK: '0x18a4'
-      Moore: v28r3p1
-    HLT1:
-      TCK: '0x517a18a4'
-      Moore: v28r3p1
-    HLT2:
-      TCK: '0x617d18a4'
-      Moore: v28r3p1
+  trigger: {}
   reconstruction:
     version: '18'
     Brunel: v54r2
```

## Stage 6

The output of stage 6 will be something that DIRAC can understand directly to create productions. This format wouldn't be specific to MC productions and could be used for Analysis Productions, Stripping campaigns or any other kind of production request.

TODO: This format is not yet finalised, see https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/issues/8

```yaml
- name: My Analysis 2018 MagDown
  inform:
  - cburr
  - sam.doe@cern.ch
  WG: Charm
  num-events: 2500000
  event-types:
  - 23613900
  - 27613500
  - 30000000
  dbtags:
    DDDB: dddb-20170721-3
    CondDB: sim-20190430-vc-md100
  steps:
  - application: Gauss/v49r19
    options:
    - $APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2018-nu1.6.py
    - $APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py
    - $APPCONFIGOPTS/Gauss/DataType-2018.py
    - $APPCONFIGOPTS/Gauss/RICHRandomHits.py
    - $DECFILESROOT/options/@{eventType}.py
    - $LBPYTHIA8ROOT/options/Pythia8.py
    - $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py
    data-pkgs:
    - AppConfig.v3r402
    - Gen/DecFiles.v30r75
    input_types: []
    output_types:
    - SIM
    opt_fmt: null
  # Further steps have been truncated
```
