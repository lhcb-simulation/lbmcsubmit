# LbMCSubmit Package Structure

This document gives an overview of how the `LbMCSubmit` package is structured. It is targeted at simulation experts who are making changing to the new package

## Table of contents

<!-- Generated with https://github.com/ekalinin/github-markdown-toc -->
* [Stages](docs/package-structure.md#stages)
* [Steps](docs/package-structure.md#steps)
* [Global properties object](docs/package-structure.md#global-properties-object)
* [Data packages](docs/package-structure.md#data-packages)
* [Sim versions](docs/package-structure.md#sim-versions)
* [Common development tasks](docs/package-structure.md#common-development-tasks)
    * [Adding a new major sim version](docs/package-structure.md#adding-a-new-major-sim-version)
    * [Adding a new minor sim version](docs/package-structure.md#adding-a-new-minor-sim-version)
    * [Adding a new generator](docs/package-structure.md#adding-a-new-generator)
* [Testing](docs/package-structure.md#testing)
    * [Pre-commit tests](docs/package-structure.md#pre-commit-tests)
    * [Code tests](docs/package-structure.md#code-tests)
    * [Updating the regression test references](docs/package-structure.md#updating-the-regression-test-references)

## Stages

Each stage is applied by a function called `apply_stage_N` where `N` is the number of the stage. The first three are declared in `LbMCSubmit/__init__.py` and are independent of the simulation version. The latter three are member functions of the `SimBase` class and taken from the initialised object to allow specific sim versions to override some functionality.

## Steps

In order to convert the Stage 5 output into something which can be executed `LbMCSubmit` uses a concept of "steps" to split the production into a collection of smaller components. Each of these map to zero or more Gaudi applications to run and therefore zero or more DIRAC "steps". In order of typical execution these are:

1. Generation: Gauss
2. Digitisation: Boole
3. Online: Level 0 emulation, HLT1 and HLT2
4. Offline: Brunel and Tesla
5. Filtering: Stripping filtering and/or trigger filtering
6. Merging

The code for each of these these is defined in a dedicated submodule under [`LbMCSubmit.steps`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/tree/main/src/LbMCSubmit/steps). Each of these submodules contains a function named `get_steps` which is called with some global properties (see [Global properties](#global-properties)) as well as dictionaries containing parts of the Stage 5 output (e.g `offline.get_steps(properties, data["reconstruction"], data["tesla"])`).

## Global properties object

The `Properties` class is defined in [`LbMCSubmit.properties`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/src/LbMCSubmit/properties.py) and contain some general information to pass between steps such as the data type and magnet polarity. Futhermore a subclass named `Stage6Properties` is used to pass information that is required by multiple [`LbMCSubmit` steps](#steps), such as if particle gun or spillover is enabled.

## Data packages

TODO

## Sim versions

Simulation versions are defined in [`LbMCSubmit.sim_versions`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/tree/main/src/LbMCSubmit/sim_versions). A base class [`SimBase`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/src/LbMCSubmit/sim_versions/simbase.py) is used for all sim versions, with a variety of abstract properties and methods being required to define what will be used. This is subclassed in a `SimXX` class for each major version, while this is not technically required it makes the management of patch releases simpler as they can declare a smaller number of properties. The patch release classes (`SimXXz`) inherit from the major sim version class (`SimXX`), overriding properties such as `default_gauss_version` as required.

## Common development tasks

This section describes how to make various common changes to `LbMCSubmit`. See the [contributing documentation](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/docs/contributing.md) for a guide on how to set up a development environment to make these changes.

### Adding a new major sim version

To create a new major sim version named `simXX`:

1. Create a new submodule named `simXX.py` under [`LbMCSubmit.sim_versions`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/tree/main/src/LbMCSubmit/sim_versions).
2. Import all objects from the new submodule by adding `from .simXX import *  # NOQA` to [`sim_versions/__init__.py`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/tree/main/src/LbMCSubmit/sim_versions/__init__.py).
3. Create a `SimXX` class in `simXX.py` using one of the existing sim versions as a reference.
4. Create a `SimXXa` class in `simXX.py` that inherits from `SimXX`.
5. Create a new collection of regression test references in `tests/references/simXXa/` following the steps in [Updating the regression test references](#updating-the-regression-test-references).

### Adding a new minor sim version

To create a new minor sim version named `simXXz`:

1. Create a `SimXXz` subclass of `SimXX` in `LbMCSubmit.sim_versions.simXX`.
2. Modify any properties and methods on the subclass that differs between the minor (`SimXXz`) and major (`SimXX`) version.
3. Create a new collection of regression test references in `tests/references/simXXz/` following the steps in [Updating the regression test references](#updating-the-regression-test-references).

### Adding a new generator

TODO

## Testing

### Pre-commit tests

[`pre-commit`](https://pre-commit.com/) is used to perform quick tests of each commit. See the [contributing documentation](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/docs/contributing.md#pre-commit) for how to set this up.

### Code tests

Unit tests are ran using `pytest`. See the [contributing documentation](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/blob/main/docs/contributing.md#pytest) for details on how to run these.

In order to ensure older simulation productions are still generated in an identical way regression tests are also ran with `pytest` to ensure identical options files are produced. These collect Stage 0 inputs from `tests/references/simXXy/*.stage0.yaml` and compare them with the expected output in ``tests/references/simXXy/*.stage6.yaml``.

### Updating the regression test references

TODO
