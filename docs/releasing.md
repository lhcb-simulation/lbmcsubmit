# Making a release

Before making a release, ensure that the [pipeline for the `main` branch succeeds](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/pipelines?page=1&scope=branches&ref=main).

## New tag

[Create a new tag](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/tags/new) from the `main` branch, following a `YYYY.MM.DD.N` date format, where `N` counts from 0 (allowing multiple releases on the same day). You can leave the "Message" box blank.

## Release notes

[Create a new release](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/releases/new) with the same title as the tag. Try to stick to a consistent format as previous release notes (e.g. [`2023.04.17.0`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/releases/2023.04.17.0)).

```markdown
Features:

- A new QoL feature (!123)
- Support for a new generator/production type (!124)

Updates:

- Update a database tag or package version (!125)

Fixes:

- Fixed a bug (!126)
```

## Deployment

Upon making a tag, the CI pipeline will automatically create Merge Requests in:

- [LbAPI](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAPI/-/merge_requests)
- [LHCb Conda Environments](https://gitlab.cern.ch/lhcb-core/conda-environments/-/merge_requests)

Check these have correctly updated the tag in the target conda environments and merge if you have the permission, or ask someone else to merge.
